while True:
	h, u, d, f = [int(x) for x in input().split()]
	if h == 0:
		break
	height = 0
	prev = u
	day = 1
	fat = (u*f)/100
	good = False
	while(True):
		if(prev > 0):
			height = height + prev
		prev = prev-fat;
		if(height > h):
			good = True
			break;
		height = height-d	
		if(height < 0):
			break
		day=day+1
	if good == True:	
		print('success on day '+str(day))
	else:
		print('failure on day '+str(day))	

