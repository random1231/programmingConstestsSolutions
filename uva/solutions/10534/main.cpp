#include <bits/stdc++.h>
using namespace std;

#define MAX_N 100000

int l[MAX_N], l_id[MAX_N], p[MAX_N];
int n;
int lis, lis_end;

vector<int> longestIS(int a[]){
    lis = 0, lis_end = 0;
    for (int i = 0; i < n; ++i){
        int pos = lower_bound(l, l + lis, a[i]) - l;
        l[pos] = a[i];
        l_id[pos] = i;
        p[i] = pos ? l_id[pos - 1] : -1;
        if (pos + 1 > lis){
            lis = pos + 1;
            lis_end = i;
        }
    }
    vector<int> v;
    int x=lis_end;
    for (; p[x] >= 0; x = p[x])
       v.insert(v.begin(),a[x]);
    v.insert(v.begin(),a[x]);
    return v;
}

int main(){
    while(cin>>n){
        int v[n];
        for(int i = 0; i < n; ++i){
            cin>>v[i];
            cerr<<"v["<<i<<"]="<<v[i]<<endl;
        }

        longestIS(v);
        int aa[n-lis_end];
        int lis1=lis;
        cerr<<lis1<<endl;
        for(int i = lis,j=0; i < n; ++i,++j){
            cin>>aa[j];
        }
        longestIS(aa);
        int lis2=lis;
        cerr<<lis2<<endl;
        if(lis1-lis2<=1)
            cout<<lis1*2-1;
        else{
            cout<<2*lis2+1;
        }
        cout<<endl;
    }
    return 0;
}
