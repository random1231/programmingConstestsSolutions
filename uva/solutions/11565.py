from sys import stdin

def solve(a,b,c):
	for i in range(-22,22+1):
		if i*i<=c:
			for j in range(-100,100+1):
				if i!=j and i*i+j*j<=c:
					for k in range(-100,100+1):
						if i!=k and j!=k and i+j+k==a and i*j*k==b and i*i+j*j+k*k==c:
							print(i, j, k)
							return True
	return False

for t in range(int(input())):
	a, b, c=[int(x) for x in input().split()]
	if solve(a,b,c)==False:
		print("No solution.")

