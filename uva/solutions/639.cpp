#include <bits/stdc++.h>

using namespace std;

char grid[5][5];
int n;

bool check(int rw,int cl){
    if(grid[rw][cl]=='X') return false;
    for(int i = cl; i < n; ++i){
        if(grid[rw][i]=='X') break;
        else if(grid[rw][i]=='o') return false;
    }

    for(int i = cl; i >= 0; --i){
        if(grid[rw][i]=='X') break;
        else if(grid[rw][i]=='o') return false;
    }

    for(int i = rw; i < n; ++i){
        if(grid[i][cl]=='X') break;
        else if(grid[i][cl]=='o') return false;
    }

    for(int i = rw; i >=0; --i){
        if(grid[i][cl]=='X') break;
        else if(grid[i][cl]=='o') return false;
    }

    grid[rw][cl]='o';

    return true;
}

//We generate every configuration(combinations) 
int solve(vector<int> &v){
    auto tam=v.size();
    auto lim=(1<<tam);
    int mx=0;
    for(int i=0;i<lim;++i){
        int cur=0;
        for(int resi = 0; resi < n; ++resi){
            for(int resj = 0; resj < n; ++resj){
                if(grid[resi][resj]=='o')
                    grid[resi][resj]='.';
            }
        }

        for(int j=0;j<tam;++j){
            //Test if jth bit is on
            if(i&(1<<j)){
                int rw=j/n;
                int cl=j%n;
                if(check(rw,cl)){
                    ++cur;
                }else{  break;}

            }
            if(cur>mx)
                mx=cur;
        }
    }

    return mx;
}

int main()
{
    while(cin>>n&&n){
        // cout<<n<<endl;
        vector<int> v;
        int c=0;
        for(int i = 0; i < n; ++i){
            for(int j = 0; j < n; ++j){
                cin>>grid[i][j];
                v.push_back(c++);
            }
        }

        cout<<solve(v);
        cout<<endl;

    }


    return 0;
}