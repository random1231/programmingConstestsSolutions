#include <bits/stdc++.h>
using namespace std;

int n;
int dp[10010];


int solve(int cur){
    if(cur==0)
        return 0;
    auto &ref=dp[cur];
    if(ref!=-1) return ref;
    int ans=INT_MAX-1;
    for(int i=1;i<=cur;++i){
        auto res=cur-i*i;
        if(res<0) break;
        ans=min(ans,solve(res)+1);
    }
    return ref=ans;
}

int main(){
    memset(dp,-1,sizeof dp);
    int t;cin>>t;while(t--){
        cin>>n;
        cout<<solve(n)<<endl;
    }
    return 0;
}