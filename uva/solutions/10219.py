from sys import stdin
from math import log10


#log(n!)=log(n)+log(n-1)+...+log(1)
def fact(beg,n):
	ans=0
	for i in range(beg,n+1):
		ans+=log10(i)
	return ans

def main():
	for s in stdin:
		n,k=[int(x) for x in s.split()]
		beg=max(k, n-k)
		ans=fact(beg+1,n)-fact(1,min(k, n-k))
		print(int(ans)+1)
main()