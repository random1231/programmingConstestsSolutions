#include <bits/stdc++.h>

using namespace std;

long long a[201];
int dp[201][11][1000];
int n,q,d,m;

long long mod(long long xx, int yy){
    if(xx>=0)return xx%yy;
    return (yy+(xx%yy))%yy;
}

int solve(int id, int cnt, long long cur){
    if(cnt==m){
        if(cur==0) return 1;
        return 0;
    }
    if(id==n) return 0;
    auto &ref=dp[id][cnt][cur];
    if(ref!=-1) return ref;
    return ref=solve(id+1,cnt,cur)+solve(id+1,cnt+1,mod(cur+a[id],d));
}

int main(){
    int tc=1;
    while(cin>>n>>q&&n&&q){
        cout<<"SET "<<tc++<<":"<<endl;
        for(int i = 0; i < n; ++i){
            cin>>a[i];
        }
        for(int i = 1; i <= q; ++i){
            memset(dp,-1,sizeof dp);
            cin>>d>>m;
            cout<<"QUERY "<<i<<": "<<solve(0,0,0)<<endl;
        }
    }
    return 0;
}