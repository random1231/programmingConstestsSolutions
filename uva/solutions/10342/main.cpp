





// I didn't solve it




#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin
#define clear(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef pair<pair<int,int>,int> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;
const double epsilon=1e-9;
const double euler=exp(1.0);


double p,q,r,s,t,u;

double simulate(double sol){
	double res=p*(exp(sol*(-1.0)))+q*sin(sol)+r*cos(sol)+s*tan(sol)+t*sol*sol+u;
	return res;
}

int solve(){
	double start=0.0,end=1.0,mid=0.0,ans=0.0;
	while(fabs(start-end)>epsilon){
		mid=(start+end)/2.0;
		ans=simulate(mid);
	}
	
	return ans;
}

int main(){
	while(cin>>p>>q>>r>>s>>t>>u){
		solve();
	}
	return 0;
}

