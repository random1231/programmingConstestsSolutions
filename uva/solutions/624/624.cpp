#include <bits/stdc++.h>

using namespace std;
int n;
int ans, ans2;

int solve(vector<int> &v, int idx, int cur, int el){
    if(idx>=v.size())
        return cur;
    int res=0;
    for(int i = idx; i < v.size(); ++i){
        if(n<v[i]+cur)
            continue;
        el|=(1<<i);
        if(res>ans){
            ans=res;
            ans2=el;
        }
        el&=~(1<<(i));
    }
    return cur;
}

int main(){
    while(cin>>n){
        int tam;
        ans=0;
        ans2=0;
        cin>>tam;
        vector<int> v;
        while(tam--){
            int tmp;cin>>tmp;v.push_back(tmp);
        }
        solve(v,0,0,0);
        for(int i = 0; i <v.size(); ++i){
            if(ans2&(1<<i))
                cout<<v[i]<< " ";
        }
        cout<<"sum:"<<ans<<endl;
    }
    return 0;
}