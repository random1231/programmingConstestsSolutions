#include <bits/stdc++.h>

using namespace std;

int l,s;

int dp[400][30][400];

int solve(int ln, int id, int curs){
    if(ln==l){
        if(curs==0) return 1;
        return 0;
    }
    if(curs<=0||id>=27) return 0;

    auto &ref=dp[ln][id][curs];
    if(ref!=-1)return ref;

    return ref=solve(ln+1,id+1,curs-id)+solve(ln,id+1,curs);
}

int main(){
    int tc=1;
    while(cin>>l>>s&&l&&s){
        memset(dp,-1,sizeof dp);
        cout<<"Case "<<tc++<<": "<<solve(0,1,s)<<endl;
    }
    return 0;
}