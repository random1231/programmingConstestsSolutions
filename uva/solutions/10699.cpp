#include <bits/stdc++.h>
using namespace std;

int numDiffPF[1000009];

void primeFactorsRange(int lim){
	for (int i = 2; i < lim; i++)
		if (numDiffPF[i] == 0)
			for (int j = i; j < lim; j += i)
				numDiffPF[j]++;
}

int main(){
	long long n;
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	primeFactorsRange(1000001);
	while(cin >> n && n!=0){
		cout << n << " : " << numDiffPF[n] <<std::endl;
	}
	return 0;
}