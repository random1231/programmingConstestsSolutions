#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define ff first
#define ss second
#define beg begin
#define clear(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef pair<pair<int,int>,int> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;

int a[50010];
int n;

ii solve(int q){
	int res= lower_bound(a,a+n,q)-a;
	int res2= upper_bound(a,a+n,q)-a;
//	cerr<<res<< " "<<res2;
	//cerr<<endl;
	if(res==0) res=-1;
	else res=a[res-1];
	if(res2==n) res2=-1;
	else res2=a[res2];
	
	return make_pair(res,res2);
}

int main(){
	cin>>n;
	for(int i=0; i<n;i++){
		cin>>a[i];
	}
	int query;cin>>query;while(query--){
		int q;cin>>q;
		auto res=solve(q);
		
		if(res.ff==-1) cout<< "X ";
		else cout<<res.ff<< " ";
		if(res.ss==-1) cout<< "X";
		else cout<<res.ss;
		cout<<endl;
		
	}
	return 0;
}

