#include <bits/stdc++.h>

using namespace std;

long long x, y, d;
//Extended euclid returns a triple that satisfies the equation
//ax+by=d where d is gcd(a, b)
void extendedEuclid(long long a, long long b){
	if(b == 0){
		x = 1;
		y = 0;
		d = a;
		return;
	}
	extendedEuclid(b, a%b);
	int x1 = y;
	int y1 = x-(a/b)*y;
	x = x1;
	y = y1;

}

int main(int argc, char const *argv[])
{
	int t;
	cin>>t;
	while(t--){
		long long a, b;
		cin>>a>>b;
		auto aa = floor(a/b);
		auto bb = ceil((a*1.0)/b);
		extendedEuclid(aa, bb);
		x*=(a/d);
		y*=(a/d);
		cout<<x<<" "<<y<<endl;
	}
	return 0;
}