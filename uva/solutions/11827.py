from fractions import gcd

def main():
	n = int(input())
	for _ in range(n):
		l = [int(x) for x in input().split()]
		mx = 0
		for i in range(len(l)-1):
			for j in range(i+1, len(l)):
				g = gcd(l[i], l[j])
				if(g>mx):
					mx=g
		print(mx)

main()