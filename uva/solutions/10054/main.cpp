#include <bits/stdc++.h>
using namespace std;
#define pii pair<int,int>
#define vii vector<pii>
#define vi vector<int>
#define tam 1010
#define fff first
#define sss second

vii adl[tam];
list<int> tour;
int loop[tam];
bool chked[tam];
bool visited[tam];

//Euler path/tour on a undirected graph
bool existEulerTour(){
    for(int i=0;i<tam;++i){
        if((int)adl[i].size()%2!=0) 
            return false;
    }
    return true;
}

void insert(int u, int v){
    if(v==u){
        ++loop[u];
        return;
    }
    adl[v].push_back({u,1});
    adl[u].push_back({v,1});
}

//Store an euler tour (starting from u) in linked list
void eulerTour(list<int>::iterator i, int u){
    for(int j=0;j<adl[u].size();++j){
        pii &v=adl[u][j];
        if(v.sss){
            v.sss=0;
            for(int k = 0; k < adl[v.first].size(); ++k){
                pii &uu=adl[v.first][k];
                if(uu.fff==u&&uu.sss){
                    uu.sss=0;
                    break;
                }
            }
            eulerTour(tour.insert(i,u),v.first);
        }
    }
}

void dfs(int v){
    visited[v]=true;
    //cout<<v<< " ";
    //rep(i,0,adl[v].size())
    for(int i=0;i<adl[v].size();++i)
        if(!visited[adl[v][i].fff])
            dfs(adl[v][i].fff);
}
int cnt=0;
bool conc(){
    for(int i=0;i<tam;++i){
        if(!visited[i]&&adl[i].size()>0){
            //cout<<"Connected component #"<<++cnt<<": ";
            ++cnt;
            if(cnt>=2) return false;
            dfs(i);
        }

    }
    //cerr<<"cnt: "<<cnt<<endl;
    return true;
}



int main(){
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    int tc=0;
    int a,b;
    int tourStart;
    int t;cin>>t;while(t--){
                
        tourStart=0;
        cnt=0;
        memset(loop,0,sizeof loop);
        memset(chked,false,sizeof chked);
        memset(visited,false,sizeof visited);

        int cc;cin>>cc;while(cc--){
            cin>>a>>b;
            tourStart=a;
            insert(a,b);
        }
        
        if(tc!=0)cout<<endl;
        cout<<"Case #"<<++tc<<endl;
        conc();

        if(cnt==1){
            if(existEulerTour()){
                eulerTour(tour.begin(),tourStart);
                tour.insert(tour.begin(),tourStart);
                for(auto i = tour.rbegin(); i != tour.rend(); ++i){
                    if(!chked[*i]&&loop[*i]>0){
                        for(int j = 0; j < loop[*i]; ++j){
                            cout<<*i<<" "<<*i<<endl;
                        }
                        chked[*i]=true;
                    }
                    if(next(i)==tour.rend()) break;
                    cout<<*i <<" "<<*next(i)<<endl;
                }
            }else cout<<"some beads may be lost"<<endl;
        }else if (cnt==0){
            int flag=0;
            for(int i = 0; i < tam; ++i){
                if(loop[i]>0)++flag;
                if(flag>1){
                    cout<<"some beads may be lost"<<endl;
                    break;
                }
            }
            if(flag==1){
                for(int i = 0; i < tam; ++i){
                    if(loop[i]>0){
                        for(int j = 0; j < loop[i]; ++j)
                            cout<<i<< " "<<i<<endl;
                    }
                }
            }
        }else cout<<"some beads may be lost"<<endl;

        for(int i = 0; i < tam; ++i){
            adl[i].clear();
        }
        tour.clear();
    }
    return 0;
}