#include <bits/stdc++.h>
using namespace std;

int n;
int arr[20];
bool taken[20];

bool prime(int x){
    for(int i=2;i<=x/2;++i) if(x%i==0) return false;
    return true;
}

void print(){
    for(int i = 0; i < n; ++i)
        cout<<arr[i]<<(i==n-1?"":" ");
    cout<<endl;
}

void solve(int id){
    if(id==n){
        if(prime(1+arr[n-1])) print();
        return;
    }
    for(int i=2;i<=n;++i){
        if(!taken[i]){
            taken[i]=true;
            arr[id]=i;
            if(prime(i+arr[id-1]))
                solve(id+1);
            taken[i]=false;
        }
    }
}

int main(){
    arr[0]=1;
    int tc=1;
    while(cin>>n){
        cout<<(tc==1?"":"\n");
        memset(taken,false,sizeof taken);
        cout<<"Case "<<tc++<<":"<<endl;
        solve(1);
    }

}