#include <bits/stdc++.h>
#define ff(i,n) for(int i=0;i<n;++i)
#define ll long long

using namespace std;

int n;
ll tgt;
ll a[110];
string sol;
short dp[101][32002*2];

bool solve(int id, ll cur, string ans){
    if(cur<-32000||cur>32000) return false;
    if(id==n){
        if(cur==tgt){
            sol=ans;
            return true;
        }
        return false;
    }
    auto &ref=dp[id][cur+32000];
    if(ref!=-1) return ref;

    string st=(id==0?"":"+");
    st=ans+st+to_string(a[id]);
    auto res1=solve(id+1,cur+a[id],st);
    if(id==0) return res1;
    if(res1) return ref=1;
    st=(id==0?"":"-");
    st=ans+st+to_string(a[id]);
    auto res2=solve(id+1,cur-a[id],st);
    if(res2) return ref=1;
    st=(id==0?"":"*");
    st=ans+st+to_string(a[id]);
    auto res3=solve(id+1,cur*a[id],st);
    if(res3) return ref=1;
    bool res4=false;
    if(cur%a[id]==0){
        st=(id==0?"":"/");
        st=ans+st+to_string(a[id]);
        res4=solve(id+1,cur/a[id],st);
    }

    return ref=(int)res4;
}

int main(){
    int t;cin>>t;while(t--){
        cin>>n;
        ff(i,n)
            cin>>a[i];
        memset(dp,-1,sizeof dp);
        cin>>tgt;
        if(solve(0,0,"")) cout<<sol<<"="<<tgt;
        else cout<<"NO EXPRESSION";
        cout<<endl;
    }
    return 0;
}