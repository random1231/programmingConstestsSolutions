#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin
#define clear(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef pair<pair<int,int>,int> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;


int n,m;
const int tam=500;
int a[tam+10][tam+10];


int solve(int q1,int q2){
	//~ cout<<"queries: "<<q1<< " "<<q2;cout<<endl;
	int ans=0;
	for(int i=0; i<n;++i){
		int idx=lob(a[i],a[i]+m,q1)-a[i];
		
		int mxsq=min(m-idx,n-i);
		//~ cout<<idx<<" " <<mxsq<<endl;
		if(mxsq<=ans) continue;
		for(int j=mxsq; j>0;--j){
			//cout<<"i "<<idx+j-1<< " j "<<i+j-1<<endl;
			if(a[i+j-1][idx+j-1]<=q2){
				ans=max(ans,j);
				break;
			}
		}
	}
	return ans;
}

int main(){
	while(cin>>n>>m){
		if(m==0&&n==0)break;
		for(int i=0; i<n;++i){
			for(int j=0; j<m;++j){
				cin>>a[i][j];
				//~ cout<<a[i][j]<< " " ;
			}
			//~ cout<<endl;
		}
		
		int query;cin>>query;
		//~ cout<<"query:" <<query;cout<<endl;
		for(int i=0; i<query;++i){
			int q1,q2;
			cin>>q1>>q2;
			cout<<solve(q1,q2);
			cout<<endl;
		}
		cout<<"-";
		cout<<endl;
	}
	
	
	return 0;
}
