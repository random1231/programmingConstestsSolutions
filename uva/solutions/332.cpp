#include <bits/stdc++.h>
using namespace std;

int main(int argc, char const *argv[])
{
	int n=0;
	string s;
	int i = 1;
	while(cin >> n && n!=-1){
		cin >> s;
		int k=0;
		s.erase(s.begin());
		s.erase(s.begin());
		auto d= stoll(s);
		k = s.size()-n;
		long long ns = 0;
		if(k!=0)
			ns = stoll(s.substr(0, k));
		int num = d;
		int den = pow(10, k+n);
		if(n!=0){
			num-=ns;
			den-=pow(10, k);
		}
		int g = __gcd(den, num);
		cout << "Case "<<i<<": " <<num/g <<"/"<<den/g<<endl;
		++i;
	}
	return 0;
}