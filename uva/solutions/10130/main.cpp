#include <bits/stdc++.h>
using namespace std;

#define MAX_N 1010
#define MAX_W 40

int n, V[MAX_N], W[MAX_N], memo[MAX_N][MAX_W];

int solve(int id, int w){
    if (id == n||w == 0)
        return 0;
    if (memo[id][w] != -1)
        return memo[id][w];
    if (W[id] > w)
        return memo[id][w] = solve(id + 1, w);
    return memo[id][w] = max(solve(id + 1, w), V[id] + solve(id + 1, w - W[id]));
}

int main(){
    int t;cin>>t;while(t--){
        int ans=0;
        memset(memo,-1,sizeof memo);
        cin>>n;for(int i = 0; i < n; ++i){
            cin>>V[i];
            cin>>W[i];
        }
        int p;cin>>p;while(p--){
            int cap;
            cin>>cap;
            ans+=solve(0,cap);
        }

        cout<<ans;
        cout<<endl;
        

    }
    return 0;
}