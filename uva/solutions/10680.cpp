#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
vector<long long> primes;
bitset<999989> bs;
double logs[1000001];

void pre(){
    for(int i=2;i<=1000000;++i)
        logs[i]=log10(i);
}

void sieve(long long upperbound){
    sieve_size = upperbound;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.push_back(i);
	    }
}

long long multmod(long long a, long long b, long long c){
    long long x = 0,y=a%c;
    while(b > 0){
        if(b%2 == 1)
            x = (x+y)%c;
        y = (y*2)%c;
        b /= 2;
    }
    return x%c;
}

long long expmod(long long a, long long b, long long c){
	long long res = 1;
	long long x = a%c;
	while(b > 0){
		if(b%2==1)
		//	res = multmod(res, x, c);
            res=((res%c)*x%c)%c;
		//x=multmod(x, x, c);
        x=((x%c)*x%c)%c;
		b/=2;
	}
	return res;
}

int main(){
    int n;
    sieve(999983);
    pre();
    int i=0;
    while(scanf("%i",&n)!=EOF&&n){
        int ix=1;
        auto p = primes[ix];
        auto tmp1=to_string(logs[n]/logs[2]);
        auto tmp2=to_string(logs[n]/logs[5]);
        auto expof2 = stoi(tmp1)-stoi(tmp2);
        auto ans=expmod(2, expof2, 10);
        while(p<=n){
            long long tmp=1;
            auto ex=(int)(logs[n]/logs[p]);
            if(p>n/2)
                tmp = p%10;
            else
                tmp = expmod(p, ex, 10);
            ans=(ans*tmp)%10;
            ++ix;
            if(ix>=primes.size())
                break;
            p=primes[ix];
            if(p==5)
                p=primes[++ix];
        }
        printf("%lli\n", ans);
    }
    return 0;
}
