#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
const int tam=103;

int dir[2]={1,-1};
int a[tam][tam];
bool visited[tam][tam];
int res[tam][tam];
int r,c,m,n;

void dfs(int x,int y){
	if(visited[x][y]) return;
	visited[x][y]=1;
	
	for(int i=0; i<2;++i){
		for(int j=0; j<2;++j){
			int p=x+dir[i]*m;
			int q=y+dir[j]*n;
			if(p>=0&&p<r&&q>=0&&q<c&&a[p][q]!=-1){
				++res[p][q];
				dfs(p,q);
			}
			p=x+dir[i]*n;
			q=y+dir[j]*m;
			if(p>=0&&p<r&&q>=0&&q<c&&a[p][q]!=-1){
				++res[p][q];
				dfs(p,q);
			}
		}
	}
}

int main(){
	ios::sync_with_stdio(false);
	int tc;cin>>tc;
	for(int i=0; i<tc;++i){
		mem(a,0);
		mem(visited,0);
		mem(res,0);
		cin>>r>>c>>m>>n;
		int w;
		cin>>w;
		for(int j=0; j<w;++j){
			int aa,bb;
			cin>>aa>>bb;
			a[aa][bb]=-1;
		}
		cout<<"Case "<<i+1<<": ";
		dfs(0,0);
		int even=0,odd=0;
		for(int j=0; j<r;++j){
			for(int k=0; k<c;++k){
				if(m==0||n==0||n==m) res[j][k]/=2;
				
				if(res[j][k]||(j==0&&k==0)){	
					if(res[j][k]%2==0) ++even;
					else ++odd;
				}
			}
		}
		
		cout<<even<< " "<<odd;
		cout<<endl;
	}
	
	
	return 0;
}
