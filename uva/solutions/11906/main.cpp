#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int r,c,m,n;
int a[101][101];
bool visited[101][101];

//-1 up
//1 down
//1 right
//-1 left
//xxx true if (m,n) false otherwise
bool valid(int x,int y,bool xxx,int dir1,int dir2){
	if(visited[x][y]) return false;
	int dx=m;
	int dy=n;
	if(!xxx){
		dx=n;
		dy=m;
	}
	
	//~ if(x==2&&y==1){
		 //~ cout<<xxx<< " "<<dir1<<" "<<dir2<<endl;
		 //~ cout<<x+(dir1*dx)<<" "<<y+(dir2*dy)<<endl;
	 //~ }
	
	if(x+(dir1*dx)<0||x+(dir1*dx)>=r) return false;
	if(y+(dir2*dy)<0||y+(dir2*dy)>=c) return false;
	auto tmpx=x;
	auto tmpy=y;
	bool can=true;
	for(int i=1; i<=dx;++i){
		tmpx+=dir1;
		if(a[tmpx][tmpy]==-1){
			can=false;
			break;
		}
	}
	if(can){
		for(int i=1; i<=dy;++i){
			tmpy+=dir2;
			if(a[tmpx][tmpy]==-1){
				can=false;
				break;
			}
		}
	}
	if(can){
		visited[tmpx][tmpy]=true;
		return true;
	}
	
	tmpx=x;
	tmpy=y;
	for(int i=1; i<=dy;++i){
		tmpy+=dir2;
		if(a[tmpx][tmpy]==-1){
			return false;
		}
	}
	
	
	for(int i=1; i<=dx;++i){
		tmpx+=dir1;
		if(a[tmpx][tmpy]==-1){
			return false;
		}
	}
	//~ cout<<x<< " "<<y;cout<<endl;
	visited[tmpx][tmpy]=true;
	return true;
}

ii solve(){
	int even=0,odd=0;
	for(int i=0; i<r;++i){
		for(int j=0; j<c;++j){
			if(a[i][j]==-1) continue;
			int res=0;
			mem(visited,false);
			if(valid(i,j,true,-1,1)) ++res;
			if(valid(i,j,true,-1,-1)) ++res;
			if(valid(i,j,true,1,1)) ++res;
			if(valid(i,j,true,1,-1)) ++res;
			if(valid(i,j,false,-1,1)) ++res;
			if(valid(i,j,false,-1,-1)) ++res;
			if(valid(i,j,false,1,1)) ++res;
			if(valid(i,j,false,1,-1)) ++res;
			if(res!=0){
				if(res%2==0) ++even;
				else ++odd;
			}
			//~ cout<<i<<","<<j<<": "<<res<<endl;
		}
		
		
		
	}
	return make_pair(even,odd);
}

int main(){
	ios::sync_with_stdio(false);
	int tc;cin>>tc;
	for(int i=0; i<tc;++i){
		mem(a,0);
		cin>>r>>c>>m>>n;
		int w;
		cin>>w;
		for(int j=0; j<w;++j){
			int aa,bb;
			cin>>aa>>bb;
			a[aa][bb]=-1;
		}
		cout<<"Case "<<i+1<<": ";
		auto ans=solve();
		cout<<ans.ff<< " "<<ans.ss;
		cout<<endl;
	}
	
	
	return 0;
}
