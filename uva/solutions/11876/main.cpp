#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin
#define clear(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef pair<pair<int,int>,int> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;

ll sievesz;
bitset<1000005> bs;
vi primes;

void sieve(ll upper){
	sievesz=upper+1;
	
	bs.set();
	
	bs[0]=bs[1]=0;
	for(ll i=2; i<=sievesz;++i){
		if(bs[i]){
			for(ll j=i*i; j<=sievesz;j+=i){
				bs[j]=0;
			}
			
			primes.pb((int)i);
		}
	}
}

ll numDiv(ll n){
	ll pfidx=0,pf=primes[pfidx],ans=1;
	while(pf*pf<=n){
		ll power=0;
		while(n%pf==0){
			n/=pf;
			++power;
		}
		
		ans*=(power+1);
		pf=primes[++pfidx];
	}
	
	if(n!=1){
		ans*=2;
	}
	
	return ans;
}

ll seq[1000009];
void genseq(){
	seq[0]=1;
	for(int i=1; i<=1000000;++i){
		seq[i]=seq[i-1]+numDiv(seq[i-1]);
	}
	
}
int a,b,t;

ll solve(){
	ll x=lob(beg(seq),end(seq),a)-seq;
	ll y=upb(beg(seq),end(seq),b)-seq;
//	cout<<x<< " "<<y<<endl;
	return y-x;
}

int main(){
	sieve(1000002);
	genseq();
	//cout<<"---"<<endl;
	
	cin>>t;
	for(int i=1; i<=t;++i){
		cin>>a>>b;
		cout<<"Case "<<i<<": ";
		cout<<solve();
		cout<<endl;
	}
	return 0;
}
