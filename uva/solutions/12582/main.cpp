#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

string s;
map<char,int> ans;

int solve(){
	map<char,ii> m;
	for(int i=0; i<s.sz;++i){
		char item=s[i];
		if(m.find(item)==m.en){
			m[item].ff=i;
		}else m[item].ss=i;
	}

	for(auto item:m){
		char cur=item.ff;
		int i=m[cur].ff+1;
		while(1){
			if(i==item.ss.ss){
				if(item.ss.ff!=0){
					++ans[item.ff];
				}
				break;
			}
			++ans[item.ff];
			int xxx=m[s[i]].ss+1;
			cur=s[xxx];	
			i=xxx;
		}
	}
	return 0;
}

int main(){
	ios::sync_with_stdio(false);
	int tc;
	cin>>tc;
	for(int k=0; k<tc;++k){
		cin>>s;
		cout<<"Case "<<k+1<<endl;
		solve();
		for(auto item:ans){
			cout<<item.ff<< " = "<<item.ss<<endl;
		}
		ans.clear();
	}
	return 0;
}
