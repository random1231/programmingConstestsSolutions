#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
const int tam=10000;
vi adl[tam+10];
vi incoming[tam+10];

vi stck;
int low[tam+10];
int num[tam+10];
bool visited[tam+10];
int cnt=0;
map<int,set<int>> strcc;

void scc(int node){
	low[node]=++cnt;
	num[node]=low[node];
	stck.pb(node);
	visited[node]=true;
	for(int i=0; i<(int)adl[node].sz;++i){
		int v=adl[node][i];
		if(num[v]==-1) scc(v);
		if(visited[v]) low[node]=min(low[v],low[node]);
	}

	if(low[node]==num[node]){
		while(1){
			int v=stck.back();
			stck.pop_back();
			visited[v]=false;
			strcc[node].insert(v);
			if(v==node) break;
		}
	}
}

int main(){
	ios::sync_with_stdio(false);
	int tc=1;
	int t;cin>>t;while(t--){
		cnt=0;
		mem(num,-1);
		mem(visited,false);
		int n,m;
		cin>>n>>m;
		for(int i=0; i<m;++i){
			int a,b;
			cin>>a>>b;
			adl[a].pb(b);
			incoming[b].pb(a);
		}

		for(int item=1; item<=n;++item){
			if(num[item]==-1){
				scc(item);
			}
		}

		int ans=0;
		for(auto item:strcc){
			auto tmp=true;
			for(auto item2:item.ss){
				for(int i=0; i<(int)incoming[item2].sz;++i){
					if(item.ss.find(incoming[item2][i])==item.ss.en){
						tmp=false;
						break;
					}
				}
				if(!tmp) break;
			}
			if(tmp) ++ans;
		}
		cout<<"Case "<<tc++<<": ";
		cout<<ans;cout<<endl;
		strcc.clear();

		for(int item=1; item<=n;++item){
			adl[item].clear();
			incoming[item].clear();
		}
	}
	return 0;
}
