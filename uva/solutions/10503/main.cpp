#include <bits/stdc++.h>
using namespace std;

int a[20];
int b[20];
bool taken[20];
int n,m;
int x,xx,y,yy;

int solve(int curx,int cury,int id){
    if(id==n){
        if(cury==xx) return true;
    }
    bool ans=false;
    for(int i = 0; i < m; ++i){
        if(!taken[i]){
            if(cury==a[i]){
                taken[i]=true;
                ans|=solve(a[i],b[i],id+1);
                taken[i]=false;
            }
            else if(cury==b[i]){
                taken[i]=true;
                ans|=solve(b[i],a[i],id+1);
                taken[i]=false;
            }

        }
    }

    return ans;
}

int main(){
    while(cin>>n&&n){
        cin>>m;
        cin>>x>>y;
        cin>>xx>>yy;
        memset(taken,false,sizeof taken);
        for(int i = 0; i < m; ++i){
            cin>>a[i]>>b[i];
        }
        cout<<(solve(x,y,0)==true?"YES":"NO");
        cout<<endl;
    }
    return 0;

}