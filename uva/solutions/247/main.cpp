#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
const int tam=25;

vi stck;
int low[tam+10];
int num[tam+10];
bool visited[tam+10];
int cnt=0;
map<int,vi> strcc;
map<string,int> names;
string hashed[tam+10];
vi adl[tam+10];

int hashnum=0;


int hashit(string x){
	if(names.find(x)==names.en){
		++hashnum;
		names[x]=hashnum;
		hashed[hashnum]=x;
		return hashnum;
	}
	return names[x];
	
}

void scc(int node){
	low[node]=++cnt;
	num[node]=low[node];
	stck.pb(node);
	visited[node]=true;
	for(int i=0; i<(int)adl[node].sz;++i){
		int v=adl[node][i];
		if(num[v]==-1) scc(v);
		if(visited[v]) low[node]=min(low[v],low[node]);
	}

	if(low[node]==num[node]){
		while(1){
			int v=stck.back();
			stck.pop_back();
			visited[v]=false;
			strcc[node].pb(v);
			if(v==node) break;
			}
	}
}

int main(){
	ios::sync_with_stdio(false);
	
	int n,m;
	int tc=1;
	while(cin>>n>>m){
		if(n==0&&m==0)break;
		if(tc>1) cout<<endl;
		set<int> edges;
		cnt=0;
		hashnum=0;
		mem(visited,false);
		mem(num,-1);
		for(int i=0; i<m;++i){
			string s1,s2;cin>>s1>>s2;
			int num1=hashit(s1);
			int num2=hashit(s2);
			adl[num1].pb(num2);
			edges.insert(num1);
			edges.insert(num2);
		}
		
		for(auto item:edges){
			if(num[item]==-1)
				scc(item);
		}
		
		cout<<"Calling circles for data set "<<tc++<<":";cout<<endl;
		for(auto item:strcc){
			int siz=item.ss.sz;
			for(int i=0; i<siz;++i){
				cout<<hashed[item.ss[i]];
				if(i<siz-1) cout<< ", ";
			}
			cout<<endl;
		}
		
		strcc.clear();
		names.clear();
		for(auto item:edges){
			adl[item].clear();
		}
	}
	return 0;
}
