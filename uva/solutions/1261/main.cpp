#include <bits/stdc++.h>
using namespace std;

string st;
unordered_map<string,bool> m;
bool answer;

bool solve(string s){
    if(s==""){
        answer=true;
        return true;
    }
    if(m.find(s)!=m.end()) return m[s];

    int len=s.size();
    bool ans=false;
    for(int i=0;i<len;){
        char cur=s[i];
        int cnt=1;
        bool fl=false;
        bool fl2=false;
        for(int j = i+1; j <= len; ++j){
            if(j<len&&cur==s[j]){
                ++cnt;
            }else fl=true;
            if(fl&&cnt>=2){
                auto val=solve(s.substr(0,i)+s.substr(j));
                m[s]=val;
                ans=val;
                i=j;
                fl2=true;
                break;
            }else if(fl&&cnt<2)break;
        }
        if(!fl2) ++i;
    }
    return ans;
}


int main(){
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    int t;cin>>t;while(t--){
        answer=false;
        cin>>st;
        solve(st);
        if(answer) cout<<"1";
        else cout<<"0";
        cout<<endl;
        m.clear();
    }

    return 0;
}