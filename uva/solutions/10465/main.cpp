#include <bits/stdc++.h>
#define MAXN 10
#define MAXW 10010
#define pii pair<int,int>

using namespace std;

pii dp[MAXW];
int val[MAXN];
int w[MAXN];
int cap,n=2;

pii solve(int curw, int cnt){
    //cerr<<"curw: "<<curw<<"  cnt: "<<cnt<<endl;
    if(curw==0) return {0,1};
    auto &ref=dp[curw];
    if(ref.first!=-1)return ref;
    int best=0;
    int bestweight=0;
    for(int i = 0; i < n; ++i){
        auto res=curw-w[i];
        if(res>=0){
            auto ans=solve(res,cnt+1);
            auto curbest=ans.first+val[i];
            if(best==curbest){
                auto xx=ans.second;
                if(res>0)++xx;
                bestweight=max(bestweight,xx);
                // cerr<<"Same!"<<endl;
                // cerr<<bestweight<<" "<<ans.second+1<<endl;
            }
            else if(curbest>best){
                best=curbest;
                bestweight=ans.second;
                if(res>0)++bestweight;
            }
        }
    }
    // cerr<<"best of "<< curw<<","<<cnt<<endl;
    // cerr<<best<< " "<<bestweight<<endl;
    return ref={best,bestweight};
}

int main(){
    while(cin>>w[0]>>w[1]>>cap){
        val[0]=w[0];
        val[1]=w[1];
        for(int i = 0; i < MAXW; ++i)
            dp[i].first=-1;
        auto ans=solve(cap,0);
        cout<<ans.second;
        if(ans.first!=cap)
            cout<< " "<<cap-ans.first;
        cout<<endl;
    }
    return 0;
}