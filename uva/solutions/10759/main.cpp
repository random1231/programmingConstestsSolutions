#include <bits/stdc++.h>
#define ull unsigned long long
#define ll long long

using namespace std;


ll n,x;
ll dp[25][145];

ll solve(int dice,int score){
//	cout<<dice<< " "<<score<<endl;
	if(dice==0){
		if(score>=x) return 1;
		return 0;
	}
	auto &ref=dp[dice][score];
	if(ref!=-1) return ref;
	ll ans=0;
	for(ll i=1;i<=6;++i){
		ans+=solve(dice-1,score+i);
	}
	
	return ref=ans;
}

ll binexp(ll a,ll b){
	if(b==0) return 1;
	if(b%2==0){
		ll temp=binexp(a,b/2);
		return temp*temp;
	}else{
		ll temp=binexp(a,b-1);
		return temp*a;
	}
}
	

int main(){
	while(cin>>n>>x){
		if(n==0&&x==0) break;
		memset(dp,-1,sizeof dp);
		ll den=binexp(6,n);
		//cerr<<den<<endl;
		auto res=solve(n,0);
		if(res==0){
			cout<<"0"<<endl;
			continue;
		}
		if(res/den==1){
			cout<<"1"<<endl;
			continue;
		}
		ll g=__gcd(den,res);
		//cerr<<res<<endl;
		cout<<(res/g)<<"/"<<(den/g)<<endl;
	}
	return 0;
}
