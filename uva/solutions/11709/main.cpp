#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
const int tam=1000;
vi adl[tam+10];
vi incoming[tam+10];

vi stck;
int low[tam+10];
int num[tam+10];
bool visited[tam+10];
int cnt=0;
map<int,set<int>> strcc;

unordered_map<string, int> names;

int hashcnt=0;
void hashit(string s){
	names[s]=++cnt;
}

void scc(int node){
	low[node]=++cnt;
	num[node]=low[node];
	stck.pb(node);
	visited[node]=true;
	for(int i=0; i<(int)adl[node].sz;++i){
		int v=adl[node][i];
		if(num[v]==-1) scc(v);
		if(visited[v]) low[node]=min(low[v],low[node]);
	}

	if(low[node]==num[node]){
		while(1){
			int v=stck.back();
			stck.pop_back();
			visited[v]=false;
			strcc[node].insert(v);
			if(v==node) break;
		}
	}
}

int main(){
	ios::sync_with_stdio(false);
	
	int n,m;
	while(cin>>n>>m){
		cnt=0;
		mem(visited,false);
		mem(num,-1);
		if(n==0&&m==0) break;
		for(int i=0; i<n;++i){
			string s1,s2;cin>>s1>>s2;
			s1.pop_back();
			s1.append(s2);
			hashit(s1);
		}
		for(int i=0; i<m;++i){
			string s1,s2;cin>>s1>>s2;
			s1.pop_back();
			s1.append(s2);
			string s3,s4;cin>>s3>>s4;
			s3.pop_back();
			s3.append(s4);
			adl[names[s1]].pb(names[s3]);
		}
		for(auto item:names){
			if(num[item.ss]==-1){
				scc(item.ss);
			}
		}
		
		cout<<(int)strcc.sz;cout<<endl;
		for(auto item:names){
			adl[item.ss].clear();
		}
		strcc.clear();
		names.clear();
	}
	
	
	return 0;
}
