#include <bits/stdc++.h>

using namespace std;

//We use this function to avoid overflow.
 unsigned long long multmod(unsigned long long a,unsigned long long b,unsigned long long c){
    unsigned long long x = 0,y=a%c;
    while(b > 0){
        if(b%2 == 1){
            x = (x+y)%c;
        }
        y = (y*2)%c;
        b /= 2;
    }
    return x%c;
}

 unsigned long long exp(unsigned long long a, unsigned long long b, unsigned long long c){
	unsigned long long res = 1;
	unsigned long long x = a%c;

	while(b > 0){
		if(b%2==1)
			res = multmod(res, x, c);
		x=multmod(x, x, c);
		b/=2;
	}
	return res;
}

int main(int argc, char const *argv[])
{
	unsigned long long a, b, c;
	while(cin >> a >> b >> c){
		cout << exp(a, b, c) << endl;
	}
	return 0;
}