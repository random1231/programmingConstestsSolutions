#include <bits/stdc++.h>

using namespace std;

int eulerPhi(long long n){
	if(n==1)
		return 0;
	long long ans = n;
	if(n%2==0){
        while(n%2==0)
            n/=2;
		ans -= (ans/2);
	}
	for(long long i = 3; i*i <= n; i+=2){
		if(n%i==0)
			ans -= (ans/i);
		while(n%i == 0){
			n /= i;
		}
	}

	if (n > 1)
		ans-=(ans/n);
	return ans;
}

int main(int argc, char const *argv[])
{
	long long n;
	while(cin>>n&&n){
		cout << eulerPhi(n)<<std::endl;;
	}
	return 0;
}