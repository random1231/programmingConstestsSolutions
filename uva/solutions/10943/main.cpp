#include <bits/stdc++.h>
#define maxn 101
#define maxk 101
using namespace std;

int memo[maxn][maxk];

int solve(int n, int k){
    if(k==1) return 1;
    if(memo[n][k]!=-1) return memo[n][k];
    int ways=0;
    for(int i = 0; i <=n; ++i){
        ways=(ways+solve(n-i,k-1))%1000000;
    }
    return memo[n][k]=ways;
}

int main(){
    int n,k;
    memset(memo,-1,sizeof memo);
    while(cin>>n>>k&&n>0&&k>0){
        cout<<solve(n,k)<<endl;
    }
    return 0;
}