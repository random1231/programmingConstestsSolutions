#include <bits/stdc++.h>
#define piv pair<int,vector<int>>
using namespace std;

int t,w,n,a[100],b[100];
map<pair<int,int>, pair<int,vector<int>> >m;


piv solve(int id, int cur, vector<int> v){
    if(id==n||cur==0) return {0,v};
    //if(m.find({id,cur})!=m.end()) return m[{id,cur}];
    int weight=w*a[id]*3;
    if(weight>cur) return solve(id+1,cur,v);
    auto res1=solve(id+1,cur,v);
    v.push_back(id+1);
    auto res2=solve(id+1,cur-weight,v);
    auto val1=res1.first;
    auto val2=res2.first+b[id];
    if(val1>val2) return m[{id,cur}]=res1;
    else return m[{id,cur}]=res2;
}

int main(){
    while(cin>>t>>w){
        cin>>n;
        for(int i=0;i<n;++i) cin>>a[i]>>b[i];
        vector<int> v;
        auto ans=solve(0,t,v);
        cout<<ans.first<<endl;
        for(auto item : ans.second){
            cout<<a[item] << " "<<b[item]<<endl;
        }
    }


    return 0;
}