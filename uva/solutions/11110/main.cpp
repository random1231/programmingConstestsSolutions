#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;


int n;
int a[101][101];
map<int,bool> visited;

int dr[]={-1,0,1,0};
int dc[]={0,1,0,-1};
bool bad;
bool done;

int solve(int x,int y,int cur){
	if(x<0||x>=n||y<0||y>=n) return 0;
	if(a[x][y]==-1) return 0;
	if(a[x][y]!=cur) return 0;
	//~ cout<<x<<" " <<y<<" " <<cur<< " "<<visited[cur];cout<<endl;
	//~ if(visited[cur]==true){
		//~ cout<<"asas";
		//~ bad=true;
		//~ return 0;
	//~ }
	a[x][y]=-1;
	for(int i=0; i<4;++i){
		solve(x+dr[i],y+dc[i],cur);
	}
	
	done=true;
	
	return 0;
}

int main(){
	ios::sync_with_stdio(false);
	
	while(cin>>n&&n>0){
		mem(a,-1);
		string s;
		int x,y;
		int cnt=1;
		getline(cin,s);
		//~ cout<<c<<endl;
		for(int i=0; i<n-1;++i){
			getline(cin,s);
			//~ cout<<s<<endl;
			stringstream strs(s);
			while(strs>>x>>y){
				a[--x][--y]=cnt;
				//~ cout<<x<<" " <<y<<endl;
			}
			visited[cnt]=false;
			++cnt;
		}
		visited[cnt]=false;
		
		//~ for(auto item:visited){
			//~ cout<<item.ff<< " "<<item.ss;cout<<endl;
		//~ }
		
		
		for(int i=0; i<n;++i){
			for(int j=0; j<n;++j){
				if(a[i][j]==-1) a[i][j]=cnt;
			}
		}
		
		//~ for(int i=0; i<n;++i){
			//~ for(int j=0; j<n;++j){
				//~ cout<<a[i][j];
			//~ }
			//~ cout<<endl;	
		//~ }
		bad=false;
		for(int k=1; k<=cnt;++k){
			done=false;
			for(int i=0; i<n;++i){
				for(int j=0; j<n;++j){
					solve(i,j,k);
					if(bad) break;
					if(done){
						//~ cout<<i<< " "<<j<<" " <<k<<endl;
					break;
				}
				}
				if(bad) break;
				if(done) break;
			}
			if(bad) break;
			visited[k]=true;
		}
		
		for(int i=0; i<n;++i){
			for(int j=0; j<n;++j){
				if(a[i][j]!=-1){
					bad=true;
					break;
				}
			}
		}
		
		if(bad){
			cout<<"wrong";
		}else cout<<"good";
		cout<<endl;
		visited.clear();
	}
	return 0;
}
