#include <bits/stdc++.h>

using namespace std;

int a[55];
int dp[55][55];
int sz;
int n;

int solve(int l,int r){
    if(r-l==1) return 0;
    auto &ref=dp[l][r];
    if(ref!=-1) return ref;
    int ans=INT_MAX;
    for(int i = l+1; i < r; ++i){
        ans=min(ans,(a[r]-a[l]) + solve(l,i) + solve(i,r));
    }
    return ref=ans;
}

int main(){
    while(cin>>n&&n){
        memset(dp,-1,sizeof dp);
        cin>>sz;
        for(int i = 1; i <= sz; ++i){
            cin>>a[i];
        }
        a[0]=0;
        a[sz+1]=n;
        auto ans=solve(0,sz+1);
        cout<<"The minimum cutting is "<<ans<<"."<<endl;
    }
    return 0;
}