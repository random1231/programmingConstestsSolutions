#include <bits/stdc++.h>

using namespace std;

int maxnode;

int solve(int balls,int node){
	int left=node*2;
	int right=left+1;
	
	if(right<maxnode&&left<maxnode){
		if(balls%2==0) solve(balls/2,right);
		else solve((balls/2)+1,left);
	}else return node;
}

int main(){
	int t;cin>>t;
	int d,i;
	while (t--){
		cin>>d>>i;
		maxnode=pow(2.0,d);
		cout<<solve(i,1)<<endl;
	}
	cin>>t;
	return 0;
}
