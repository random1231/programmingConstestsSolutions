#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
const int tam=50000;
vi adl[tam+10];
bool visited[tam+10];
bool visited2[tam+10];
set<int> vertices;
int n;
int curmax;

void dfs(int node,int cnt){
	if(visited[node]) return;
	//~ cout<<node<< " "<<cnt<<endl;
	visited[node]=true;
	visited2[node]=true;
	++cnt;
	auto &ref=adl[node];
	for(int i=0; i<(int)ref.sz;++i){
		dfs(ref[i],cnt);
	}
	curmax=max(cnt,curmax);
	//~ cout<<curmax
	visited[node]=false;
}

int solve(){
	int ans=-1;
	int tmp=0;
	mem(visited,false);
	mem(visited2,false);
	for(auto item:vertices){
		if(visited2[item]){
			continue;
		}
		//~ cout<<"----" ;
		curmax=0;
		dfs(item,0);
		if(curmax>tmp){
			ans=item;
			tmp=curmax;
		}
	}
	return ans;
}

int main(){
	ios::sync_with_stdio(false);
	int tc=1;
	int t;cin>>t;while(t--){
		cin>>n;
		for(int i=0; i<n;++i){
			int a,b;cin>>a>>b;
			adl[a].pb(b);
			vertices.insert(a);
		}
		cout<<"Case "<<tc++<<": ";
		cout<<solve();cout<<endl;
		for(auto item:vertices){
			adl[item].clear();
		}
		vertices.clear();
	}
	
	
	return 0;
}
