#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
bitset<10000009> bs;
set<long long> primes;

void sieve(long long upperbound){
    sieve_size = upperbound;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.insert(i);
	    }
}

int main(){
    int n;
    sieve(32778);
    while(cin>>n&&n){
        auto it=primes.lower_bound(n);
        --it;
        int ans=0;
        while(*it>=2){
            auto x=n-(*it);
            if(primes.count(x)!=0){
                if((*it)*2!=n)
                    ++ans;
                else
                    ans+=2;
            }
            if(*it==2)
                break;
            --it;

        }

        cout << ans/2<<endl;
    }

	return 0;
}
