#include <bits/stdc++.h>

using namespace std;
//We use this function to avoid overflow.
long long multmod(long long a, long long b, long long c){
    long long x = 0,y=a%c;
    while(b > 0){
        if(b%2 == 1){
            x = (x+y)%c;
        }
        y = (y*2)%c;
        b /= 2;
    }
    return x%c;
}

long long expmod(long long a, long long b, long long c){
	long long res = 1;
	long long x = a%c;

	while(b > 0){
		if(b%2==1)
			res = multmod(res, x, c);
		x=multmod(x, x, c);
		b/=2;
	}
	return res;
}

string firstdigits(long a,long b){
	long double x=(b*1.0)*log10(a);
	long double ex=x-trunc(x);
	auto res=pow(10, ex);
	//auto tmp=(int)log10(res)+1;
	string ans;
	int i=0;
	auto s=to_string(res);
	for(auto c:s){
		if(c!='.'){
			++i;
			ans+=c;
		}
		if(i==3)
			break;
	}

	return ans;

}

int main(int argc, char const *argv[])
{
	int t;
	cin>>t;
	while(t--){
		long long a, b;
		cin>>a>>b;
		auto s=firstdigits(a, b);
		auto x=expmod(a, b, 1000);
		auto res=to_string(x);
		for(int i=0;i<res.size()%3;++i)
			res.insert(res.begin(), '0');
		cout << s<<"..."<<res<<endl;
	}
	return 0;
}