#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
vector<long long> primes;
bitset<20000009> bs;

void sieve(long long upperbound){
    sieve_size = upperbound+1;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.push_back(i);
	    }
}

void pre(std::vector<long long> &twin){
	sieve(20000001);
	for(int i = 0; i < primes.size()-1; ++i){
		if(primes[i+1]-primes[i] == 2)
			twin.push_back(primes[i]);
	}
}

int main(){

	int n;
	std::vector<long long> v;
	pre(v);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	while(cin >> n){
		cout << "(" << v[n-1] << ", " << v[n-1]+2<< ")" <<std::endl;
	}
}