#include <bits/stdc++.h>

using namespace std;
int n,k,m;

long long dp[100][100][100];

long long solve(int curm, int curk, int cnt, int id){
    if(curm>m||curk>k) return 0;
    if(cnt==n){
        if(curk==k) return 1;
        return 0;
    }
    auto &ref=dp[curm][curk][cnt];
    if(ref!=-1) return ref;
    if(id==0)
        return ref=solve(curm+1,curk,cnt+1,0)+solve(1,curk+1,cnt+1,1);
    else
        return ref=solve(curm+1,curk,cnt+1,1)+solve(1,curk+1,cnt+1,0);
}

int main(){
    while(cin>>n>>k>>m){
        memset(dp,-1,sizeof dp);
        if(n==1&&k==1) cout<<"1"<<endl;
        else
            cout<<solve(2,1,2,1)+solve(1,2,2,0)<<endl;
    }
    return 0;
}