from math import sqrt

sieve_size = 0
primes=[]
flags = [True]*50009

def sieve(upperbound):
	sieve_size = upperbound+1
	flags[0] = False
	flags[1] = False
	for i in range(2, sieve_size+1):
		if flags[i] == True:
			for j in range (i*i, sieve_size+1, i):
				flags[j] = False
			primes.append(i)

def primeFactors(n):
	lim = sqrt(n)
	factors =[]
	index=0
	primeF=primes[index]
	while(primeF<=lim):
		rep = 0
		while(n%primeF==0):
			n//=primeF
			factors.append(primeF)
		index+=1
		primeF=primes[index]
	if n!=1:
		factors.append(n)
	return factors


def main():
	sieve(50000)
	while True:
		l = int(input())
		if l==0:
			break
		if l == 1:
			print("1 = 1")
			continue
		fl=False
		if l<0:
			fl=True
			l*=-1
		ans = primeFactors(l)
		if fl == True:
			ans.insert(0, -1)
		print(str(l if fl == False else -l)+" = ", end="")
		for x in range(len(ans)):
			print(str(ans[x]), end="")
			if x != len(ans)-1:
				print(" x ", end="")
		print()

main()