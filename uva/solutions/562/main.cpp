#include <bits/stdc++.h>
using namespace std;
//Set the numbers for the problem
#define MAX_N 110
#define MAX_W 510

int n, cap,W[MAX_N], memo[MAX_N][MAX_W];

int solve(int id, int w){
    if (id == n||w == 0)
        return cap-w;
    if (memo[id][w] != -1)
        return memo[id][w];
    if (W[id] > w)
        return memo[id][w] = solve(id + 1, w);
    return memo[id][w] = max(solve(id + 1, w), solve(id + 1, w - W[id]));
}

int main(){
    int t;cin>>t;while(t--){
        memset(memo,-1,sizeof memo);
        int ss=0;
        int nn;cin>>nn;for(int i = 0; i < nn; ++i){
            cin>>W[i];
            ss+=W[i];
        }
        n=nn;
        cap=ss/2;
        int xx=solve(0,cap);
        int yy=ss-xx;
        cout<<abs(xx-yy)<<endl;
    }
    return 0;
}