#include <bits/stdc++.h>
#define pii pair<int,int>
using namespace std;

int price,m;
pii arr[29];
int k[101],tgt,cn;
int dp[100010];

int solve(int tg){
    for(int i = 1; i <= tgt; ++i){
        dp[i]=INT_MAX;
        for(int j = 0; j <= m; ++j){
            auto idx=i-arr[j].first;
            dp[i]=min(dp[i],dp[idx<0?0:idx]+arr[j].second);
        }
    }
    return dp[tgt];
}

int main(){
    int a,b;
    int t=1;
    while(scanf("%d.%d %d",&a,&b,&m)!=EOF){
        price=a*100+b;
        arr[0]={1,price};
        for(int i=1;i<=m;++i){
            int x,aa,bb;
            scanf("%d %d.%d",&x,&aa,&bb);
            arr[i]={x,aa*100+bb};
        }
        getchar();
        string s;
        getline(cin,s);
        stringstream ss(s);
        cout<<"Case "<<t++<<":"<<endl;
        memset(dp,0,sizeof dp);
        while(ss>>tgt){
            auto res=solve(tgt);
            string ans=to_string(res);
            cerr<<ans<<endl;
            cout<<"Buy "<<tgt<< " for $";
            //cout<<ans.substr(0, ans.size()-2)<<"."<<ans.substr(ans.size()-2, ans.size())<<endl;
            cout<<fixed<<setprecision(2)<<(res*1.0)/100<<endl;
        }
    }
    return 0;
}