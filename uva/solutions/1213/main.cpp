#include <bits/stdc++.h>
using namespace std;

int n, k, dp[1125][15][1125];

long long sieve_size;
bitset<100000009> bs;
vector<long long> primes;
int sz;

void sieve(long long upperbound){
    sieve_size = upperbound;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
        if(bs[i]){
            for (long long j = i*i; j <= sieve_size; j+=i)
                bs[j] = 0;
            primes.push_back(i);
        }
}

int solve(int id, int k, int nm){
    if(k==0){
        if(nm==0) return 1;
        return 0;
    }
    if(nm<0||id == sz) return 0;
    auto &ref=dp[id][k][nm];
    if (ref != -1) return ref;
    return ref = solve(id + 1, k-1,nm-primes[id]) + solve(id + 1, k,nm);
}

int main(){
    sieve(1125);
    memset(dp,-1,sizeof dp);
    while(cin>>n>>k&&n&&k){
        sz=upper_bound(primes.begin(), primes.end(),n)-primes.begin();
        cout<<solve(0,k,n)<<endl;
    }
}