#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
bitset<10000009> bs;
set<long long> primes;

void sieve(long long upperbound){
    sieve_size = upperbound;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.insert(i);
	    }
}

int main(int argc, char const *argv[])
{
	sieve(9999992);
	int n;
	while(cin>>n&&n){
		auto it=primes.lower_bound(n);
        if(*it==n)
		  ++it;
		auto lim=10;
		while(lim<=n){
            lim*=10;
		}
		auto fl=false;
		for(;it!=primes.end()&&*it<lim;++it){
            auto found=true;
            auto s=to_string(*it);
            sort(s.begin(), s.end());

            do{
                auto fl=stoi(s);
                if(primes.count(fl)==0){
                    found=false;
                    break;
                }
            }while(next_permutation(s.begin(), s.end()));
            if(found){
                fl=true;
                break;
            }
		}

		if(fl)
            cout << *it;
        else
            cout <<"0";
        cout <<endl;
	}
	return 0;
}
