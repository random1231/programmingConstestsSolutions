#include <bits/stdc++.h>

using namespace std;

double a[101];
int x,n;
map<pair<int,double>,double>dp;

double solve(int id, double curs){
    if(id==n){
        if(curs>50.0) return 100.0*(a[x-1]/(curs));
        return 0;
    }
    auto it=dp.find({id,curs});
    if(it!=dp.end()){ return dp[{id,curs}];}

    if(x-1==id)
        return dp[{id,curs}]=solve(id+1,curs);
    return dp[{id,curs}]=max(solve(id+1,curs),solve(id+1,curs+a[id]));
}

int main(){
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    while(cin>>n>>x&&n&&x){
        dp.clear();
        for(int i=0;i<n;++i)
            cin>>a[i];
        cout<<fixed<<setprecision(2)<<solve(0,a[x-1])<<endl;
    }
        



	return 0;
}

