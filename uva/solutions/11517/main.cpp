#include <bits/stdc++.h>
#define MAXN 101
#define MAXV 10001
#define INF 123456
using namespace std;

int n, cap;
int val[MAXN];
pair<int,int> dp2[MAXN][MAXV];

pair<int,int> solve2(int id, int w){
    if(w<=0)
        return {cap-w,0};
    if (id == n)
        return {INF,INF};
    if (dp2[id][w].first != -1)
        return dp2[id][w];
    auto ans1=solve2(id + 1, w);
    auto ans2=solve2(id + 1, w - val[id]);
    ++ans2.second;
    return dp2[id][w]=min(ans1,ans2);
}

int main(){
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    int t;cin>>t;while(t--){
        cin>>cap;cin>>n;
        for(int i = 0; i < n; ++i)
            cin>>val[i];
        for(int i = 0; i < MAXN; ++i){
            for(int j = 0; j < MAXV; ++j){
                dp2[i][j].first=-1;
                dp2[i][j].second=0;
            }
        }
        auto ans=solve2(0,cap);
        cout<<ans.first<< " "<<ans.second<<endl;
    }
    return 0;
}