#include <bits/stdc++.h>
using namespace std;

#define MAX_N 100000

int l[MAX_N], l_id[MAX_N], p[MAX_N];
int n;

vector<int> longestIS(int a[]){
    int lis = 0, lis_end = 0;
    for (int i = 0; i < n; ++i){
        int pos = lower_bound(l, l + lis, a[i]) - l;
        l[pos] = a[i];
        l_id[pos] = i; 
        p[i] = pos ? l_id[pos - 1] : -1;
        if (pos + 1 > lis){
            lis = pos + 1;
            lis_end = i;
        }
    }
    vector<int> v;
    int x=lis_end;
    for (; p[x] >= 0; x = p[x])
       v.push_back(x);
    v.push_back(x);
    return v;
}

int main(){
    vector<pair<pair<int,int>,int> > v;
    int cn=1;
    int f,g;while(cin>>f>>g){
        v.push_back({{f,g},cn++});
    }

    sort(v.begin(), v.end());
    n=v.size();
    int b[n];
    int c[n];
    for(int i = n-1,j=0; i >= 0; --i,++j){
        b[j]=v[i].first.second;
        c[j]=v[i].second;
    }
    auto ans=longestIS(b);
    cout<<ans.size()<<endl;
    for(auto x : ans){
        cout<<c[x]<<endl;
    }

    return 0;
}