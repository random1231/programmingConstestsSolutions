positions=[]
ans=0

def brute(pos, lim, flag):
	global ans
	x=positions[pos][0]
	y=positions[pos][1]
	currpos=x+y
	nPos=pos+1
	nLim=lim
	if flag==False:
		nLim-=2
	fl=True
	while currpos<=nLim:
		ans+=1
		if nPos<len(positions):
			x+=1
			brute(nPos,x, False)
		currpos+=1

def main():
	for _ in range(int(input())):
		global ans
		ans=0
		l=[int(x) for x in input().split()]
		n=l[0]
		k=l[1]
		del(l[0])
		del(l[0])
		if sum(l)+len(l) > n:
			print('0')
			continue
		i=1
		for x in l:
			positions.insert(0,(i,x))
			i+=x+1
		brute(0,n,True)
		print(ans+1)

main()