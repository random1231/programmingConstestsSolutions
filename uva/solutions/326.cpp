#include <bits/stdc++.h>
using namespace std;

int main(int argc, char const *argv[])
{
	int n=1;
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	while(cin >> n && n != 0){
		int table[n+2][n+2];
		std::vector<int> v, ans;

		for(int i=0;i<n;++i){
			int x;
			cin >> x;
			v.push_back(x);
			table[1][i+1] = x;
		}
		ans.push_back(v[v.size()-1]);
		int k;
		cin >> k;
		int j;
		for(int i=1;i<n;++i){
			for(j=1;j<=n-i;++j){
				table[i+1][j] = table[i][j+1]-table[i][j];
			}
			ans.push_back(table[i+1][j-1]);
		}
		auto tmp = k;
		while(tmp--){
			for(int i = ans.size()-2; i>=0;--i){
				ans[i]=ans[i]+ans[i+1];
			}
		}

		cout << "Term " << n+k << " of the sequence is " << ans[0] << std::endl;
	}
	return 0;
}