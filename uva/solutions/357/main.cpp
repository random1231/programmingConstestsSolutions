#include <bits/stdc++.h>

#define MAXN 6
#define MAXV 30001

using namespace std;

int n=5, val[]={1,5,10,25,50};
long long dp[MAXN][MAXV];

long long solve(int type, long long value){
    if (value == 0)
        return 1;
    if (value < 0 || type == n)
        return 0;
    if (dp[type][value] != -1)
        return dp[type][value];
    return dp[type][value] = solve(type + 1, value) + solve(type, value - val[type]);
}

int main(){
    memset(dp,-1,sizeof dp);
    long long xx;
    while(cin>>xx){
        auto ans=solve(0,xx);
        if(ans==1)
           cout<<"There is only 1 way to produce "<< xx<<" cents change.";
        else
            cout<<"There are "<<ans<<" ways to produce "<<xx<<" cents change.";
        cout<<endl;
    }
    return 0;
}
