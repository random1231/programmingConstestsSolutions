#include <bits/stdc++.h>
using namespace std;

long long cnk2(long long nn, long long kk) {
	const int maxn = nn;
	long long C[maxn+1][maxn+1];
	for (int n=0; n<=maxn; ++n) {
	    C[n][0] = C[n][n] = 1;
	    for (int k=1; k<n; ++k)
	        C[n][k] = C[n-1][k-1] + C[n-1][k];
	}
	return C[nn][kk];
}

int main(){
	int t;
	cin >> t;
	for(int l = 1;l<=t;++l){
		string s;
		cin >> s;
		string a, b;
		int i;
		for(i=1;s[i]!='+';a+=s[i], ++i);
		++i;
		for(;s[i]!=')';b+=s[i], ++i);
		string tem =s.substr(s.size()-2, string::npos);
		if(tem[0]=='^')
			tem.erase(tem.begin());
		int n= stoi(tem);
		cout << "Case "<<l<<": ";
		if(n==1){
			cout << a+"+"+b<<std::endl;;
			continue;
		}

		cout << a <<"^"<<n<<"+";
		for(int k = 1; k<n;++k){
			auto c = cnk2(n, k);
			string s1 = a+((n-k) != 1? ("^"+to_string(n-k)):"");
			string s2 = b+(k != 1? ("^"+to_string(k)):"");
			cout << c <<"*"<<s1<<"*"<<s2<<"+";
		}

		cout << b <<"^"<<n<<endl;;

	}
}