#include <bits/stdc++.h>

using namespace std;
unsigned long long v[51];

void pre(){
	v[0]=1;
	v[1]=2;
	for(int i = 2; i <=51;++i){
		v[i] = v[i-1]+v[i-2];
	}
}

int main(int argc, char const *argv[])
{
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	pre();
	int n;
	cin >> n;
	for(int i =1;i<=n;++i){
		int j;
		cin >>j;
		cout << "Scenario #"<<i<<":"<<endl<<v[j]<<endl<<endl;
	}

	
	return 0;
}