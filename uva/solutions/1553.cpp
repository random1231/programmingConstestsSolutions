//Uva 11553
#include <bits/stdc++.h>

using namespace std;

int grid[9][9];

int main(int argc, char const *argv[])
{
	int t;
	cin>>t;

	while(t--){
		int perms[]={0,1,2,3,4,5,6,7};
		int n;
		int mx=1001;
		cin>>n;
		for(int i=0;i<n;++i)
			for(int j=0;j<n;++j)
				cin>>grid[i][j];


		
		do{
			auto cur=0;
			for(int i=0;i<n;++i){
				cur+=grid[i][perms[i]];
			}
			if(cur<mx)
				mx=cur;
		}while(next_permutation(perms,perms+n));

		cout<<mx<<endl;

	}
	return 0;
}