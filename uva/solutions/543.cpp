#include <bits/stdc++.h>
using namespace std;

long long sieve_size;
set<int> primes;
bitset<10000001> bs;

void sieve(long long upperbound){
    sieve_size = upperbound+1;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.insert((int)i);
	    }
}

int main(int argc, char const *argv[])
{
	int n=1;
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	sieve(1000000);
	primes.erase(primes.begin());
	while(cin >> n && n != 0){
		int a, b;
		auto it = primes.lower_bound(n);
		--it;
		bool f = false;
		while(*it >=3){
			b = *it;
			a = n-b;

			if(primes.count(a)!=0){
				f = true;
				break;
			}
			if(*it == 3)
				break;
			--it;
		}

		if(f)
			cout << n << " = " << a << " + " << b;
		else
			cout << "Goldbach's conjecture is wrong.";
		cout << std::endl;

	}
	return 0;
}
