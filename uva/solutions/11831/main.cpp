#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

char a[105][105];

int n,m,s;
string str;
int startx,starty;
char orientation;
int ans;

bool validmove(int x,int y){
	if(x>=n||x<0||y>=m||y<0) return false;
	if(a[x][y]=='#') return false;
	if(a[x][y]=='*'){
		a[x][y]='.';
		 ++ans;
	 }
	return true;
}

int solve(){
	ans=0;
	char cur=orientation;
	int x=startx;
	int y=starty;
	for(auto item:str){
		if(item=='F'){
			if(cur=='N'){
				if(validmove(x-1,y)) --x;
			}else if(cur=='S'){
				if(validmove(x+1,y)) ++x;
			}else if(cur=='L'){
				if(validmove(x,y+1)) ++y;
			}else if(cur=='O'){
				if(validmove(x,y-1)) --y;
			}
		}else if(item=='D'){ //90 to the right
			if(cur=='N'){
				cur='L';
			}else if(cur=='S'){
				cur='O';
			}else if(cur=='L'){
				cur='S';
			}else if(cur=='O'){
				cur='N';
			}
		}else if(item=='E'){ //90 to the left
			if(cur=='N'){
				cur='O';
			}else if(cur=='S'){
				cur='L';
			}else if(cur=='L'){
				cur='N';
			}else if(cur=='O'){
				cur='S';
			}
		}
		
	}
	return ans;
}

int main(){
	ios::sync_with_stdio(false);
	while(cin>>n>>m>>s){
		if(n==0&&m==0&&s==0) break;
		for(int i=0; i<n;++i){
			for(int j=0; j<m;++j){
				cin>>a[i][j];
				if(a[i][j]=='N'||a[i][j]=='S'||a[i][j]=='L'||a[i][j]=='O'){
					startx=i;
					starty=j;
					orientation=a[i][j];
				}
			}
		}
		
		cin>>str;
		cout<<solve();
		cout<<endl;
	}
	
	
	return 0;
}
