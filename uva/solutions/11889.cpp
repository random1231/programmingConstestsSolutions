#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
vector<long long> primes;
bitset<10000009> bs;

void sieve(long long upperbound){
    sieve_size = upperbound;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.push_back(i);
	    }
}

unordered_map<long long, int> primeFactors(long long n) {
	long long lim = sqrt(n);
	unordered_map<long long, int> factors;
	long long index = 0, primeF = primes[index];
	while(primeF <= lim){
		while(n%primeF == 0){
			n /= primeF;
			if(factors.find(primeF)!=factors.end())
				++factors[primeF];
			else
				factors[primeF]=1;
        }
		primeF = primes[++index];
	}
	if (n != 1)
		factors[n]=1;
	return factors;
}

int main(){
    int t;
    sieve(3169);
    cin>>t;
    while(t--){
        int a, c;
        cin >> a>>c;
        if(c%a!=0){
            cout << "NO SOLUTION"<<endl;
            continue;
        }

        auto m1 = primeFactors(a);
        auto m2 = primeFactors(c);
        int ans=1;

        for(auto x:m2){
            auto fi=x.first;
            auto se=x.second;
            if(m1.find(fi)==m1.end())
                ans*=pow(fi, se);
            else if(se>m1[fi])
                ans*=pow(fi, se);
        }

        /*int check = (max(ans, a)/__gcd(ans, a))*min(ans, a);
        if(check!=c)
            cout << "NO SOLUTION"<<endl;
        else*/
        cout << ans<<endl;
    }

	return 0;
}

