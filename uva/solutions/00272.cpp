#include <bits/stdc++.h>

using namespace std;

int main(){
    char c;
    bool x = false;
    while(scanf("%c", &c)!=EOF){
        if(c == '"'){
            if(!x){
                cout << "``";
                x = !x;
                }else{
                    cout << "''";
                    x = !x;
                }
            }else if(c== ' ')
                cout << " ";
            else if(c=='\n')
                cout << endl;
            else
                cout << c;
        }
    return 0;
}
