#include <bits/stdc++.h>
using namespace std;

int n,t,p,mn;
int dp[100][1000];

int solve(int id, int curs){
    if(id==n){
        if(curs==t) return 1;
        return 0;
    }
    auto &ref=dp[id][curs];
    if(ref!=-1) return ref;
    int ans=0;
    for(int i=p;i<=mn;++i){
        ans+=solve(id+1,curs+i);
    }

    return ref=ans;
}

int main(){
    int tc;cin>>tc;while(tc--){
        memset(dp,-1,sizeof dp);
        cin>>n>>t>>p;
        mn=t-((n-1)*p);
        cout<< solve(0,0)<<endl;
    }

    return 0;
}

