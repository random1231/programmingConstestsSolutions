#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
vector<int> primes;
bitset<10000001> bs;

void sieve(long long upperbound){
    sieve_size = upperbound+1;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.push_back((int)i);
	    }	    

}

vector<int> primeFactors(long long n) {
	long long lim = sqrt(n);
	vector<int> factors;
	long long index = 0, primeF = primes[index];
	while(primeF <= lim){
		while(n%primeF == 0){
			n /= primeF;
			factors.push_back(primeF);
		}
		primeF = primes[++index];
	}

	if (n != 1)
		factors.push_back(n);
	return factors;
}


int main(int argc, char const *argv[])
{
	sieve(50000);
	int l;
	while(cin>>l&&l){
		bool fl=false;
		if(l<1){
			fl=true;
			l*=-1;
		}

		auto ans = primeFactors(l);
		if(fl)
			ans.insert(ans.begin(), -1);
		cout << (fl==false?l:-l) << " = ";
		for(int i=0;i<ans.size();++i){
			cout << ans[i] << (i!=ans.size()-1? " x ":"");
		}
		cout << std::endl;


	}
	return 0;
}