#include <bits/stdc++.h>

using namespace std;



int main(){
    int t;
    cin>>t;
    for(int i=1;i<=t;++i){
        int x, y, z;
        cin>>x>>y>>z;
        vector<int> v{x, y, z};
        sort(v.begin(), v.end());
        cout << "Case " << i << ": "<< v[1] << endl;
    }

    return 0;
}
