#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin
#define clear(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef pair<pair<int,int>,int> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;

ll pow2[33],pow3[33];

set<ll> powers;
const int lim=2147483647;

void gen(){
	pow2[0]=pow3[0]=1;
	powers.insert(1);
	for(ll i=1; i<33;++i){
		pow2[i]=pow2[i-1]*2;
		powers.insert(pow2[i]);
	}
	
	for(ll i=1; i<21;++i){
		pow3[i]=pow3[i-1]*3;
		powers.insert(pow3[i]);
	}

	for(ll i=1; i<33;++i){
		for(ll j=1; j<21;++j){
			ll res=pow2[i]*pow3[j];
			if(res>lim) break;
			powers.insert(res);
		}
	}
}

int main(){
	gen();
	int n;while(cin>>n&&n){
		auto it=powers.upb(n);
		if((*(prev(it)))==n) cout<<n<<endl;
		else cout<<*it<<endl;
	}
	return 0;
}
