#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin
#define clear(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef pair<pair<int,int>,int> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;

ll sievesz;
vi primes;
int n;
bitset<1000020> bs;

void sieve(ll upperbound){
	sievesz=upperbound+1;
	bs.set();
	bs[0]=bs[1]=0;
	for(ll  i=2; i<=sievesz;++i){
		if(bs[i]){
			for(ll j=i*i; j<=sievesz;j+=i){
				bs[j]=0;
			}
			primes.pb((int)i);
		}
	}
}

int solve(){
	int upp=lob(primes.beg(),primes.end(),n)-primes.beg()-1;
	int ans=0;
	//~ cout<<"upp: "<<upp<<endl;
	for(int i=upp; i>=1;--i){
		if(n-primes[i]==1)continue;
		int low=n-primes[i];
		bool flag=true;
		if(n-primes[i]>=n/2){
			low=primes[i];
			flag=false;
		}
		//~ cout<<primes[i]<<" " <<low<<endl;
		int ix=lob(primes.beg(),primes.end(),low)-primes.beg();
		//~ cout<<"ix "<<ix<<endl;
		ans+=ix;
		if(flag&&primes[ix]==low){
			
			ans+=1;
		}
		
		//~ cout<<"ans "<<ans<<endl;
		//cout<<ix<<endl;
		
	}
	return ans;
}

int main(){
	sieve(1000010);
	//~ cout<<primes[0]<< " "<<primes[1]<<endl;
	//~ cout<<lob(primes.beg(),primes.end(),4)-primes.beg()<<endl;
	int tc=1;
	while(cin>>n&&n){
		cout<<"Case "<<tc++<<": ";
		cout<<solve();
		cout<<endl;
	}
	return 0;
}
