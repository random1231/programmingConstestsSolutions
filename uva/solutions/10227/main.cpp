#include <bits/stdc++.h>

using namespace std;

template<class T>
class UnionFind
{
public:
    unordered_map<T, T> parent;
    unordered_map<T, int> degree;
    unordered_map<T, int> setSize;
    int connectedComp=0;
    int sz=0;

    T getParent(T vertex){
        if(!exists(vertex))
            return -(1<<30);
        if(parent[vertex]==vertex)
            return vertex;
        getParent(parent[vertex]);
        parent[vertex]=getParent(parent[vertex]);
        return parent[vertex];
    }

    bool inSameSet(T x, T y){
        return (getParent(x)==getParent(y));
    }

    bool exists(T vertex){
        if(parent.find(vertex)==parent.end())
            return false;
        return true;
    }

    int sizeOfSet(T x){
        if(!exists(x))
            return -1;
        return setSize[getParent(x)];
    }

    bool unionSet(T a, T b){
        T x=getParent(a);
        T y=getParent(b);
        if(x==y) return false;
        if(degree[x]>degree[y]){
            parent[y]=x;
            degree[x]=degree[y]+degree[x];
            setSize[x] += setSize[y];
        }else{
            parent[x]=y;
            degree[y]=degree[y]+degree[x];
            setSize[y] += setSize[x];
        }
        --connectedComp;
        return true;
    }
    void insertSet(T x){
        if(exists(x))
            return;
        parent.insert({x,x});
        degree.insert({x,1});
        setSize.insert({x,1});
        ++connectedComp;
        ++sz;
    }
};

int main(){
    bool arr[101][101];
    int tc;cin>>tc;
    while(tc--){
        memset(arr,false,sizeof arr);
        int p,t;cin>>p>>t;
        int ans=p;
        UnionFind<int> uf;
        int a,b;
        getchar();
        while(1){
            string s;
            getline(cin,s);
            if(s==""){
                break;
            }
            stringstream ss(s);
            ss>>a>>b;
            arr[a][b]=true;
        }
        for(int i = 1; i <= p; ++i){
            bool flag=true;
            for(int j = i+1; j <= p; ++j){
                for(int k = 1; k <=t; ++k){
                    if(arr[i][k]!=arr[j][k]){
                        flag=false;
                        break;
                    }else flag=true;
                }
                if(flag){
                    uf.insertSet(i);
                    uf.insertSet(j);
                    if(uf.unionSet(i,j)){
                        --ans;
                    }
                }
            }
        }
        cout<<ans<<endl;
        if(tc>0)
            cout<<endl;
    }
    return 0;
}