#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int tam=103;

int n,m;
int value[tam];

set<ii> adl[tam];
bool done;
int lastnode;

void dfs(int node,int &ans){
	if(adl[node].empty()){
		lastnode=node;
		done=true;
		return;
	}

	//~ cout<<node<< " "<<ans<<" "<<(*(--adl[node].end())).ff<< " "<<(*(--adl[node].end())).ss<<endl;
	ans+=(*(--adl[node].end())).ff;
	int val=(*(--adl[node].end())).ss;
	dfs(val,ans);
	
}

int main(){
	ios::sync_with_stdio(false);
	int tc;cin>>tc;
	for(int k=0; k<tc;++k){
		cin>>n>>m;
		for(int i=0; i<n;++i){
			cin>>value[i];
		}
		
		for(int i=0; i<m;++i){
			int a,b;cin>>a>>b;
			adl[a].insert({value[b],b});
		}
		int ans=0;
		dfs(0,ans);
		cout<<"Case "<<k+1<<": ";
		cout<<ans<<" " <<lastnode;cout<<endl;
		for(int i=0; i<tam;++i){
			adl[i].clear();
		}
		done=false;
	}
	
	
	return 0;
}
