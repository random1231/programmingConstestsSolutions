from sys import stdin

fact=[None]*367

def pre():
	fact[0]=1
	fact[1]=1
	for i in range (2, 367):
		ans = 1
		for x in range(2,i+1):
			ans*=x
		fact[i] = ans

def main():
	pre()
	while(True):
		line = input()
		num = int(line)

		if(num == 0):
			break
		freq = [0]*10
		for s in str(fact[num]):
			i = int(s)
			freq[i]+=1
		print(str(num)+"! --")
		print('   ', end='')
		for i in range(5):
			s = str(freq[i])
			x = len(s)
			print("("+str(i)+")"+(5-x)*" "+str(freq[i])+( "    " if i!= 4 else ""), end='')
		print()
		print('   ', end='')
		for i in range(5, 10):
			s = str(freq[i])
			x = len(s)
			print("("+str(i)+")"+(5-x)*" "+str(freq[i])+( "    " if i!= 9 else ""), end='')
		print()
main()