#include <bits/stdc++.h>

using namespace std;

using vi=vector<int>;

class UnionFind
{
private: vi p, rank; int numSets;
public:
    UnionFind( int N){
        numSets=N;
        p.assign(N,0);
        rank.assign(N,0);
        for(int i = 0; i < N; ++i){
            p[i]=i;
        }
    }

    int findSet(int i){
         return (p[i]==i) ? i : (p[i]=findSet(p[i]));
    }

    bool isSameSet(int i, int j){
        return (findSet(i)==findSet(j));
    }

    void unionSet(int i, int j){
        if(isSameSet(i,j))
            return;
        --numSets;
        int x=findSet(i);
        int y=findSet(j);
        if(rank[x]>rank[y]) p[y]=x;
        else{
            p[x]=y;
            if(rank[x]==rank[y]) ++rank[y];
        }
    }

    int numDisjointSets(){
        return numSets;
    }
};


int main(){
    int t;
    cin>>t;
    for(int i = 0; i < t; ++i){
        if(i>0)
            cout<<endl;
        int n;
        cin>>n;
        string w;
        getline(cin,w);
        ++n;
        string s;
        int x=0;
        int y=0;
        UnionFind u(n);
        while(1){
            getline(cin,s);
            if(s==""){
                cout<<x<<","<<y<<endl;
                break;
            }
            char c;
            int a, b;
            stringstream ss(s);
            ss>>c>>a>>b;
            if(c=='c'){
                u.unionSet(a, b);
            }else if(c=='q'){
                if(u.isSameSet(a,b)) ++x;
                else ++y;
            }
        }
    }
    return 0;
}