#include <bits/stdc++.h>

using namespace std;

int n,k;
short dp[10001][10001];
int val[10001];

int solve(int id, int curs){
    int mod=((curs%k)+k)%k;
     if(id==n){
        if(mod==0)
            return 1;
        return 0;
    }
    auto &ref=dp[id][mod];
    if(ref!= -1) return ref;
    return ref=solve(id+1,curs+val[id])||solve(id+1,(id==0? val[id]:curs-val[id]));
}
 
 int main(){
     int t;cin>>t;while(t--){
        memset(dp,-1,sizeof dp);
        cin>>n>>k;
        for(int i = 0; i < n; ++i){
            cin>>val[i];
        }
        
        if(solve(0,0)) cout<<"Divisible";
        else cout<<"Not divisible";
        cout<<endl;
     }
     return 0;
 }