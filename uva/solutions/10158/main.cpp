#include <bits/stdc++.h>

using namespace std;

class UnionFind
{
public:
    vector<int> p, rank,setSize;
    int numSets;
    UnionFind(int N){
        numSets=N;
        p.assign(N,0);
        rank.assign(N,0);
        setSize.assign(N, 1);
        for(int i = 0; i < N; ++i){
            p[i]=i;
        }
    }

    int findSet(int i){
         return (p[i]==i) ? i : (p[i]=findSet(p[i]));
    }

    bool inSameSet(int i, int j){
        return (findSet(i)==findSet(j));
    }

    void unionSet(int i, int j){
        if(inSameSet(i,j))
            return;
        --numSets;
        int x=findSet(i);
        int y=findSet(j);
        if(rank[x]>rank[y]){
            p[y]=x;
            setSize[x] += setSize[y];
        }
        else{
            p[x]=y;
            setSize[y] += setSize[x];
            if(rank[x]==rank[y])
                ++rank[y];
        }
    }

    int sizeOfSet(int i){
        return setSize[findSet(i)];
    }

};

int n;


int enID(int x){
    return n+x;
}

bool enemies(int x,int y, UnionFind &uf){
    return uf.inSameSet(enID(x),y)||uf.inSameSet(enID(y),x);
}


int main(){
    cin>>n;
    int c,x,y;
    int line=0;
    UnionFind uf(2*n);
    while(cin>>c>>x>>y&&c){
        switch(c){
            case 1:
                if(enemies(x,y,uf)){
                    //cout<<"Trying to be friends but they're enemies :("<<endl;
                    cout<<"-1"<<endl;
                }
                else if(!uf.inSameSet(x,y)){
                    //cout<<"Trying to be friends. Now they're friends :')"<<endl;
                    uf.unionSet(x,y);
                    uf.unionSet(enID(x),enID(y));
                }else{
                    //cout<<"Trying to be friends but they're already friends :)"<<endl;
                }
                break;
            case 2:
                if(uf.inSameSet(x,y)){
                    //cout<<"Trying to be enemies but they're friends :("<<endl;
                    cout<<"-1"<<endl;
                }
                else if(!enemies(x,y,uf)){
                    uf.unionSet(enID(x),y);
                    uf.unionSet(enID(y),x);
                }else{
                    //cerr<<"Trying to be enemies but they're already enemies :)"<<endl;
                }                
                break;
            case 3:
                if(uf.inSameSet(x,y))
                    cout<<"1"<<endl;
                else cout<<"0"<<endl;
                break;

            case 4:
                if(enemies(x,y,uf))
                    cout<<"1"<<endl;
                else cout<<"0"<<endl;
                break;
        }
    }
    return 0;
}