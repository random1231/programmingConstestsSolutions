#include <bits/stdc++.h>
#define ll long long

using namespace std;

ll n,s;

ll dp[66][66][2];

ll solve(int id,int cur,int prev){
    if(id==0){
        if(cur==0) return 1;
        return 0;
    }

    if(cur>id) return 0;
    if(cur==id) return 1;

    auto &ref=dp[id][cur][prev];
    if(ref!=-1) return ref;
    return ref=solve(id-1,(prev==1?cur+1:cur),0)+solve(id-1,cur-1,1);
}

int main(){
    memset(dp,-1,sizeof dp);
    while(cin>>n>>s&&n>=0&&s>=0){
         cout<<solve(n,s,0)<<endl;
    }

    return 0;
}