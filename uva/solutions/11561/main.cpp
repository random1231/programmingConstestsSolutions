#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int w,h;
char a[51][51];
int startx,starty;
int dr[]={-1,0,1,0};
int dc[]={0,1,0,-1};
char mo[]={'u','r','d','l'};

void solve(int x,int y,int &gold){
	if(a[x][y]=='#'||a[x][y]=='v'){
		return;
	}
	//~ cout<<x<< " "<<y<< " "<<gold;cout<<endl;
	
	if(a[x][y]=='G'){
		//~ cout<<"gold"<<endl;
		++gold;
	}
	
	a[x][y]='v';
	for(int i=0; i<4;++i){
		if(a[x+dr[i]][y+dc[i]]=='T') return;
		
	}
	
	
	for(int i=0; i<4;++i){
		//~ cout<<mo[i]<<endl;
		solve(x+dr[i],y+dc[i],gold);
	}
	
	
}

int main(){
	ios::sync_with_stdio(false);
	while(cin>>w>>h){
		for(int i=0; i<h;++i){
			for(int j=0; j<w;++j){
				cin>>a[i][j];
				if(a[i][j]=='P'){
					startx=i;
					starty=j;
				}
			}
		}
		//~ cout<<"start: "<<startx<< " "<<starty<<endl;
		int ans=0;
		solve(startx,starty,ans);
		cout<<ans;cout<<endl;
	}
	
	
	return 0;
}
