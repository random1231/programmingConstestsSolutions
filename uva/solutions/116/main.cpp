#include <bits/stdc++.h>

using namespace std;

int a[11][101],n,m, dp[11][101], path[11][101];


int solve(int i, int j){
    if(j==m)
        return 0;
    auto &ref=dp[i][j];
    if(ref!=INT_MAX) return ref;

    int p[]{i==0?n-1:i-1,i,i==n-1?0:i+1};

    int x=a[i][j];
    for(int rep = 0; rep < 3; ++rep){
        auto val=x+solve(p[rep],j+1);
        if(val<ref||(val==ref&&p[rep]<path[i][j])){
            ref=val;
            path[i][j]=p[rep];
        }
    }

    return ref;
}

int main(){
	while(cin>>n>>m){
		for(int i=0;i<n;++i) for(int j=0;j<m;++j){
            cin>>a[i][j];
            dp[i][j]=INT_MAX;
        }
        int mn=INT_MAX;
        int index;
        for(int i=0;i<n;++i){
            auto res=solve(i,0);
            if(res<mn){
                mn=res;
                index=i;
            }
        }

        for(int i = 0; i < m; ++i){
            if(i!=0) cout<< " ";
            cout<<index+1;
            index=path[index][i];
        }
        cout<<endl<<mn<<endl;
    }
	return 0;
}
