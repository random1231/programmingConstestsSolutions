using namespace std;

#include <bits/stdc++.h>

long long sieve_size;
vector<int> primes;
bitset<10000001> bs;
int mu[1000010];
int M[1000010];

void sieve(long long upperbound){
    sieve_size = upperbound+1;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.push_back((int)i);
	    }	    
}

void calc(long long n) {
	long long index = 0, primeF = primes[index], ans = 0;
	auto tmp=n;

	long long lim = sqrt(n);
	while(primeF <= lim){
		if(n%primeF == 0){
			if((n/primeF)%primeF == 0){
				mu[tmp]=0;
				return;
			}
			n /= primeF;
			++ans;
		}
		++index;
		if(index<primes.size())
			primeF = primes[index];
		else
			break;
	}

	if (n != 1)
		ans++;
	if(ans%2==0)
		mu[tmp]=1;
	else
		mu[tmp]=-1;
}

void pre(){
	mu[0]=mu[1]=1;
	M[1]=1;
	for(int i=2;i<=1000000;++i){
		calc(i);
		M[i]=M[i-1]+mu[i];
	}

}

int main(int argc, char const *argv[]){
	sieve(1000);
	pre();
	int n;
	while(cin>>n&&n){
		cout.width(8);
		cout<<n;
		cout.width(8);
		cout<<mu[n];
		cout.width(8);
		cout<<M[n]<<endl;
	}
}