#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
bitset<100000009> bs;
vector<long long> primes;

void sieve( long long upperbound){
    sieve_size = upperbound;
    bs.set();
    bs[0] = bs [1] = 0;
    for( long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for ( long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.push_back(i);
	    }
}

int main(int argc, char const *argv[])
{
	 long long n;
	sieve(1000010);
	while(cin>>n&&n>=0){
		auto tmp=n;
		int i=0, p=primes[i];
		int lim=sqrt(n);
		while(p<=lim){
			while(n%p==0){
				cout <<"    "<<p<<endl;
				n/=p;
			}
			if(i<primes.size()-1)
				p=primes[++i];
			else
				break;
		}

		if(n!=1) cout <<"    "<<n<<endl;
		cout <<endl;

	}
	return 0;
}