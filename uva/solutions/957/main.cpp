#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lowb lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin
#define clear(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef pair<pair<int,int>,int> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;

int t;
int n;
int a[100010];
int ans=0;
int fy=0,ly=0;

int solve(){
	ans=0;
	fy=0,ly=0;
	for(int i=0; i<n;i++){
		int res=upb(a+i,a+n,a[i]+t-1)-a;
		if((res-1)-i+1>ans){
			ans=(res-1)-i+1;
			fy=a[i];
			ly=a[res-1];
		}
	}
	return 0;
}

int main(){
	while(cin>>t){
		cin>>n;
		for(int i=0; i<n;i++){
			cin>>a[i];
		}
		solve();
		cout<<ans<< " "<<fy<< " "<<ly;
		cout<<endl;
	}
	return 0;
}
