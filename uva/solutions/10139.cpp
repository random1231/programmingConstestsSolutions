#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
bitset<10000001> bs;

void sieve(long long upperbound, vector<long long> &primes){
    sieve_size = upperbound;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.push_back(i);
	    }
}

unordered_map<long long, int> primeFactors(long long n, vector<long long> &primes) {
	long long lim = sqrt(n);
	unordered_map<long long, int> factors;
	long long index = 0, primeF = primes[index];
	while(primeF <= lim){
		while(n%primeF == 0){
			n /= primeF;
			if(factors.find(primeF)!=factors.end())
				++factors[primeF];
			else
				factors[primeF]=1;
        }
		primeF = primes[++index];
	}
	if (n != 1)
		factors[n]=1;
	return factors;
}

int getPower(long long n, long long p){
	int ans=0;
	for(long long power=p;power<=n;power*=p)
		ans+=n/power;
	return ans;
}

int main(int argc, char const *argv[])
{
	vector<long long> primes;
	sieve(46350, primes);
	long long m,n;
	while(cin>>m>>n){
		if(n==0){
			cout << n << " divides " <<m<<"!"<<endl;
			continue;
		}else if(m==0){
			if(n==1)
				cout << n << " divides " <<m<<"!"<<endl;
			else
				cout << n << " does not divide " <<m<<"!"<<endl;
			continue;
		}else if(n<=m){
			cout << n << " divides " <<m<<"!"<<endl;
			continue;
		}
		auto m2=primeFactors(n, primes);
		bool fl=false;
		for(auto x:m2){
			auto power = getPower(m, x.first);
			if(power<x.second)
				fl=true;
		}
		if(fl)
			cout << n << " does not divide " <<m<<"!";
		else
			cout << n << " divides " <<m<<"!";
		cout << endl;
	}
	return 0;
}
f