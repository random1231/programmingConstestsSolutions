#include <bits/stdc++.h>

using namespace std;

unsigned long long sieve_size;
vector<unsigned long long> primes;
bitset<10000009> bs;
map<unsigned long long, int> factors;

void sieve(unsigned long long upperbound){
    sieve_size = upperbound;
    bs.set();
    bs[0] = bs [1] = 0;
    for(unsigned long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (unsigned long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.push_back(i);
	    }
}

void primeFactors(unsigned long long n) {
	unsigned long long lim = sqrt(n);
	unsigned long long index = 0, primeF = primes[index];
	while(primeF <= lim){
		while(n%primeF == 0){
			n /= primeF;
			if(factors.find(primeF)!=factors.end())
				++factors[primeF];
			else
				factors[primeF]=1;
        }
		primeF = primes[++index];
	}
	if(n != 1){
		if(factors.find(n)!=factors.end())
			++factors[n];
		else
			factors[n]=1;
	}
}

int main(int argc, char const *argv[])
{
	sieve(1010);
	unsigned long long lim = 1000000000000000000;
	int c;
	cin>>c;
	for(int i=1;i<=c;++i){
		string s;
		cin>>s;
		int k=0;
		for(auto it=s.rbegin(); *it=='!';++it, ++k);
		int n = stoull(s.substr(0, s[s.size()-k]));
		for(long long i=n;i>1;i-=k)
			primeFactors((unsigned long long)i);
		unsigned long long ans=1;
		bool exc=false;
		for(auto x:factors){
			ans*=(x.second+1);
			if(ans>lim)
				exc=true;
		}
		cout<<"Case "<<i<<": ";
		if(!exc)
			cout << ans;
		else
			cout << "Infinity";
		cout <<endl;
		factors.clear();
	}
	return 0;
}