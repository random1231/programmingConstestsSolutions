#include <bits/stdc++.h>

using namespace std;
unsigned long long v[10009];

void pre(){
	v[0]=1;
	v[1]=2;
	for(int i = 2; i <=10000;++i){
		v[i] = v[i-1]+v[i-2];
	}
}

int main(int argc, char const *argv[])
{
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	pre();
	int n;
	while(cin >> n){
		cout << v[n] << endl;
	}
	return 0;
}