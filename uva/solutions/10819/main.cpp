#include <bits/stdc++.h>
using namespace std;

int m,n;
int a[101],c[101];
int dp[101][10001];

// int solve(int id, int curs){
//     if(id==n) return 0;
//     auto &ref=dp[id][curs];
//     if(ref!=-1){cerr<<"overlap"<<endl;
//     cerr<<"id: "<<id<< " "<<"curs: "<<curs<<endl;
//     return ref;}
//     if(curs+a[id]>m)
//         return ref= solve(id+1,curs);
//     return ref= max(solve(id+1,curs),solve(id+1,curs+a[id])+c[id]);
// }

int solve(int id, int curs){
    cerr<<id<< " "<< curs<<endl;
    if(abs(curs)<200) curs+=200;
    if(id==n) return 0;
    auto &ref=dp[id][curs];
    if(ref!=-1) return ref;
    return ref= max(solve(id+1,curs),solve(id+1,curs-a[id])+c[id]);
}

int main(){
    while(cin>>m>>n){
        memset(dp,-1,sizeof dp);
        for(int i = 0; i < n; ++i){
            cin>>a[i]>>c[i];
        }

        cout<<solve(0,m)<<endl;
    }
    return 0;
}