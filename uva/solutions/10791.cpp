#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
bitset<100000009> bs;
vector<long long> primes;

void sieve(long long upperbound){
    sieve_size = upperbound;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.push_back(i);
	    }
}

int main(){
	long long n;
	int c=1;
	sieve(65536);
	while(cin>>n&&n){
		if(n==1){
			cout << "Case "<<c<<": 2"<<std::endl;
			++c;
			continue;
		}

		int lim=sqrt(n);
		long long ans=0;
		int co=0;
		for(auto it=primes.begin();it!=primes.end()&&*it<=lim&&n>1;++it){
			auto p=*it;
			auto res=1;
			bool f=false;
			if(n%p==0)++co;
			while(n%p==0){
				n/=p;
				res*=p;
				f=true;
			}
			if(f)
				ans+=res;
		}


		if(n!=1){
			++co;
			ans+=n;
		}
		if(co<=1) //prime power
			ans+=1;
		cout << "Case "<<c<<": "<<ans<<std::endl;

		++c;
	}
}