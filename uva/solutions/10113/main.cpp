#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

map<string,vector<pair<string,pair<int,int> > > > m;

bool found;
int ans1,ans2;
set<string> visited;

void dfs(string source,string target,int x,int y){
	visited.insert(source);
	if(source==target){
		ans1=x;
		ans2=y;
		found=true;
		return;
	}

	for(auto item:m[source]){
		string name=item.ff;
		int a=item.ss.ff;
		int b=item.ss.ss;
		if(visited.find(name)==visited.en){
			dfs(name,target,x*a,y*b);
		}
		if(found){
			return;
		}
	}
}

int main(){
	ios::sync_with_stdio(false);
	int itema,itemb;
	char type,ign;
	string namea,nameb;
	while(1){
		string s;
		getline(cin,s);
		stringstream strs(s);
		strs>>type;
		if(type=='.') break;
		if(type=='!'){
			strs>>itema>>namea>>ign>>itemb>>nameb;
			int g=__gcd(itema,itemb);
			m[namea].pb({nameb,{itema/g,itemb/g}});
			m[nameb].pb({namea,{itemb/g,itema/g}});
		}else{
			strs>>namea>>ign>>nameb;
			auto it=m.find(namea);
			bool xxx=true;
			if(it==m.end()) xxx=false;
			else{
				found=false;
				dfs(namea,nameb,1,1);
				if(found){
					int g=__gcd(ans1,ans2);
					cout<<ans1/g<<" "<< namea<<" = " << ans2/g<< " " <<nameb;
					cout<<endl;
				}else xxx=false;
			}
			if(!xxx){
				cout<<"? "<< namea<<" = ? " <<nameb;	
				cout<<endl;
			}
			visited.clear();
		}
	}
	return 0;
}
