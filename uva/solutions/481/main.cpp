#include <bits/stdc++.h>
using namespace std;

#define MAX_N 2000

int l[MAX_N], l_id[MAX_N], p[MAX_N];
int n;

vector<int> longestIS(int a[]){
    int lis = 0, lis_end = 0;
    for (int i = 0; i < n; ++i){
        int pos = lower_bound(l, l + lis, a[i]) - l;
        l[pos] = a[i];
        l_id[pos] = i;
        p[i] = pos ? l_id[pos - 1] : -1;
        if (pos + 1 > lis){
            lis = pos + 1;
            lis_end = i;
        }
    }
    vector<int> v;
    int x=lis_end;
    for (; p[x] >= 0; x = p[x])
       v.insert(v.begin(),a[x]);
    v.insert(v.begin(),a[x]);
    return v;
}

int main(){
    int a[MAX_N];
    int i=0;
    for(i = 0; cin>>a[i]; ++i)
    n=i;
    auto v=longestIS(a);
    cout<<v.size();
    cout<<endl;
    cout<<"-";
    cout<<endl;
    for(auto x : v){
        cout<<x;
        cout<<endl;
    }
    
    return 0;
}