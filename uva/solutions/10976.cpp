#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[])
{
	int k;
	while(cin>>k&&k){
		int cn=0;
		for(int y=k+1; y<=2*k;++y){
			if((y*k)%(y-k)==0) ++cn;
		}
		cout<<cn<<endl;
		for(int y=k+1; y<=2*k;++y){
			int num=(y*k);
			int den=(y-k);
			if(num%den==0)
				cout<<"1/"<<k<<" = "<<"1/"<<num/den<<" + "<<"1/"<<y<<endl;
		}
	}
	return 0;
}