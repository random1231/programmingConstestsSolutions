#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[])
{
	int n;
	cin >> n;
	while(n--){
		string x;
		vector<int> v;
		while(cin >> x && x !="\n")
			v.push_back(stoi(x));
		int mx=0;
		for(int i=0;i<v.size()-1;++i){
			for(int j=0;j<v.size();++j){
				auto g=__gcd(v[i], v[j]);
				if(g>mx)
					mx = g;
			}
		}

		cout << mx << std::endl;
	}
	return 0;
}