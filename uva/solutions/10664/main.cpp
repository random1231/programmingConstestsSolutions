#include <bits/stdc++.h>
using namespace std;
//Set the numbers for the problem
#define MAX_N 1010
#define MAX_W 210

int n, cap,W[MAX_N], memo[MAX_N][MAX_W];

int solve(int id, int w){
    if (id == n||w == 0)
        return cap-w;
    if (memo[id][w] != -1)
        return memo[id][w];
    if (W[id] > w)
        return memo[id][w] = solve(id + 1, w);
    return memo[id][w] = max(solve(id + 1, w), solve(id + 1, w - W[id]));
}

int main(){
    int t;cin>>t;
    getchar();
    while(t--){
        memset(memo,-1,sizeof memo);
        string s;
        getline(cin,s);
        stringstream ss(s);
        int x;
        int i=0;
        int sm=0;
        while(ss>>x){
            W[i++]=x;
            sm+=x;
        }
        n=i;
        if(sm%2!=0){
            cout<<"NO"<<endl;
            continue;
        }
        sm/=2;
        cap=sm;
        if(solve(0,sm)!=sm){
            cout<<"NO"<<endl;
            continue;
        }
        cout<<"YES"<<endl;
    }
    return 0;
}