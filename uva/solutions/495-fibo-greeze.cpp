#include <bits/stdc++.h>

using namespace std;

long long f[10000];

int fibo(long long n){
	if(n == 0)
		return 0;
	if(n == 1)
		return 1;
	if(f[n] != 0)
		return f[n];
	f[n] = fibo(n-1)+fibo(n-2);
	return f[n];
}


int main(){
	cout << fibo(6);
	return 0;
}
