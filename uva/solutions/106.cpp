#include <bits/stdc++.h>

using namespace std;
int ma[1000009];

int main(int argc, char const *argv[])
{
	int num;
	ios::sync_with_stdio(false);
	while(cin >> num){
		int trip = 0;
		int a , b, c=0;
		memset(ma, 0, 1000009);
		for(int m = 1;m*m<=num;++m){
			for(int n=m+1;n*n<=num;n+=2){
				if((m+n)%2!=1||__gcd(m, n)!=1)
					continue;
				b = 2*m*n; a=n*n-m*m; c=m*m+n*n;
				if(c>num)
					break;
				if(a*a+b*b==c*c){
					int l = 1;
					while(l*c <= num){
						ma[a*l] = 1;
						ma[b*l] = 1;
						ma[c*l] = 1;
						cout << "c*l: " << c*l << endl;
						++l;
					}
					++trip;
				}
			}
		}
		int ans = 0;
		for(int i = 1; i<=num;++i)
			if(ma[i]!=1)
				++ans;
		cout << trip << " " << ans << endl;
	}
	return 0;
}