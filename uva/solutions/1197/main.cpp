#include <bits/stdc++.h>

using namespace std;

class UnionFind
{
public:
    vector<int> p, rank,setSize;
    int numSets;
    UnionFind(int N){
        numSets=N;
        p.assign(N,0);
        rank.assign(N,0);
        setSize.assign(N, 1);
        for(int i = 0; i < N; ++i){
            p[i]=i;
        }
    }

    int findSet(int i){
         return (p[i]==i) ? i : (p[i]=findSet(p[i]));
    }

    bool inSameSet(int i, int j){
        return (findSet(i)==findSet(j));
    }

    void unionSet(int i, int j){
        if(inSameSet(i,j))
            return;
        --numSets;
        int x=findSet(i);
        int y=findSet(j);
        if(rank[x]>rank[y]){
            p[y]=x;
            setSize[x] += setSize[y];
        }
        else{
            p[x]=y;
            setSize[y] += setSize[x];
            if(rank[x]==rank[y])
                ++rank[y];
        }
    }

    int sizeOfSet(int i){
        return setSize[findSet(i)];
    }

};

int main(){
    int n,m;
    while(cin>>n>>m&&n>0){
        UnionFind uf(n);
        while(m--){
            int t;cin>>t;
            vector<int> v;
            while(t--){
                int x;
                cin>>x;
                v.push_back(x);
            }
            for(int i = 0; i < v.size()-1; ++i){
                uf.unionSet(v[i],v[i+1]);
            }
        }
        cout<<uf.sizeOfSet(0)<<endl;
    }
    return 0;
}