#include <bits/stdc++.h>

#define MAXN 6
#define MAXV 7500

using namespace std;

int n=5, val[]={1,5,10,25,50}, memo[MAXN][MAXV];

int ways(int type, int value) {
    if (value == 0)
        return 1;
    if (value < 0 || type == n)
        return 0;
    if (memo[type][value] != -1){
        return memo[type][value];
    }
    return memo[type][value] = ways(type + 1, value) + ways(type, value - val[type]);
}

int main(){
    int v;
    memset(memo,-1,sizeof memo);
    while(cin>>v){
        cout<<ways(0,v)<<endl;
    }
    return 0;
}