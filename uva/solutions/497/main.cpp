#include <bits/stdc++.h>
using namespace std;

#define MAX_N 100000

int l[MAX_N], l_id[MAX_N], p[MAX_N];
int n;

vector<int> longestIS(int a[]){
    int lis = 0, lis_end = 0;
    for (int i = 0; i < n; ++i){
        int pos = lower_bound(l, l + lis, a[i]) - l;
        l[pos] = a[i];
        l_id[pos] = i;
        p[i] = pos ? l_id[pos - 1] : -1;
        if (pos + 1 > lis){
            lis = pos + 1;
            lis_end = i;
        }
    }
    vector<int> v;
    int x=lis_end;
    for (; p[x] >= 0; x = p[x])
       v.insert(v.begin(),a[x]);
    v.insert(v.begin(),a[x]);
    return v;
}

int main(){
    int t;
    cin>>t;
    string s;
    getchar();
    getchar();
    for(int i = 0; i < t; ++i){
        if(i!=0)
            cout<<endl;
        vector<int> v;
        //getline(cin,s);
        char ss[100]="hans";
        while(fgets(ss,100,stdin)!=NULL){
            if(strcmp(ss,"\n")==0) break;
            v.push_back(stoi(ss));
        }
        n=v.size();
        int a[n];
        for(int i = 0; i < n; ++i){
            a[i]=v[i];
        }
        auto ans=longestIS(a);
        cout<<"Max hits: "<<ans.size()<<endl;
        for(auto x : ans){
            cout<<x<<endl;
        }
    }
    
    return 0;
}
