#include <bits/stdc++.h>
using namespace std;

std::vector<long long> fibo(50);

void pre(){
	fibo[0] = 1;
	fibo[1] = 1;
	for(int i = 2; i <= 50; ++i){
		fibo[i] = fibo[i-1]+fibo[i-2];
	}
}

int main(int argc, char const *argv[])
{
	pre();
	int n;
	cin >> n;
	while(n--){
		int num;
		cin >> num;
		int ans = num;
		vector<int> pos;
		while(ans>=0){
			auto it = lower_bound(fibo.begin(), fibo.end(), ans);
			if(ans-(*it)==0){
				pos.push_back(distance(fibo.begin(), it));
				break;
			}
			--it;
			pos.push_back(distance(fibo.begin(), it));
			ans-=*it;
		}

		if(pos[pos.size()-1] == 0)
			pos[pos.size()-1] = 1;
		auto it = pos.begin();
		auto val = *it;
		cout << num << " = ";
		for(int i = val; i>0;--i){
			if(val == i&&it!=pos.end()){
				cout << "1";
				val = *(++it);
			}
			else
				cout << "0";
		}

		cout << " (fib)" << endl;
	}
	return 0;
}