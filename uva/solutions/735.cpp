#include <bits/stdc++.h>

using namespace std;

bool visited[61][61][61];

int main(int argc, char const *argv[])
{
	set<int> s;
	for(int i=1;i<=20;++i){
		s.insert(i);
		s.insert(i*2);
		s.insert(i*3);
	}
	s.insert(0);
	s.insert(50);
	vector<int> v(s.begin(), s.end());

	int score;
	while(cin>>score){
		if(score<=0){
			cout<<"END OF OUTPUT"<<endl;;
			return 0;
		}

		int per=0;
		int com=0;
		memset(visited,false,sizeof visited);
		for(auto i:v){
			for(auto j:v){
				for(auto k:v){
					if(i+j+k==score){
						if(visited[i][j][k]==false){
							visited[i][j][k]=true;
							visited[i][k][j]=true;
							visited[j][i][k]=true;
							visited[j][k][i]=true;
							visited[k][i][j]=true;
							visited[k][j][i]=true;
							++com;
						}
						++per;
					}
				}
			}
		}

		if(per>0){
			cout <<"NUMBER OF COMBINATIONS THAT SCORES " << score << " IS " << com<<"."<<endl;
			cout <<"NUMBER OF PERMUTATIONS THAT SCORES " << score << " IS " << per<<"."<<endl;
		}else
			cout<<"THE SCORE OF "<< score<< " CANNOT BE MADE WITH THREE DARTS."<<endl;
		cout<<"**********************************************************************"<<endl;

		

	}

	return 0;
}