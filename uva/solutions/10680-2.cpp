#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
vector<long long> primes;
bitset<10000009> bs;
long long res[1000001];

long long multmod(long long a, long long b, long long c){
    long long x = 0,y=a%c;
    while(b > 0){
        if(b%2 == 1)
            x = (x+y)%c;
        y = (y*2)%c;
        b /= 2;
    }
    return x%c;
}

long long expmod(long long a, long long b, long long c){
	long long res = 1;
	long long x = a%c;
	while(b > 0){
		if(b%2==1)
			res = multmod(res, x, c);
		x=multmod(x, x, c);
		b/=2;
	}
	return res;
}

void pre(){
    for(long long i=0;i<=1000;++i){
        int ix=1;
        auto p = primes[ix];
        auto expof2 = (int)(log10(i)/log10(2))-(int)(log10(i)/log10(5));
        auto ans=expmod(2, expof2, 10);
        while(p<=i){
            long long tmp=1;
            auto ex=(int)(log10(i)/log10(p));
            if(p>i/2)
                tmp = p%10;
            else
                tmp = expmod(p, ex, 10);
            ans=(ans*tmp)%10;
            p=primes[++ix];
            if(p==5)
                p=primes[++ix];
        }
        res[i]=ans;
    }
}

void sieve(long long upperbound){
    sieve_size = upperbound;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.push_back(i);
	    }
}

int main(){
    int n;
    sieve(32778);
    pre();
    while(cin>>n&&n){
        cout <<res[n]<<endl;
    }

	return 0;
}
