#include <bits/stdc++.h>

using namespace std;

int board[8][8];
int a[8];
int nu=0;
//vector< vector< vector<int> > > sol;
int sol[92][8][8];
int idx=0;

void addSol(){
    for(int i = 0; i < 8; ++i){
        for(int j = 0; j < 8; ++j){
            sol[idx][i][j]=board[i][j];
        }
    }
    ++idx;
}

bool validPos(int row, int col){
    //Checking the left side of the row
    for(int i=0;i<col;++i)
        if(board[row][i])
            return false;
    //Checking upper diagonal on then left
    for(int i=row,j=col;i>=0&&j>=0;--i, --j)
        if(board[i][j])
            return false;
    //Checking upper diagonal on then left
    for(int i=row,j=col;i<8&&j>=0;++i, --j)
        if(board[i][j])
            return false;
    return true;
}

bool solve(int col){
    if(col>=8){
        nu++;
        addSol();
        return true; //All the queens have been placed!
    }
    for(int row=0;row<8;++row){
        if(validPos(row,col)){ //We can place the queen in this position
            board[row][col]=1;
            //Now we are gonna place the rest of the queens
            solve(col+1);
            board[row][col]=0; //Not a valid position... Backtrack!
        }
    }
    return false; //We couldnt place a queen in any row of this column
}

int solve2(){
    int ans=1<<30;
    for(int k = 0; k < 92; ++k){
        int cur=0;
        for(int i = 0; i < 8; ++i){
            for(int j = 0; j < 8; ++j){
                if(sol[k][j][i]==1){
                    cur+=((a[i]-1)!=j?1:0);
                    break;
                }
            }
        }
        if(cur<ans)
            ans=cur;
    }

    return ans;
}

int main(int argc, char const *argv[])
{
    solve(0);
    //print();

    int t=1;
    while(1){
        for(int i = 0; i < 8; ++i){
            if(!(cin>>a[i]))
                return 0;
        }

        cout<< "Case " << t++<<": "<<solve2();
        cout<<endl;
    }
    return 0;
}