#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

vi adl[310];
int color[310];

int n;

bool isBipartite(int v, int c){
    color[v]=(c+1)%2;
    for(int i=0;i<(int)adl[v].size();++i){
        if(color[adl[v][i]]==-1)
            return isBipartite(adl[v][i],color[v]);
        else if(color[adl[v][i]]==color[v]){
            return false;
		}
    }
    return true;
}

int main(){
	ios::sync_with_stdio(false);
	while(cin>>n&&n>0){
		mem(color,-1);
		while(1){
			int a,b;
			cin>>a>>b;
			if(a==0||b==0)break;
			adl[a].pb(b);
			adl[b].pb(a);
		}
		if(!isBipartite(1,0))
			cout<<"NO";
		else
			cout<<"YES";
		cout<<endl;

		for(int i=1; i<=n;++i){
			adl[i].clear();
		}
	}
	
	return 0;
}
