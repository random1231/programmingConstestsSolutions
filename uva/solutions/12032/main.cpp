#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin
#define clear(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef pair<pair<int,int>,int> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;

int n;
int a[100010];

bool simulate(int x){
	a[0]=0;
	int cur=x;
	for(int i=1; i<=n;++i){
		if(a[i]-a[i-1]>cur){
			return false;
		}if(a[i]-a[i-1]==cur) --cur;
	}
	
	return true;
	
}

int solve(){
	int start=1;
	int end=10100010;
	int mid;
	int ans;
	while(end>=start){
		mid=(start+end)/2;
		//~ cout<<start<< " "<<mid<< " "<<end<< " | ";
		if(simulate(mid)){
			end=mid-1;
			ans=mid;
		}else start=mid+1;
	}
	return ans;
}


int main(){
	int tc;cin>>tc;
	a[0]=0;
	for(int i=1; i<tc+1;++i){
		cin>>n;
		//~ int maxdif=0;
		for(int i=1; i<=n;++i){
			cin>>a[i];
			//~ maxdif=max(a[i]-a[i-1],maxdif);
		}
		cout<<"Case "<<i<<": ";
		cout<<solve();
		cout<<endl;
	}
	return 0;
}
