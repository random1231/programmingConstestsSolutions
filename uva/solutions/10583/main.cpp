#include <bits/stdc++.h>

using namespace std;

class UnionFind
{
public:
    vector<int> p, rank,setSize;
    int numSets;
    UnionFind(int N){
        numSets=N;
        p.assign(N,0);
        rank.assign(N,0);
        setSize.assign(N, 1);
        for(int i = 0; i < N; ++i){
            p[i]=i;
        }
    }

    int findSet(int i){
         return (p[i]==i) ? i : (p[i]=findSet(p[i]));
    }

    bool isSameSet(int i, int j){
        return (findSet(i)==findSet(j));
    }

    void unionSet(int i, int j){
        if(isSameSet(i,j))
            return;
        --numSets;
        int x=findSet(i);
        int y=findSet(j);
        if(rank[x]>rank[y]){
            p[y]=x;
            setSize[x] += setSize[y];
        }
        else{
            p[x]=y;
            setSize[y] += setSize[x];
            if(rank[x]==rank[y])
                ++rank[y];
        }
    }

    int sizeOfSet(int i){
        return setSize[findSet(i)];
    }

};

int main(){
    int n,m;
    int cs=1;
    while(cin>>n>>m&&n>0){
        UnionFind uf(n);
        while(m--){
            int a,b;cin>>a>>b;
            uf.unionSet(a-1,b-1);
        }
        cout<<"Case "<<cs<<": "<<uf.numSets;
        cout<<endl;
        ++cs;
    }
    return 0;
}