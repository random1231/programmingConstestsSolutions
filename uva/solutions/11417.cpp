#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[])
{
	int n;
	while(cin>>n&&n){
		long long G=0;
		for(long long i=1;i<n;i++)
			for(long long j=i+1;j<=n;j++)
			G+=__gcd(i,j);
		cout << G<<std::endl;
	}
	return 0;
}