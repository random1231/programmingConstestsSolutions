#include <bits/stdc++.h>

using namespace std;

int m,c,price[25][25];
int memo[210][25];


int shop(int money, int g){
    if(money<0) return -1e9;
    if(g==c) return m-money;
    if(memo[money][g]!=-1) return memo[money][g];
    int ans=-1;
    for(int i = 1; i <= price[g][0]; ++i)
        ans=max(ans,shop(money-price[g][i],g+1));
    return memo[money][g]=ans;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        cin>>m>>c;
        for(int i = 0; i < c; ++i){
            cin>>price[i][0];
            for(int j = 1; j <= price[i][0]; ++j){
                cin>>price[i][j];
            }
        }
        memset(memo,-1,sizeof memo);
        int res=shop(m,0);
        if(res<0) cout<<"no solution";
        else cout<<res;
        cout<<endl;
    }
    return 0;
}