#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[])
{
	int t;
	cin>>t;
	for(int i=1;i<=t;++i){
		int n;
		cin>>n;
		vector<int> v;
		for(int j=0;j<n;++j){
			int x;
			cin>>x;
			v.push_back(x);
		}

		sort(v.begin(), v.end());
		cout<<"Case "<<i<<": "<<v[n/2]<<endl;
	}
	return 0;
}