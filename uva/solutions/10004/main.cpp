#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

vi adl[201];
int color[201];

int n;

bool isBipartite(int v, short c){
    color[v]=(c+1)%2;
    for(int i=0;i<adl[v].size();++i){
        if(color[adl[v][i]]==-1)
            return isBipartite(adl[v][i],color[v]);
        else if(color[adl[v][i]]==color[v]) 
            return false;
    }
    return true;
}

int main(){
	ios::sync_with_stdio(false);
	while(cin>>n&&n>0){
		mem(color,-1);
		int start=0;
		int l;cin>>l;
		
		while(l--){
			int a,b;
			cin>>a>>b;
			start=a;
			adl[a].pb(b);
			adl[b].pb(a);
		}
		if(isBipartite(start,0)){
			cout<<"BICOLORABLE.";
		}else cout<<"NOT BICOLORABLE.";
		cout<<endl;
		for(int i=0; i<201;++i){
			adl[i].clear();
		}
	}
	
	return 0;
}
