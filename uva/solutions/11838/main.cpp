#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
const int tam=2000;


vi adl[tam];
vi stck;
int low[tam];
int num[tam];
bool visited[tam];
int cnt=0;
map<int,vi> strcc;

void scc(int node){
	low[node]=++cnt;
	num[node]=low[node];
	stck.pb(node);
	visited[node]=true;
	for(int i=0; i<(int)adl[node].sz;++i){
		int v=adl[node][i];
		if(num[v]==-1) scc(v);
		if(visited[v]) low[node]=min(low[v],low[node]);
	}

	if(low[node]==num[node]){
		while(1){
			int v=stck.back();
			stck.pop_back();
			visited[v]=false;
			strcc[node].pb(v);
			if(v==node) break;
		}
	}
}

int main(){
	ios::sync_with_stdio(false);
	int n,m;
	while(cin>>n>>m){
		mem(visited,false);
		mem(num,-1);
		if(n==0&&m==0) break;
		for(int i=0; i<m;++i){
			int a,b,c;
			cin>>a>>b>>c;
			adl[a].pb(b);
			if(c==2){
				adl[b].pb(a);
			}
		}

		for(int i=1; i<n+1;++i){
			if(num[i]==-1) scc(i);
		}


		if(strcc.sz==1){
			cout<<"1";
		}else{
			cout<<"0";
		}cout<<endl;
		
		strcc.clear();
		for(int i=1; i<n+1;++i){
			adl[i].clear();
		}
	}
	
	
	return 0;
}
