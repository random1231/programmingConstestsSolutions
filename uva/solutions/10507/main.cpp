#include <bits/stdc++.h>

using namespace std;

template<class T>
class UnionFind
{
public:
    unordered_map<T, T> parent;
    unordered_map<T, int> degree;
    int connectedComp=0;
    T getParent(T vertex){
        if(parent.find(vertex)==parent.end())
            return -1;
        if(parent[vertex]==vertex)
            return vertex;
        getParent(parent[vertex]);
        parent[vertex]=getParent(parent[vertex]);
        return parent[vertex];
    }

    bool exists(T vertex){
        if(parent.find(vertex)==parent.end())
            return false;
        return true;
    }

    bool inSameSet(T x, T y){
        return (getParent(x)==getParent(y));
    }

    void unionSet(T a, T b){
        T x=getParent(a);
        T y=getParent(b);
        if(x==y) return;
        if(degree[x]>degree[y]){
            parent[y]=x;
            degree[x]=degree[y]+degree[x];
        }else{
            parent[x]=y;
            degree[y]=degree[y]+degree[x];
        }
        --connectedComp;
    }
    void insertSet(T x){
        parent.insert({x,x});
        degree.insert({x,1});
        ++connectedComp;
    }
};

void create(char a, char b, bool created[], UnionFind<char> uf[]){
    uf[a].insertSet(a);
    uf[a].insertSet(b);
    uf[a].unionSet(a,b);
    created[a]=true;
}

int main(){
    int n;
    int m;
    while(cin>>n>>m){
        string s;
        cin>>s;
        if(n==s.size()){
            cout<<"WAKE UP IN, 0, YEARS";
            cout<<endl;
            continue;
        }
        int ans=0;
        int xxx=n-s.size();
        bool woke[123];
        memset(woke,false,sizeof woke);
        for(auto item : s){
            woke[item]=true;
        }
        UnionFind<char> uf[123];
        bool created[123];
        memset(created,false,sizeof created);
        for(int i = 0; i < m; ++i){
            string tmp;
            cin>>tmp;
            char a=tmp[0];
            char b=tmp[1];
            if(!created[a]){
                create(a,b,created,uf);
                if(!created[b]){
                    create(b,a,created,uf);
                }else{
                    uf[b].insertSet(a);
                    uf[a].unionSet(a,b);
                }
           }else{
                uf[a].insertSet(b);
                uf[a].unionSet(a,b);
                if(!created[b]){
                    create(b,a,created,uf);
                }else{
                    uf[b].insertSet(a);
                    uf[a].unionSet(a,b);
                }
           }
        }

        int num=0;

        while(1){
            vector<int> wk;
            for(int i = 0; i < 123; ++i){
                if(created[i]==true&&!woke[i]){
                    int cnt=0;
                    for(int j = 0; j < 123; ++j){
                        if(woke[j]==true){
                            char c=j;
                            if(uf[i].exists(c)){
                                ++cnt;
                            }
                            if(cnt>=3){
                                wk.push_back(i);
                                ++num;
                                break;
                            }
                        }
                    }
                }
            }
            if(!wk.empty()){
                ++ans;
                for(auto item : wk){
                    woke[item]=true;
                }
            }else break;
        }
        if(num==xxx)
            cout<<"WAKE UP IN, "<<ans<<", YEARS";
        else
            cout<<"THIS BRAIN NEVER WAKES UP";
        cout<<endl;
    }
    return 0;
}