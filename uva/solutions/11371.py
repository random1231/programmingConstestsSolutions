from sys import stdin
from math import inf

for s in stdin:
	a=int(''.join(sorted(s, reverse=True)))
	mn=2000000001
	x=0
	idx=-1
	l=[int(num) for num in s.strip()]
	flag=False
	zeros=0
	while x<len(l):
		if s[x]!='0':
			if int(s[x])<mn:
				mn=int(s[x])
				idx=x
				flag=True
		else:
			zeros+=1
		x+=1
	del(l[idx])
	aux=''.join(sorted(''.join(str(tmp) for tmp in l)))
	b=''
	if zeros == len(l):
		b=int(s)
	elif len(s.strip())==1:
		b=str(mn)
	elif int(aux)!=0:
		b=str(mn)+aux
	else:
		b=str(mn)
	res=a-int(b)
	print(str(a)+" - "+str(b)+" = "+str(res)+" = 9 * "+str(res//9))