#include <bits/stdc++.h>

using namespace std;

vector<int> divisors(long long n) {
	long long lim = sqrt(n);
	vector<int> v;
	for(long long i = 2; i <= lim; ++i){
		if(n%i==0){
			v.push_back(i);
			if(i!=n/i)
				v.push_back(n/i);
		}
	}
	return v;
}

int main(){
	int n;
	while(cin >> n && n){
		auto v = divisors(n);
		int ans = v.size()+2;
		if(!v.empty()){
			for(int i=0;i<v.size()-1;++i){
				for(int j=i+1;j<v.size();++j){
					auto g = __gcd(v[i], v[j]);
					if((v[i]/g)*v[j]==n)
						++ans;
				}
			}
		}
		else if(n == 1)
			ans = 1;
		cout << n << " "<< ans << endl;
	}
	return 0;
}