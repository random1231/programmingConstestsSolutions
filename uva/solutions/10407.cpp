#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[])
{
	while(1){
		std::vector<int> v;
		bool fl = true;
		int x=0;
		while(cin >> x&&x){
			v.push_back(x);

		}

		if(v.empty())
			break;
		int mn = *min_element(v.begin(), v.end());
		int g=0;
		for(auto x:v)
			g = __gcd(g, x-mn);

		cout<<g<<endl;
	}
	
	return 0;
}