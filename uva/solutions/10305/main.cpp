#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
const int tam=100;

vi tp;
vi adl[tam+10];
bool visited[tam+10];
vi ts;


void topo(int node){
	visited[node]=true;
	for(int i=0; i<(int)adl[node].sz;++i){
		int v=adl[node][i];
		if(!visited[v]) topo(v);
	}
	
	ts.pb(node);
}

int main(){
	ios::sync_with_stdio(false);
	int n,m;
	while(cin>>n>>m){
		if(n==0&&m==0) break;
		mem(visited,false);
		for(int i=0; i<m;++i){
			int a,b;cin>>a>>b;
			adl[a].pb(b);
		}
		for(int i=1; i<=n;++i){
			if(!visited[i]){
				topo(i);
			}
		}
		
		for(int i=(int)ts.sz-1;i>=0;--i){
			cout<<ts[i];
			if(i>0){
				cout<<" ";
			}
		}
		cout<<endl;
		
		for(int i=0; i<101;++i){
			adl[i].clear();
		}
		ts.clear();
	}
	
	
	
	return 0;
}

