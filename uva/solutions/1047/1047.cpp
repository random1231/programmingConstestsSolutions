#include <bits/stdc++.h>
using namespace std;

int n, tow;
vector<pair<int,int>> vans;
int ans;

void solve(vector<pair<int,int>> &a, vector< vector<int> > cm){
    auto n=a.size();
    auto lim=(1<<n);
    for(int i=0;i<lim;++i){
        int curans=0;
        int counter=0;
        for(int j=0;j<n;++j){
            //Test if jth bit is on
            if(i&(1<<j)){
                ++counter;
            }
        }

        if(counter!=tow)
            continue;
        
        vector<pair<int,int>> subs;
        for(int j=0;j<n;++j){
            //Test if jth bit is on
            if(i&(1<<j)){
                subs.push_back(a[j]);
            }
        }

        for(auto item : subs)
            curans+=item.first;
        for(int k = 0; k < cm.size(); ++k){
            auto fl=false;
            auto valid=true;
            for(int l = 0; l < cm[k].size()-1; ++l){
                for(auto item : subs){
                    if(item.second==cm[k][l]){
                        fl=true;
                        break;
                    }else fl=false;
                }

                if(!fl){
                    valid=false;
                    break;
                }
            }
            if(valid)
                curans-=cm[k][cm[k].size()-1];
        }

        if(curans>ans){
            ans=curans;
            vans=subs;
        }
    }
}

int main(){
    int tc=1;
    while(cin>>n>>tow&&n&&tow){
        vector<pair<int,int>> a;
        for(int i = 0; i < n; ++i){
            int tmp;
            cin>>tmp;
            a.push_back({tmp,i+1});
        }
        vector< vector<int> > cm;
        int c;
        cin>>c;

        for(int i = 0; i < c; ++i){
            vector<int> v;
            int k;
            cin>>k;
            for(int j = 0; j <= k; ++j){
                int tmp;
                cin>>tmp;
                v.push_back(tmp);
            }

            cm.push_back(v);
        }

        solve(a,cm);

        cout<<"Case Number  "<<tc++;
        cout<<endl;
        cout<<"Number of Customers: "<<ans;
        cout<<endl;
        cout<<"Locations recommended:";
        for(auto x :vans)
            cout<< " "<<x.second;
        cout<<endl;
        cout<<endl;
        vans.clear();
        ans=0;
    }
    return 0;
}