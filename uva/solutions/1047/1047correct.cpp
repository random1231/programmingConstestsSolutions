#include <bits/stdc++.h>
using namespace std;

int main(){
    int nTower, nBuilt,tc=1;
    while(cin>>nTower>>nBuilt&&nTower&&nBuilt){
        vector<int> v;
        unordered_map<int, int> m;
        int bestChoice;

        while(nTower--){
            int tmp;
            cin>>tmp;
            v.push_back(tmp);
        }
        int common;
        cin>>common;
        while(common--){
            int tam;
            cin>>tam;
            int bs=0;//Representing areas as a bitset
            while(tam--){
                int tmp;
                cin>>tmp;
                bs=bs|(1<<(tmp-1));
            }
            int val;
            cin>>val;
            m[bs]=val;
        }
        //Generating subsets
        int area=-1e9;
        auto n=v.size();
        auto lim=(1<<n);
        for(int i=0;i<lim;++i){
            int curArea=0;
            if(__builtin_popcount(i)!=nBuilt)
                continue;
            for(int j=0;j<n;++j){
                if(i&(1<<j))
                    curArea+=v[j];
            }
            //Apply inclusion-exclusion
            for(auto item : m){
                int intrscn=__builtin_popcount(i&item.first);//AND gives the itersection of both sets(represented as bitsets)
                if(intrscn>=2)//There must be at least 2 elements in order to have intersection (common values)
                    curArea=curArea-((intrscn-1)*item.second);//Substracting over-counted elements.
            }

            if(curArea>area){
                area=curArea;
                bestChoice=i;
            }
        }

        cout<<"Case Number  "<<tc++;
        cout<<endl;
        cout<<"Number of Customers: "<<area;
        cout<<endl;
        cout<<"Locations recommended:";
        for(int i = 0; i < n; ++i){
            if(bestChoice&(1<<i))
                cout<<" "<<i+1;
        }
        cout<<endl;
        cout<<endl;


    }
    return 0;
}