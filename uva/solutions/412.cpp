#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[])
{
	int n;
	while(cin >>n&&n){
		std::vector<int> v;
		for(int i=0;i<n;++i){
			int x;
			cin >> x;
			v.push_back(x);
		}

		int c=0;

		for(int i=0;i<n-1;++i){
			for(int j=i+1;j<n;++j){
				if(__gcd(v[i], v[j]) == 1)
					++c;
			}
		}


		int t = (n*(n-1))/2;

		if(c == 0)
			cout << "No estimate for this data set."<<endl;
		else{
			double ans = (6*t*1.0)/c;
			cout << fixed << setprecision(6) << sqrt(ans)<<std::endl;
		}
	}


	return 0;
}