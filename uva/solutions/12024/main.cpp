#include <bits/stdc++.h>

using namespace std;

int n;

string s="abcdefghijkl";
string s2;
int ans;
int f[13];

void fact(){
	f[1]=1;
	int ans=1;
	for(int i=2;i<=12;++i){
		f[i]=f[i-1]*i;
	}
	
}


void solve(){
	for (int i = 0; i < n; i++){
		if(s2[i]==s[i]) return;
	}
	++ans;
	
}

int val[13];

int main(){
	int t;
	cin>>t;
	fact();
	memset(val,-1,sizeof val);
	val[11]=14684570;
	val[12]=176214841;
	while(t--){
		cin>>n;
		if(val[n]!=-1){
			cout<<val[n]<<"/"<<f[n]<<endl;
			continue;
		}
		ans=0;
		s2="";
		for (int i = 0; i < n; i++)
			s2+=s[i];
		
		do{
			solve();
		
		}while(next_permutation(s2.begin(),s2.end()));
		val[n]=ans;
		cout<<ans<< "/"<<f[n]<<endl;
	}
	return 0;
}
