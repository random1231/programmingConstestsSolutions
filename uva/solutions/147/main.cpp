#include <bits/stdc++.h>

#define MAXN 100
#define MAXV 30001

using namespace std;

int n=11, val[]={10000,5000,2000,1000,200,500,100,50,20,10,5};
long long dp[MAXN][MAXV];

long long solve(int type, long long value) {
    if (value == 0)
        return 1;
    if (value < 0 || type == n)
        return 0;
    if (dp[type][value] != -1)
        return dp[type][value];
   return dp[type][value] = solve(type + 1, value) + solve(type, value - val[type]);
}

int main(){
    //double x;
    string x;
    memset(dp,-1,sizeof dp);

    while(cin>>x){
        string xxx=x;
        for(int i = 0; i < xxx.size(); ++i){
            if(xxx[i]=='.'){
                xxx.erase(xxx.begin()+i);
                break;
            }
        }        long long xx=stoll(xxx);
        if(xx==0) break;
        auto ans=solve(0,xx);
        cout<<setw(6)<<fixed<<setprecision(2)<<x<<setw(17);
        cout<<ans<<endl;
    }
    return 0;
}