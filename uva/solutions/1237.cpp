#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[])
{
	int t;
	cin>>t;
	bool f=false;

	while(t--){
		if(f)
			cout<<endl;
		f=true;
		int n;
		cin>>n;
		map<string,pair<int,int>> m;
		while(n--){
			string s;
			int l,h;
			cin>>s>>l>>h;
			m.insert({s,{l,h}});
		}

		int q;
		cin>>q;
		while(q--){
			int x;
			cin>>x;
			int cnt=0;
			string cur;
			for(auto item : m){
				if(x<item.second.first||x>item.second.second)
					continue;
				++cnt;
				if(cnt>1)
					break;
				cur=item.first;
			}

			if(cnt>1||cnt==0)
				cout<<"UNDETERMINED"<<endl;
			else
				cout<<cur<<endl;
		}
	}
	return 0;
}