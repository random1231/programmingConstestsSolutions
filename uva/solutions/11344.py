m=0
def div1():
	return True
def div2():
	return int(m[len(m)-1])%2==0
def div3():
	return sum([int(x) for x in m])%3==0
def div4():
	return int(m[len(m)-2]+m[len(m)-1])%4==0 if len(m)>=2 else int(m)%4==0
def div5():
	return int(m[len(m)-1])%5==0
def div6():
	return div2() and div3()
def div7():
	l=[1,3,2,6,4,5]
	ans=0
	i=0
	for x in reversed(m):
		ans=ans+int(x)*l[i]
		i=(i+1)%6
	return ans%7==0
def div8():
	return int(m[len(m)-3]+m[len(m)-2]+m[len(m)-1])%8==0 if len(m)>=3 else int(m)%8==0
def div9():
	return sum([int(x) for x in m])%9==0
def div10():
	return int(m[len(m)-1])==0
def div11():
	ans=0
	for i in range(len(m)):
		ans=ans+(int(m[i]) if i%2==0 else int(m[i])*-1)
	return ans%11==0
def div12():
	return div3() and div4()
for _ in range(int(input())):
	m=input().strip();
	s=input()
	l=[int(x) for x in s.split()]
	del(l[0])
	fl=True
	for i in l:
		if eval("div"+str(i)+"()")==False:
			fl=False
			break
	if fl==True:
		print(m+" - Wonderful.")
	else:
		print(m+" - Simple.")