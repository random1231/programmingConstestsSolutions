from sys import stdin
from fractions import gcd
def main():
	for line in stdin:
		num = int(line)
		trip = 0
		ma = [False]*1000009
		m=2
		c=0
		while(c<=num):
			n=m-1
			while(n>0):
				if (m+n)%2!=1 or gcd(m, n)!=1:
					n-=1
					continue
				b = m*m-n*n
				a = 2*m*n
				c = m*m+n*n
				if c <= num:
					if a*a+b*b==c*c:
						l = 1
						while(l*c <= num):
							ma[a*l] = True
							ma[b*l] = True
							ma[c*l] = True
							l+=1
						trip+=1
				n-=1
			m+=1
		ans = 0
		i = 1
		while i<=num:
			if(ma[i]!=True):
				ans+=1
			i+=1
		print(trip, ans)

main()