#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin
#define clear(a,b) memset(a,b,sizeof a)
#define tam 31269

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef pair<pair<int,int>,int> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;


ll ones[tam+10];
ll xxx[12];
int n;

//a[i]=(a[i-1]-1+i+(i-10+1))

void generate(){
	ones[1]=1;
	for(ll i=2; i<=tam;++i){
		//cout<<i<<endl;
		ones[i]=ones[i-1]-1+i;
		for(ll j=10;;j*=10){
			if(i<j) break;
			ones[i]+=(i-j);
		}	
	}
}

void generate2(){
	for(int i=1; i<11;++i){
		xxx[i]=xxx[i-1]+(pow(10.0,i)-pow(10.0,i-1))*i;
	}
}

int main(){
	generate();
	generate2();
	int t;cin>>t;while(t--){
		cin>>n;
		ll res=n-ones[upb(ones,ones+tam,n)-ones-1]+1;
		int ex=lob(xxx,xxx+11,res)-xxx-1;
		ll res2=xxx[ex];
		ll tmp=res-res2;
		ll rema=tmp%(ex+1);
		ll divisions=0;
		ll num=((tmp/(ex+1)));

		if(rema!=0){
			num=((tmp/(ex+1)+1)*(ex+1));
			divisions=num-tmp;
			num=num/(ex+1);
		}

		ll ans=(((ll)pow(10.0,ex)+num-1)/((ll)pow(10.0,divisions)))%10;
		cout<<ans<<endl;
	}
	return 0;
}
