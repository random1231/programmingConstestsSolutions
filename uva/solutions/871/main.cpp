#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

char grid[100][100];
int N;

int dr[]={-1,-1,0,1,1,1,0,-1};
int dc[]={0,1,1,1,0,-1,-1,-1};
int row,column;

int floodfill(int x,int y){
	if(x<0||x>=row||y<0||y>=column) return 0;
	//~ cout<<x<<" " <<y<<endl;
	auto &ref=grid[x][y];
	if(ref=='v'||ref=='0') return 0;
	ref='v';
	int ans=1;
	for(int i=0; i<8;++i){
		ans+=floodfill(x+dr[i],y+dc[i]);
	}
	return ans;

}

int main(){
	ios::sync_with_stdio(false);
	int tc;cin>>tc;
	string s;
	getline(cin,s);
	getline(cin,s);
	for(int k=0; k<tc;++k){
		row=0;
		int n;
		while(getline(cin,s)){
			if(s.sz==0) break;
			n=s.sz;
			for(int i=0; i<n;++i){
				grid[row][i]=s[i];
			}
			++row;
		}
		column=n;

		int ans=0;
		for(int i=0; i<row;++i){
			for(int j=0; j<column;++j){
				ans=max(ans,floodfill(i,j));
			}
		}
		
		cout<<ans;
		cout<<endl;
		if(k!=tc-1) cout<<endl;
		
	}

	return 0;
}
