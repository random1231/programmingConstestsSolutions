#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

char grid[26][26];
int m,n,pos1,pos2;

int dr[]={-1,-1,0,1,1,1,0,-1};
int dc[]={0,1,1,1,0,-1,-1,-1};

int floodfill(int x,int y){
	if(x<0||x>=m||y<0||y>=n) return 0;
	auto &ref=grid[x][y];
	if(ref=='w'||ref=='v') return 0;
	ref='v';
	
	int ans=1;
	for(int i=0; i<8;++i){
		ans+=floodfill(x+dr[i],y+dc[i]);
	}
	if(y==n-1){
		//~ if(x>0){
			//~ ans+=floodfill(x-1,0);
		//~ }
		ans+=floodfill(x,0);
		//~ if(x<m-1){
			//~ ans+=floodfill(x+1,0);
		//~ }
	}else if(y==0){
		ans+=floodfill(x,n-1);
	}
	
	return ans;
}

int main(){
	ios::sync_with_stdio(false);
	cin>>m>>n;
	for(int i=0; i<m;++i){
		for(int j=0; j<n;++j){
			cin>>grid[i][j];
		}
	}
	
	cin>>pos1>>pos2;
	floodfill(pos1,pos2);
	int ans=0;
	for(int i=0; i<m;++i){
		for(int j=0; j<n;++j){
			ans=max(ans,floodfill(i,j));
		}
	}
	cout<<ans;
	cout<<endl;
	
	return 0;
}
