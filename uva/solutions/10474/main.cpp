#include <bits/stdc++.h>

using namespace std;


int main(){
	int n,q;
	int tc=1;
	while(cin>>n>>q&&n&&q){
		vector<int> a;
		for (int i = 0; i < n; i++)
		{
			int x;cin>>x;a.push_back(x);
		}
		
		
		
		
		sort(a.begin(),a.end());
		
		cout<<"CASE# "<<tc++<<":"<<endl;
		for (int i = 0; i < q; i++){
			int query;
			cin>>query;
			auto res= lower_bound(a.begin(),a.end(),query);
			int pos=res-a.begin();
			if(pos<n&&a[pos]==query){
				cout<<query<< " found at "<<pos+1<<endl;
			}else cout<<query<< " not found"<<endl;
		}
		
	}
	return 0;
}
