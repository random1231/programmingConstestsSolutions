#include <bits/stdc++.h>

using namespace std;
typedef  long long ll;
int ma[1000010];

int main(int argc, char const *argv[])
{
	ll num;
	int i = 1;
	while(scanf("%lld", &num) == 1){
		memset(ma, 0, sizeof(ma));
		ll m = 2;
		ll trip = 0;
		ll a , b, c=0;
		for(;c<=num;++m){
			for(ll n=m-1;n>0;--n){
				if((m+n)%2!=1||__gcd(n, m)!=1)
					continue;
				a = 2*m*n; b=m*m-n*n; c=m*m+n*n; //Look, a primitive pythagorean triple, yay!
				if(c > num) continue;
				++trip;
				ll l = 1;
				//Generating pythagorean triples less than num
				//This the answer to the second part of the exercise
				while(l*c <= num){
					ma[a*l] = 1;
					ma[b*l] = 1;
					ma[c*l] = 1;
					++l;
				}
			}
		}

		ll ans = 0;
		for(ll i = 1; i<=num;++i)
			if(ma[i]==1){
				//ma[i] = 0;
				++ans;
			}

		printf("%lld %lld\n",trip,num-ans);
		++i;
	}
	return 0;
}