#include <bits/stdc++.h>

using namespace std;

unsigned long long f[21];

void pre(){
	
	for(int i=0;i<=20;++i)
		f[i]=1;
	for(int i=2;i<=20;++i)
		f[i]=i*f[i-1];
}

int main(int argc, char const *argv[])
{
	int n;
	cin >> n;
	pre();
	for(int i =1;i<=n;++i){
		std::map<char, int> m;
		string s;
		cin >> s;
		for(auto c:s){
			if(m.find(c)!= m.end())
				++m[c];
			else
				m[c] = 1;
		}
	
		unsigned long long ans = f[(int)s.size()];

		for(auto it:m){
			ans/=f[it.second];
		}

		cout << "Data set "<<i<<": "<<ans <<endl;
	}
	return 0;
}
