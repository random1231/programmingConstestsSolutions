#include <bits/stdc++.h>

using namespace std;

string v[101];
string r;
string dig[]={"0","1","2","3","4","5","6","7","8","9"};
int n;

void solve(int idx, string s){

    if(idx==r.size()){
        cout<<s;
        cout<<endl;
        return;
    }

    if(r[idx]=='#'){
        for(int i = 0; i < n; ++i){
            solve(idx+1,s+v[i]);
        }
    }

    else if(r[idx]=='0'){
        for(int i = 0; i < 10; ++i){
            solve(idx+1,s+dig[i]);
        }
    }

}


int main(){
    while(cin>>n){
        for(int i = 0; i < n; ++i){
            cin>>v[i];
        }

        int m;
        cin>>m;
        cout<<"--";
        cout<<endl;
        while(m--){
            cin>>r;
            solve(0,"");
        }

    }
    return 0;
}