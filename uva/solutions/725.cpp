#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[])
{
	ios::sync_with_stdio(false);
	int n;
	bool fst=true;
	while(cin>>n&&n){
		if(!fst)
			cout<<endl;
		fst=false;
		bool f=true;
		for(int i=1234;i<=98765/n;++i){
			bool leadZ=false;
			set<char> s;
			bool no=false;
			auto x=n*i;
			auto s1=to_string(i);
			auto s2=to_string(x);
			for(auto c:s1){
				if(s.find(c)!=s.end()){
					no=true;
					break;
				}
				else
					s.insert(c);
			}

			if(no){
				continue;
			}

			if(s1.size()<5&&s.find('0')==s.end()){
				leadZ=true;
				s.insert('0');
			}



			for(auto c:s2){
				if(s.find(c)!=s.end()){
					no=true;
					break;
				}
				else
					s.insert(c);
			}

			if(no)
				continue;

			for(auto j='0';j<='9';++j){
				if(s.find(j)==s.end()){
					no=true;
					break;
				}
			}

			if(!no){
				f=false;
				cout<<x<<" / "<< (leadZ==true?"0":"") <<i<<" = "<<n<<endl;
			}

		}
		if(f)
			cout<<"There are no solutions for "<<n<<"."<<std::endl;
	}
	return 0;
}