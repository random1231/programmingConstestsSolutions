#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
vector<int> primes;
bitset<10000001> bs;
vector<int> v;

void sieve(long long upperbound){
    sieve_size = upperbound+1;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.push_back((int)i);
	    }	    
}

int divisorSum(long long n) {
	long long index = 0, primeF = primes[index];
	long long ans=1;
	long long lim = sqrt(n);
	while(primeF <= lim){
		long long power=0;
		while(n%primeF == 0){
			n /= primeF;
			++power;
		}

		ans*=((long long)pow((double)primeF, power+1.0)-1)/(primeF-1);

		primeF = primes[++index];
	}

	if (n != 1)
		ans*=((long long)pow((double)n, 2.0)-1)/(n-1);
	return ans;
}

void pre(){
	for (int i = 0; i <= 1000; ++i)
		v.push_back(divisorSum(i));
}

int main(){
	sieve(1000);
	pre();
	int n;
	int c=1;
	while(cin>>n&&n){
		int i;
		bool found=false;
		for (i = 1000; i >= 0; --i)
			if(v[i]==n){
				found=true;
				break;
		}
		cout<<"Case "<<c<<": "<<(found?i:-1)<<std::endl;
		++c;
	}
}
