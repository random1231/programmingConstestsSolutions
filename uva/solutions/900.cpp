#include <bits/stdc++.h>
using namespace std;

long long wall[59];

void pre(){
	wall[0] = 1;
	wall[1] = 1;
	for(int i = 2; i <= 50; ++i){
		wall[i] = wall[i-1]+wall[i-2];
	}
}

int main(int argc, char const *argv[])
{
	int n;
	pre();
	while(cin >> n && n != 0){
		cout << wall[n] << endl;
	}
	return 0;
}