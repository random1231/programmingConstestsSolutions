#include <bits/stdc++.h>

using namespace std;


int binaryS(std::vector<int> v,int a){
	int right=v.size()-1;
	int left=0;
	int mid,ans;
	while(1){
		if(right-left==1){
			if((v[right]-a)<(a-v[left]))
				ans=v[right];
			else
				ans=v[left];
			break;
		}
		mid=(left+right)/2;
		if(v[mid]>a)
			right=mid;
		else if(v[mid]<a)
			left=mid;
		else{
			ans=a;
			break;
		}
	}

	return ans;	
}

int main(int argc, char const *argv[])
{
	int n;
	int cas=1;
	while(cin>>n&&n){
		vector<int> ar;
		for(int i=0;i<n;++i){
			int tmp;
			cin>>tmp;
			ar.push_back(tmp);
		}
		//sort(ar.begin(), ar.end());

		int q;
		cin>>q;
		cout<< "Case "<<cas<<":"<<endl;
		vector<int> s;
		for(int i=0;i<n;++i){
			for(int j=i+1;j<n;++j){
				auto tmp=ar[i]+ar[j];
					s.push_back(tmp);
			}
		}

		sort(s.begin(), s.end());

		/*cout<<"Values:"<<endl;
		for(auto va:s)
			cout<<va<<" ";
		cout<<endl;*/

		while(q--){
			int k;
			cin>>k;
			auto ans=binaryS(s,k);
			cout<<"Closest sum to "<<k<<" is "<<ans<<".";
			cout<<endl;
		}
		++cas;
	}
	return 0;
}