def fac(n):
	ans=1
	for i in range(2,n+1):
		ans*=i
	return ans

def main():
	for _ in range(int(input())):
		l=[int(x) for x in input().split()]
		n=l[0]
		k=l[1]
		del(l[0])
		del(l[0])
		white=n-sum(l)
		if k > n+1:
			print('0')
			continue
		ans=(fac(white+1)//fac(k))//fac(white+1-k)
		print(ans)


main()