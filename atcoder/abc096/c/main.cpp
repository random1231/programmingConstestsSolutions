#include <bits/stdc++.h>

using namespace std;

char a[51][51];
int n,m;

bool valid(int i, int j){
    if(i-1>=0){
        if(a[i-1][j]=='#')
            return true;
    }
    if(i+1<=n-1){
        if(a[i+1][j]=='#')
            return true;
    }
    if(j+1<=m-1){
        if(a[i][j+1]=='#')
            return true;
    }

    if(j-1>=0){
        if(a[i][j-1]=='#')
            return true;
    }
    return false;
}

int main(){
    ios::sync_with_stdio(false);
    int ans=0;
    cin>>n>>m;
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < m; ++j){
            cin>>a[i][j];
        }
    }


    for(int i = 0; i < n; ++i){
        for(int j = 0; j < m; ++j){
            if(a[i][j]=='#'){
                if(!valid(i, j)){
                    cout<<"No"<<endl;
                    return 0;
                }
            }
        }
    }


    cout<<"Yes";
    cout<<endl;


    return 0;
}