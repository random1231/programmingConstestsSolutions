#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
bitset<1000009> bs;

void sieve(long long upperbound, vector<long long> &primes){
    sieve_size = upperbound;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
        if(bs[i]) {
            for (long long j = i*i; j <= sieve_size; j+=i)
                bs[j] = 0;
            primes.push_back(i);
        }
}

bool isPrime(long long n, vector<long long> &primes) {
    if(n <= sieve_size) return bs[n];
    auto upp = (long long)sqrt(n);
    for (int i = 0;primes[i] <= upp and i <(int)primes.size(); ++i)
        if(n % primes[i] == 0) return false;
    return true;
}

int main(){
    ios::sync_with_stdio(false);
    vector<long long> v;
    sieve(5477, v);
    v.erase(v.begin()+2);
    v.erase(v.begin()+3);
    v.erase(v.begin()+4);
    int n;
    cin>>n;
    srand(time(NULL));
    //vector<long long> a;
    while(1){
        vector <long long> a;
        int cn=0;
        a.push_back(2);
        while(cn<n-1){
                a.push_back(v[(int)rand()%(v.size())]);++cn;
            }

        for(int i = 0; i <a.size(); ++i) for(int j = 0; j <a.size(); ++j)
            for(int k = 0; k <a.size(); ++k) for(int l = 0; l <a.size(); ++l)
                for(int m = 0; m <a.size(); ++m) {
                    if(isPrime(a[i]+a[j]+a[k]+a[l]+a[m],v)){
                        cerr<<a[i]<<" "<< a[j]<<" "<< a[k]<<" "<< a[l]<<" "<< a[m]<<endl;
                        continue;
                    }
                }
        for(auto item : a){
           cout<<item<<" ";
        }
           return 0;
        
    }

    
    return 0;
}