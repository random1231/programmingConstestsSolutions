#include <bits/stdc++.h>

using namespace std;

int main(){
    ios::sync_with_stdio(false);
    int n, m, p,k;
    cin>>n>>m>>p;
    cin>>k;
    priority_queue<int> q;
    q.push(n);
    q.push(m);
    q.push(p);

    while(k--){
        auto x=q.top()*2;
        q.pop();
        q.push(x);
    }
    int ans=0;
    while(!q.empty()){
        ans+=q.top();
        q.pop();
    }

    cout<<ans;
    cout<<endl;

    return 0;
}