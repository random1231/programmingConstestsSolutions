#include <bits/stdc++.h>
using namespace std;

int binaryS(vector<int> v,int a){
    int right=v.size()-1;
    int left=0;
    int mid,ans;
    while(1){
        if(right-left==1){
            if((v[right]-a)<(a-v[left]))
                ans=v[right];
            else
                ans=v[left];
            break;
        }
        mid=(left+right)/2;
        if(v[mid]>a)
            right=mid;
        else if(v[mid]<a)
            left=mid;
        else{
            ans=a;
            break;
        }
    }

    return ans; 
}

int main(){
    int n;
    cin>>n;
    vector<int> v;
    for(int i = 0; i < n; ++i){
        int tmp;
        cin>>tmp;
        v.push_back(tmp);
    }

    sort(v.begin(), v.end());
    if(n==2)
    {
        cout<<v[1]<< " "<<v[0];
        cout<<endl;
        return 0;
    }


    int mx=v[n-1];
    int mm=(mx+1)/2;
    v.pop_back();

    cout<<mx<< " "<<binaryS(v,mm);
    cout<<endl;


    return 0;
    
}