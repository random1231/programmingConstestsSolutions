#include <bits/stdc++.h>

using namespace std;

int main(){
    ios::sync_with_stdio(false);
    int n;
    int ans=INT_MAX;
    string s;
    cin>>n;
    cin>>s;
    int w=0;
    int e=0;
    int ec=count(s.begin(), s.end(),'E');
    for(int i = 0; i < n; ++i){
        int r=ec-e;
        if(s[i]=='E') --r;
        ans=min(ans,r+w);
        if(s[i]=='E') ++e;
        else ++w;
    }

    cout<<ans<<endl;

    return 0;
}