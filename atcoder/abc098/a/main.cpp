#include <bits/stdc++.h>

using namespace std;

int main(){
    ios::sync_with_stdio(false);
    int a,b;
    cin>>a>>b;
    vector<int> v{a+b,a-b,a*b};
    sort(v.rbegin(), v.rend());
    cout<<v[0]<<endl;
    return 0;
}