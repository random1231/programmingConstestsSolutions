#include <bits/stdc++.h>

using namespace std;

int main(){
    ios::sync_with_stdio(false);
    int n;
    string s;
    cin>>n;
    cin>>s;
    int ans=0;
    for(int i = 0; i < n; ++i){
        set<char> s1;
        set<char> s2;
        // cerr<<"ok"<<endl;
        // cerr<<i<<endl;
        for(int j = 0; j <= i; ++j){
            s1.insert(s[j]);
            // cerr<<s[j]<<endl;
            // cerr<<s1[s1.size()]<<endl;
        }
        //cerr<<"---"<<endl;
        for(int j = i+1; j < n; ++j){
            // cerr<<s[j]<<endl;
            s2.insert(s[j]);
            // cerr<<s2[s2.size()]<<endl;
        }

        int sz=0;
        
        for(auto x : s1){
            if(s2.find(x)!=s2.end()) ++sz;
        }
        
        ans=max(ans,sz);
    }
    cout<<ans<<endl;

    return 0;
}