#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

bool ans[11110];

void gen(){
	mem(ans,false);
	for(int i=1;i<=25;++i){
		ans[4*i]=true;
	}
	for(int i=1;i<=14;++i){
		ans[i*7]=true;
	}
	
	for(int i=4;i<=92;i+=4){
		//ans[i*7]=true;
		for(int j=7; j<100;j+=7){
			if(i+j>100)break;
			ans[i+j]=true;
		}
	}
}

int main(){
	ios::sync_with_stdio(false);
	gen();
	int n;cin>>n;
	if(ans[n]==true){
		cout<<"Yes";
	}else{
		cout<<"No";
	}
	cout<<endl;
	
	
	return 0;
}
