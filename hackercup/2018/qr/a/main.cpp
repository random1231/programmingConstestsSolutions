#include <bits/stdc++.h>
#define ll long long

using namespace std;



int main(){
    int t;cin>>t;
     ll k,n,v;
    string a[51];
    int cn=1;
    while(t--){
        cin>>n>>k>>v;
        for(int i=0;i<n;++i){
            cin>>a[i];
        }
        set<int> ind;
        for(int i=((k%n)*((v-1)%n))%n,j=0;j<k;++j){
            ind.insert(i);
            ++i;
            if(i>=n) i=i%n;
        }
        cout<<"Case #"<<cn++<<": ";
        int f=true;
        for(auto item:ind){
			if(!f)cout<< " ";
			f=false;
            cout<<a[item];
        }
        cout<<endl;
    }
    

    return 0;
}
