#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin
#define sz size
#define clear(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
const ll mod=1000000007;

char a[3][1001];
ll dp[3][1001][3];
int n;

//0->up
//1->down
//2 right
ll solve(int x,int y,int dir){
	//~ cout<<x<< " "<<y<< " "<<dir<<endl;
	if(x<0||x>2||y>=n||y<0) return 0;
	if(a[x][y]=='#') return 0;
	if(x==2&&y==n-1&&dir!=2){
		return 1;
	}
	
	auto &ref=dp[x][y][dir];
	if(ref!=-1){
		//cerr<<"yes!"<<endl;
		return ref;
	}

	if(dir==0||dir==1){
		return ref=solve(x,y+1,2)%mod;
	}else{
		return ref=(solve(x-1,y,1)%mod+solve(x+1,y,0)%mod)%mod;
	}
}

int main(){
	ios::sync_with_stdio(false);
	int tc;cin>>tc;
	for(int k=0; k<tc;++k){
		clear(dp,-1);
		cin>>n;
		for(int i=0; i<3;++i){
			for(int j=0; j<n;++j){
				cin>>a[i][j];
				//~ cout<<a[i][j];
			}
			//~ cout<<endl;
		}
		cout<<"Case #"<<k+1<<": ";
		cout<<solve(0,0,2);
		cout<<endl;
	}
	
	
	return 0;
}
