#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin
#define sz size
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;


int tc,n,k;

ii a[2002];
vi pre,pos;
int label[2002];
int eq[2002];

void preorder(int node){
	if(node==0){
		return;
	}
	pre.pb(node);
	preorder(a[node].ff);
	preorder(a[node].ss);
}

void postorder(int node){
	if(node==0){
		return;
	}
	
	postorder(a[node].ff);
	postorder(a[node].ss);
	pos.pb(node);
}

bool assign(int node,int value,int start){
	//~ if(node==start) return;
	//~ cout<<node<<" "<<value<<" "<<start;
	//~ cout<<endl;
	label[node]=value;
	if(label[eq[node]]==-1){
		assign(eq[node],value,start);
	}else if(eq[node]==start){
		//~ cout<<"Hans"<<endl;
		return true;
	}else return false;
}

bool solve(){
	preorder(1);
	postorder(1);
	
	int s1=pre.sz();
	int s2=pos.sz();
	assert(s1==s2);
	for(int i=0; i<s1;++i){
		//~ cout<<pre[i]<<" "<<pos[i];
		//~ cout<<endl;
		eq[pre[i]]=pos[i];
	}
	
	int num=0;
	//~ bool can=true;
	vi same;
	for(int i=1; i<n+1;++i){
		if(label[i]==-1){
			if(i==eq[i]){
				same.pb(i);
				continue;
			}
			label[i]=++num;
			
			if(num>k||!assign(eq[i],num,i)){
				return false;
				//break;
			}
			//~ ++num;
		}
	}
	
	int tam=(int)same.sz();
	if(k-num>tam) return false;
	
	for(int i=0; i<tam;++i){
		if(k==num)num=0;
		label[same[i]]=++num;
	}
	
	
	return true;
}

int main(){
	ios::sync_with_stdio(false);
	cin>>tc;
	for(int i=0; i<tc;++i){
		mem(label,-1);
		cout<<"Case #"<<i+1<<": ";
		cin>>n>>k;
		for(int j=1; j<n+1;++j){
			int l,r;
			cin>>l>>r;
			a[j]=make_pair(l,r);
		}
		
		if(solve()){
			for(int j=1; j<=n;++j){
				cout<<label[j];
				if(j!=n) cout<<" ";
			}
				
		}else cout<<"Impossible";
		
		
		cout<<endl;
		pre.clear();
		pos.clear();
	}
	
	return 0;
}
