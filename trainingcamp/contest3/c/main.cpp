#include <bits/stdc++.h>

using namespace std;


int n;
int x=-1;
int y=-1;

bool quad(int i){
    long long aux=i*i+i+2*n;
    long long disc=4*aux+1;
    int res=(int)sqrt(disc);
    
    if(res*res!=disc) return false;
    
    if((res-1)%2!=0) x=-1;
    else
        x=(res-1)/2;
    //cout<<i<< " "<<x<<endl;
    return true;
}

int mx=1<<30;
int res1=-1;
int res2=-1;

bool solve(){
    bool f=false;
    for (int i = (n/2)-1; i >=0; i--){
        if(quad(i)){
            //cout<<i<< " "<<x<<endl;
            if(!f){
                res1=i;
                res2=x;
                mx=abs(res1-res2);
                f=true;
                //cout<<res1<< " "<<res2<<endl;
            }

            else if(abs(x-i)<mx){
                res1=i;
                res2=x;
                mx=abs(res1-res2);
                // cout<<res1<< " "<<res2<<endl;
            }

            /*for(int i=min(res1,res2)+1;i<=max(res1,res2);++i){
                    cout<<i<<"";
                    if(i!=max(res1,res2))
                        cout<<" + ";
                    
                }
            cout<<endl;*/
        }
    }
    
    cout<<res1<< " "<<res2<<endl;
    return f;
}


int main(){
    int t;
    cin>>t;while(t--){
        cin>>n;
        if(n==1){
            cout<<"IMPOSSIBLE"<<endl;
            continue;
        }
        if(n%2==1){
            cout<<n<< " = "<<((n+1)/2-1)<< " + "<<(n+1)/2;
        }
        else{
            if(!solve())
                cout<<"IMPOSSIBLE";
            else{
                cout<<n<< " = ";
                for(int i=min(res1,res2)+1;i<=max(res1,res2);++i){
                    cout<<i<<"";
                    if(i!=max(res1,res2))
                        cout<<" + ";
                    
                }
            }
        }
        cout<<endl;
    }
    return 0;
}