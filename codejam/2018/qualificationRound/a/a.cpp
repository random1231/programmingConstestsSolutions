#include <bits/stdc++.h>
using namespace std;

bool isEq(int i){
    if(i=='S')
        return true;
    return false;
}

int computeD(int &d, string &p){
    int damage=1;
    int cur=0;
    for(auto c : p){
        if(c=='C')
            damage*=2;
        else
            cur+=damage;
    }
    return cur;

}
    
int main(){
    int t;
    ios::sync_with_stdio(false);
    cin>>t;

    for(int tc=1;tc<=t;++tc){
        int d;
        string p;
        cin>>d>>p;
        int cnt=0;
        int impcnt=0;
        bool imp=false;

        auto damage = computeD(d,p);
        if(damage<=d){
            cout<<"Case #"<<tc<<": 0";
            cout<<endl;
            continue;
        }

        int nt=count_if(p.begin(), p.end(),isEq);
        if(nt>d){
            cout<<"Case #"<<tc<<": IMPOSSIBLE"<<endl;
            continue;
        }

        bool fl=false;
        while(1){
            /*if(impcnt==p.size()-1){
                imp=true;
                break;
            }

            impcnt=0;*/

            for(int i = 0; i < p.size()-1; ++i){
                if(p[i]=='C'&&p[i+1]=='S'){
                    auto tmp=p[i];
                    p[i]=p[i+1];
                    p[i+1]=tmp;
                    ++cnt;
                    damage = computeD(d,p);
                    if(damage<=d){
                        fl=true;
                        break;
                    }
                }//else ++impcnt;
            }

            if(fl) break;

        }
        /*if(imp)
            cout<<"Case #"<<tc<<": IMPOSSIBLE";
        else{*/
            cout<<"Case #"<<tc<<": "<<cnt;
        //}
        cout<<endl;

    }
    return 0;
}
