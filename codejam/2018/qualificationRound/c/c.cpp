#include <bits/stdc++.h>
using namespace std;

int grid[1000][1000];
vector<pair<int,int>> s={{500,500},{500,501},{499,500},{499,501},{501,500}, {501,501}};

bool isFilled(vector<pair<int,int>>::iterator &it){
    int x=it->first;
    int y=it->second;
    if(grid[x-1][y-1]!=1)
        return false;
    if(grid[x-1][y]!=1)
        return false;
    if(grid[x-1][y+1]!=1)
        return false;
    if(grid[x][y-1]!=1)
        return false;
    if(grid[x][y]!=1)
        return false;
    if(grid[x][y+1]!=1)
        return false;
    if(grid[x+1][y-1]!=1)
        return false;
    if(grid[x+1][y]!=1)
        return false;
    if(grid[x+1][y+1]!=1)
        return false;
    return true;
}


    
int main(){
    ios::sync_with_stdio(false);
    int t;
    cin>>t;
    for(int tc=1;tc<=t;++tc){
        memset(grid,0,sizeof grid);
        s={{499,500},{499,501},{501,500}, {501,501},{500,500},{500,501}};
        srand(time(NULL));
        int x;
        int y;
        int a;
        cin>>a;
        int tot=0;
        while(1){
            tot++;
            int el=0;
            int sz=s.size();
            if(sz!=0){
                el=rand()%sz;
            }
            int i=s[el].first;
            int j=s[el].second;
            //cerr<<"i "<<i<<" j "<<j<<endl;

            cout<<i<<" "<<j<<endl<<flush;
            //cerr<<i<<" "<<j<<endl;

            cin>>x>>y;
            //cerr<<"x "<<x<<" y "<<y<<endl;
            //cerr<<"------"<<endl;
            //cerr<<x<<" "<<y<<endl;
            if(x==0&&y==0)
                break;
            if(x==-1&&y==-1){
                cerr<<"Too slow"<<endl;
                break;
            }
            grid[x][y]=1;
            /*for(int nu = 498; nu < 503; ++nu){
                for(int nuu = 499; nuu < 503; ++nuu){
                    cerr<<grid[nu][nuu];
                }
                cerr<<endl;
            }*/

            for (vector<pair<int,int>>::iterator it = s.begin(); it != s.end(); ++it){
                if(isFilled(it)){
                    s.erase(it);
                    //cerr<<"size "<<s.size()<<endl;
                    break;
                    /*if(s.size()==0)
                        break;*/
                }
            }
            //cerr<<endl;
        //cerr<<"tot: "<<tot<<endl;
        }

    }
    return 0;
}