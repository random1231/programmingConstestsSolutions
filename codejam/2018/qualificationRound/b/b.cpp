#include <bits/stdc++.h>
using namespace std;
    
int main(){
    ios::sync_with_stdio(false);
    int t;
    cin>>t;
    for(int tc=1;tc<=t;++tc){
        int n;
        cin>>n;
        vector<int> v;
        while(n--)
        {
            int tmp;
            cin>>tmp;
            v.push_back(tmp);
        }

        bool done=false;
        while(!done){
            done=true;
            for(int i = 0; i < v.size()-2; ++i){
                if(v[i]>v[i+2]){
                    done=false;
                    int tmp=v[i];
                    v[i]=v[i+2];
                    v[i+2]=tmp;
                }
            }
        }

        if(is_sorted(v.begin(), v.end()))
            cout<<"Case #"<<tc<<": OK";
        else{
            int ps=0;
            for(int i = 0; i < v.size()-1; ++i){
                if(v[i]>v[i+1]){
                    ps=i;
                    break;
                }
            }
            cout<<"Case #"<<tc<<": "<<ps;
        }
    cout<<endl;
    }

    return 0;
}