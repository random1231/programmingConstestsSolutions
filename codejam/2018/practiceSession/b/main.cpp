#include <bits/stdc++.h>

using namespace std;

int main(){
    int t;
    cin>>t;
    for(int i = 0; i < t; ++i){
        int n;
        cin>>n;
        vector<pair<int,char>> v;
        for(int j = 0; j < n; ++j){
            int x;
            cin>>x;
            v.push_back({x,char('A'+j)});
        }

        cout<<"Case #"<<i+1<<": ";
        
        /*for(auto item : v){
            cout<<item.first<<" "<<item.second<< " ";
            cout<<endl;
        }*/

        while(!v.empty()){
            if(v.size()==2){
                for(int i = 0; i < v[0].first; ++i){
                    cout<<v[0].second<<v[1].second<< " ";
                }
                break;
            }
            sort(v.rbegin(), v.rend());
            v[0].first-=1;
            cout<<v[0].second<< " ";
            if(v[0].first==0)
                v.erase(v.begin());
        }

        cout<<endl;
    }
    
    return 0;
}