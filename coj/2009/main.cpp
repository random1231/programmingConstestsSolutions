#include <bits/stdc++.h>

using namespace std;

map<int, vector<int> > m;

void subsets(vector<int> &v){
    auto n=v.size();
    auto lim=(1<<n);
    for(int i=0;i<lim;++i){
        int cursum=0;
        for(int j=0;j<n;++j){
            //Test if jth bit is on
            if(i&(1<<j))
                cursum+=v[j];
        }
        // cout<<cursum<<endl;
        if(m.find(cursum)==m.end()){
            vector<int> ve{i};
            m[cursum]=ve;
        }else{
            m[cursum].push_back(i);
        }
    }
}


int main(int argc, char const *argv[]){
    int n;
    vector<int> v;
    cin>>n;while(n--){
        int a;
        cin>>a;
        v.push_back(a);
    }
    subsets(v);
    int ans=0;
    for(auto item : m){
        //cout<<item.first<<endl;
        int sz=item.second.size();
        if(sz>=2){
            cout<<sz<<endl;
            for(int i = 0; i < sz; ++i){
                for(int j = 0; j < sz; ++j){
                    if(i==j) continue;
                    int found=true;
                    for(int k=31;k>=0;--k){
                        if(item.second[i]&(1>>k)&&item.second[j]&(1>>k)){
                            found=false;
                            break;
                        }
                    }
                    if(found){
                        cout<<item.first<<"---"<<endl;
                        cout<<item.second[i]<< " "<<item.second[j]<<endl;
                        ++ans;
                    }
                }
            }
             cout<<endl;
        }
    }
    if(ans>0)
        cout<<ans<<endl;
    else cout<<0<<endl;
    return 0;
}