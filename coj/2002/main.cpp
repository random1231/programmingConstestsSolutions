#include <bits/stdc++.h>

using namespace std;

int n;
int a[1000010];
set<int> s;


int solve(){
	int longest=0;
	
    for(auto x: s){
		int pre=-1;
		int streak=0;
        for (int i = 0; i < n; i++){
            if(a[i]!=x){
                if(a[i]==pre) ++streak;
                else streak=1;                
                pre=a[i];
            }

            longest=max(longest,streak);
        }
    }
	return longest;
}


int main(){
    cin>>n;
    for(int i=0;i<n;++i){
        cin>>a[i];
        s.insert(a[i]);
    }
    
    if(s.size()==1) cout<<n<<endl;
	else cout<<solve()<<endl;
    return 0;
}
