#include <iostream>

using namespace std;

long long hack(long long n)
{
   if(n == 3)
       return 3;
   if(n == 2)
       return 2;
   if(n == 1)
       return 1;
   return hack(n-1)+2*hack(n-2)+3*hack(n-3);
}

int main()
{
    for(int i = 1; i <= 50; i++)
     {
        auto h = hack(i);
        if(h%2!=0)
            cout << i << " " << h << endl;
     }
    return 0;
}
