from math import sqrt
n = int(input())
l = list(map(int, input().split(' ')))
lim = int(sqrt(l[n-1]))
ans = 0
for x in range(2, lim+1):
    count = 0
    for y in l:
        if y%x != 0:
            count += 1
        if count > 1:
            break
    if count == 1:
        ans = x
        break
print(ans)
