#include <bits/stdc++.h>

using namespace std;

int main()
{
    multimap<int, int> m;
    int n, k, ans = 0;
    cin >> n >> k;
    for(int i = 0; i < n; ++i) 
    {
        int x;
        cin >> x;
        auto num = x % k;
        m.insert(make_pair(num, i));
    }

    for(auto x : m) {
        auto ran = 3-x.first;
        if(ran == 3)
            ran = 0;
        auto er = m.equal_range(ran);
        for(auto it = er.first; it != er.second; ++it) {
              if(it->second > x.second)
                if(it->first+x.first == 3 || it->first+x.first == 0)
                    ans++;
        }
    }
    
    cout << ans;

    return 0;
}
