recurse x n i ans | i == 0 = ans 
                  | x-i^n == 0 = ans+1
                  | x-i^n > 0 = recurse  (x-i^n) n (i-1) ans 
                  | x-i^n < 0 = recurse  x n (i-1) ans 

recurse2 x n i ans = sum[recurse x n l ans | l <- reverse[1..i]]

--theSumOfPowers::[Int]->Int->Int
theSumOfPowers x n = sum[recurse2 x n l 0 | l <- reverse[1..(sqrt x)] ] 
