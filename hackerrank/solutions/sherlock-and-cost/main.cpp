#include <bits/stdc++.h>

using namespace std;


int a[100005];
int dp[100005][105];
int n;

int solve(int pos,int x){
    if(pos==n)return 0;
    auto &ref=dp[pos][x];
    if(ref!=-1)return ref;
    int ans=INT_MIN;
    ans=max(ans,solve(pos+1,1)+(pos>0 ? abs(x-1):0));
    ans=max(ans,solve(pos+1,a[pos])+(pos>0 ? abs(x-a[pos]):0));
    return ref=ans;
}

int main(){
    int t;cin>>t;while(t--){
        memset(dp,-1,sizeof dp);
        cin>>n;
        for(int i = 0; i < n; ++i){
            cin>>a[i];
        }
        cout<<solve(0,0)<<endl;
    }
    return 0;
}