from math import sqrt
def quadEc(a, b, c):
    disc = b*b-4*a*c
    if disc < 0:
        return None #complex numbers
    sq = sqrt(disc)
#    if sq*sq != disc:
#        return [-1, -1] #real numbers
    root1 =0 
    root2 =0
    num1 = sq - b
#    if num1 > 0 and num1 % 2 == 0:
    root1 = num1/(2*a)
    num2 = -sq - b 
#    if num2 > 0 and num2 % 2 == 0:
    root2 = num2/(2*a)
    return [num1, num2, 2*a]
print('Quadratic equation solver of the type ax^2+bx+c')
print('Enter a, b, c in a line separated by 1 space')
a, b, c = list(map(int, input().split()))
res = quadEc(a, b, c)
if res  == None:
    print('The equation has no real solutions')
else:
    x, y, den = res
    print(str(a)+'x^2+'+str(b)+'x+'+str(c)+' = ('+str(x)+'/'+str(den)+')('+str(y)+'/'+str(den)+')')
