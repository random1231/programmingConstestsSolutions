from math import sqrt
n = int(input())
lim = int(sqrt(n))
ans = 1
curSum = 1
for i in range(1, lim+1):
    if n % i == 0:
        j = n//i
        x = sum(list(map(int, list(str(i)))))
        y = sum(list(map(int, list(str(j)))))
        if x == y:
            tempAns = i if i < j else j
            tempSum = x if x > y else y
        else:
            tempAns = i if x > y else j
            tempSum = x if x > y else y
        print(tempAns, tempSum)
        if tempSum > curSum:
            ans = tempAns
            curSum = tempSum
        elif tempSum == curSum:
            if ans > tempAns:
                ans = tempAns

print(ans)
