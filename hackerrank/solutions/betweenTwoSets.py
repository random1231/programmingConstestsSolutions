input()
l1 = list(map(int, input().split(' ')))
l2 = list(map(int, input().split(' ')))
mi = min(l2)
ma = max(l1)
ans = 0
for i in range(ma, mi+1, ma):
    if all(i%x == 0 for x in l1) and all(x%i == 0 for x in l2):
        ans +=1
print(ans)
