#include <bits/stdc++.h>

using namespace std;

int gcd(int x, int y)
{
    if(y == 0)
        return x;
    if(x > y)
        return gcd(y, x%y);
    return gcd(x, y%x);
}

int main()
{
    int T;
    cin >> T;
    while(T--)
    {
       int n; 
        vector<int> v, ans;
        cin >> n;
       for(int i = 0; i < n; ++i) 
       {
           int x;
           cin >> x;
           v.push_back(x);
       }

       ans.push_back(v[0]);
       for(int i = 0; i < n-1; ++i) 
       {
           int g = gcd(v[i], v[i+1]);
           ans.push_back((v[i]*v[i+1])/g);
       }

       ans.push_back(v[n-1]);

       
       for(auto x : ans)
           cout << x << " ";

       cout << endl;
    }
}
