//The grid only has three states, the trivial case when n is even in which all the elements of the grid are O
//If we change the state of the grid and then we change the state again we get the original grid thus
//we only need to change the state of the grid at most one
//So the states are: original, trivial, changed
//If n = 1 -> Original
//if n = 3 -> Changed
//if n = 5 -> Original
//if n = 7 -> Changed
//See the pattern?
//1 is 1st odd number, 3 is the 2nd odd number
//5 is 3rd odd number, 7 is the 4th odd number
//See the pattern?
//if n is odd, n is the (n+1)/2 odd number
//so we must check if this number is odd or even
//if it's even we change the state, otherwise just print the original grid
//
//Bomberman rules :) btw
//
//att: ThrashansEXE
//---------------------------------------------awesome code---------------------------------------------------------
#include <bits/stdc++.h>
#define vvc vector<vector<char>> 

using namespace std;

void detonate(vvc &v, int r, int c, int i, int j)
{
    if(i-1 >= 0)
        v[i-1][j] = '.';
    if(j-1 >= 0)
        v[i][j-1] = '.';
    if(i+1 < r)
        if(v[i+1][j] != 'O')
            v[i+1][j] = '?';
    if(j+1 < c)
        if(v[i][j+1] != 'O')
            v[i][j+1] = '?';
    v[i][j] = '.';
}

bool emptyGrid(vvc &v, int r, int c)
{
    int ans = 0;
    for(int i = 0; i < r; ++i)
        for(int j = 0; j < c; ++j)
            if(v[i][j] == 'O')
               if(i-1 >= 0 || j-1 >= 0 || i+1 < r || j+1 < c)
                   ans++;
    return ans >= r*c;
}

void changeState(vvc &v, int r, int c)
{
    for(int i = 0; i < r; ++i)
    {
        for(int j = 0; j < c; ++j)
        {
            if(v[i][j] == '.')
                v[i][j] = 'O';
            else if(v[i][j] == 'O')
                detonate(v, r, c, i, j);
            else if(v[i][j] == '?')
                v[i][j] = '.';
        }
    }
}

void printFullBombs(vvc &v, int r, int c)
{
   for(int i = 0; i < r; ++i)
   {
       for(int j = 0; j < c; ++j)
           cout << "O";
       cout << endl;
   }
}

int main()
{
    int r, c, n;
    cin >> r >> c >> n;
    vvc v(r);

    for(int i = 0; i < r; ++i)
    {
        for(int j = 0; j < c; ++j)
        {
             char x;
             cin >> x;
             v[i].push_back(x);
        }
    }
    if (n%2 == 0 || emptyGrid(v, r, c))
    {
        printFullBombs(v, r, c);
        return 0;
    }
    if(n > 1) //A stupid optimization
    {
        if(((n+1)/2)%2 == 0) //An awesome optimization :)
            changeState(v, r, c);
    }

    for(int i = 0; i < r; ++i)
    {
        for(int j = 0; j < c; ++j)
             cout << v[i][j];
        cout << endl;
    }
    return 0; 
}
