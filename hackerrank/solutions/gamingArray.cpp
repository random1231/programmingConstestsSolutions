#include <bits/stdc++.h>

using namespace std;

int solve(vector<int> v)
{
    int ans = 0;
    auto it = v.end();
    while(it != v.begin())
    {
       it = max_element(v.begin(), --it);
       ans++;
    }

    return ans;

}

int main()
{
    int g;
    cin >> g;

    while(g--)
    {
        int n;
        cin >> n;
        vector<int> v;
        for(int i = 0; i < n; ++i)
        {
            int x;
            cin >> x;
            v.push_back(x);
        }

        cout << (solve(v)%2 == 0 ? "ANDY" : "BOB") << endl;
    }
    return 0;
}
