num::[Integer]->Integer
num [] = 1
num (x:ls) = ((x `mod`(modulo))*((num ls) `mod` (modulo))) `mod` (modulo)
             where
                 modulo = 10^9+7
sol::[Integer]->[Integer]->Integer
sol x y = (gcd (num x) (num y))`mod`(10^9+7)


main = do
        getLine
        l1 <- getLine
        let ints1 = map read (words l1) :: [Integer]
        getLine
        l2 <- getLine
        let ints2 = map read (words l2) :: [Integer]
        print (sol (ints1) (ints2))
        let ans = (gcd (product(ints1)) (product(ints2)))`mod`(10^9+7)
        print(ans)
