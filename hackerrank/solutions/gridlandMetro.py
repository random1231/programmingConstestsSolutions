def solve(l, n, m):
    inter = intersection(l)
    ans = 0
    for x in l:
        ans+=(x[2]-x[1]+1)
    return n*m-(ans-inter)

def intersection(l):
    incExc = 0
    for i in range(len(l)+1):
        for j in range(i+1, len(l)):
            if l[i][0] == l[j][0]:
                if l[i][1] < l[j][1]:
                    a = l[i] 
                    b = l[j] 
                elif l[i][1] == l[j][1]:
                    if l[i][2] < l[j][2]:
                        a = l[i] 
                        b = l[j] 
                    else:
                        a = l[j] 
                        b = l[i]
                else:
                    a = l[j] 
                    b = l[i]
                if b[1] >= a[1] and b[1] <= a[2]:
                    incExc+=(b[2]-b[1]+1)
    return incExc
                


n, m, k = list(map(int, input().split()))
l = []
for _ in range(k):
    r, c1, c2 = list(map(int, input().split()))
    l.append((r, c1, c2))
print(solve(l, n, m))
