from math import sqrt
def quadEc(b, c):
    disc = b*b-4*c
    if disc < 0:
        return [-1, -1] #complex numbers
    sq = int(sqrt(disc))
    if sq*sq != disc:
        return [-1, -1] #real numbers
    root1 = -1
    root2 = -1
    num1 = sq - b
    if num1 > 0 and num1 % 2 == 0:
        root1 = num1//2
    num2 = -sq - b 
    if num2 > 0 and num2 % 2 == 0:
        root2 = num2//2
    return [root1, root2]

#for _ in range(int(input())):
#    i = input()
    #for x in range(1, len(i)+1):
    #    print(i[x:]+i[:x])

print(quadEc(-45, 324))
