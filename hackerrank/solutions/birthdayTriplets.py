from math import sqrt
def quadEc(b, c):
    disc = b*b-4*c
    if disc < 0:
        return [-1, -1] #complex numbers
    sq = int(sqrt(disc))
    if sq*sq != disc:
        return [-1, -1] #real numbers
    root1 = -1
    root2 = -1
    num1 = sq - b
    if num1 > 0 and num1 % 2 == 0:
        root1 = num1//2
    num2 = -sq - b 
    if num2 > 0 and num2 % 2 == 0:
        root2 = num2//2
    return [root1, root2]

def isPerfectSquare(x):
    res = int(sqrt(x)) 
    return res*res == x

def isPerfectCube(x):
    res = int(x**(1/3)) 
    return res*res*res == x

def triplet(x,y, z):
    lim = 10**15
    rs = z - x*x
    #if rs % 2 != 0:
    #    return []
    rs /= 2
    for a in range(1, lim+1):
        ans1 = -1
        ans2 = -1
        a1 = False
        a2 = False 
        b = a**2-x
        c = int(-x*(a**2)+a**4-rs)
        r1, r2 = quadEc(b, c)
        if r1 > 0 and isPerfectSquare(r1) == True:
            ans1 = int(sqrt(r1))
            if isPerfectCube(y-(a**3+r1*int(ans1))) and ans1 != a:
                tripB = int(x**(1/3))
                if tripB != ans1:
                    a1 = True
        if r2 > 0 and isPerfectSquare(r2) == True and ans2 != a:
            ans2 = int(sqrt(r2))
            if isPerfectCube(y-(a**3+r2*int(ans2))):
                tripB = int(x**(1/3))
                if tripB != ans2:
                    a2 = True
        if a1 == True:
            if a2 == True:
                return [a, tripB, min(ans1, ans2)]
            else:
                return [a, tripB, ans1]
        elif a2 == True:
            return [a, tripB, ans2]


mod = 10**9+7
for _ in range(int(input())):
    f2, f3, f4, l, r = list(map(int, input().split(' ' )))
    a, b, c = triplet(f2, f3, f4)
    ans = 0
    for x  in range(l, r+1):
        ans += (((pow(a, x, mod)+pow(b, x, mod))%mod)+pow(c, x, mod))%mod
    print(ans)
