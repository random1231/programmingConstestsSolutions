myGcd x y | x == y    = x
          | otherwise = gcd y (x`mod`y)
