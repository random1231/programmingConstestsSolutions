#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n;
    cin >> n;
    long long ans = 1;
    while(n--)
    {
        long long x;
        cin >> x;
        ans = x*ans/__gcd(x, ans);
    }
    cout << ans;
    
    return 0;
}

