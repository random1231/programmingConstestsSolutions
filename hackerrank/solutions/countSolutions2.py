#x^2+y^2=ax+by
#x(x-a)+y(y-b) = 0
#if we assing different values to x
#we will get the answer in O(c)
#let z = x(x-a) remember that this expression
#would yield an integer once we assign x a value
#we get y^2-by+z=0
#we solve this quadratic equation
#and check if the roots are integers
from math import sqrt
def quadEc(b, c):
    disc = b*b-4*c
    if disc < 0:
        return [-1, -1] #complex numbers
    sq = int(sqrt(disc))
    if sq*sq != disc:
        return [-1, -1] #real numbers
    root1 = -1
    root2 = -1
    num1 = sq - b
    if num1 > 0 and num1 % 2 == 0:
        root1 = num1//2
    num2 = -sq - b 
    if num2 > 0 and num2 % 2 == 0:
        root2 = num2//2
    return [root1, root2]

for _ in range(int(input())):
    a, b, c, d = list(map(int, input().split(' ')))
    ans = 0
    for x in range(1, c+1):
        val = x*(x-a)
        y1, y2 = quadEc(-b, val)
        if y1 > 0 and y1 <= d:
            ans +=1
        if y2 > 0 and y2 <= d and y2 != y1:
            ans +=1
    print(ans)
