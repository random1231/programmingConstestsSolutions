#include <bits/stdc++.h>

using namespace std;

int main()
{
    map<int, int> m;
    int n, k, ans1 = 0, ans2 = 0;
    cin >> n >> k;
    for(int i = 0; i < n; ++i) 
    {
        int x;
        cin >> x;
        auto num = x % k;
        if(m.count(num) == 0)
            m[num] = 1;
        else
            m[num]++;
    }

    bool z = false;
    for(auto x : m)
    {
        auto ran = k-x.first;
        if(ran == k)
            z = true;
        else
        {
            auto val = m.find(ran);
            if(val != m.end())
            {
                ans1 += x.second;
                ans2 += val->second;
                cout << x.first << " " << x.second << endl;
                m.erase(val);
            }
        }
    }

    if(ans1 > ans2)
        cout << ans1 + (int)z;
    else
        cout << ans2 + (int)z;
}
