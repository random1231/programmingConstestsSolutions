#include <bits/stdc++.h>

using namespace std;

const int n=20;
int grid[n][n];

bool productUpperDiagonal(int x, int y, unsigned long long &res){
	if(x<3||y>n-4)
		return false;
	int cnt=0;
	for(unsigned long long row=x, col=y;;++col,--row){
		//cout<< grid[x][col]<< " ";
		res*=grid[row][col];
		++cnt;
		if(cnt==4)
			break;
	}
	//cout<<res<<endl;
	return true;
}

bool productLowerDiagonal(int x, int y, unsigned long long &res){
	if(x>n-4||y>n-4)
		return false;
	int cnt=0;
	for(unsigned long long row=x, col=y;;++col,++row){
		//cout<< grid[x][col]<< " ";
		res*=grid[row][col];
		++cnt;
		if(cnt==4)
			break;
	}
	//cout<<res<<endl;
	return true;
}

bool productUp(int x, int y, unsigned long long &res){
	if(x<3)
		return false;
	int cnt=0;
	for(unsigned long long row=x;;++row){
		//cout<< grid[x][col]<< " ";
		res*=grid[row][y];
		++cnt;
		if(cnt==4)
			break;
	}
	//cout<<res<<endl;
	return true;
}

bool productRight(int x, int y, unsigned long long &res){
	if(y>n-4)
		return false;
	int cnt=0;
	for(unsigned long long col=y;;++col){
		//cout<< grid[x][col]<< " ";
		res*=grid[x][col];
		++cnt;
		if(cnt==4)
			break;
	}
	//cout<<res<<endl;
	return true;
}

int main(){
	unsigned long long rightAns=0;
	unsigned long long rightUp=0;
	unsigned long long rightUppD=0;
	unsigned long long rightLowD=0;
	unsigned long long answer;
    for(int grid_i = 0;grid_i < n;grid_i++){
       for(int grid_j = 0;grid_j < n;grid_j++){
          cin >> grid[grid_i][grid_j];
       }
    }

    for(int grid_i = 0;grid_i < n;grid_i++){
       for(int grid_j = 0;grid_j < n;grid_j++){
	       	unsigned long long res[4];
	       	res[0]=1;
	       	res[1]=1;
	       	res[2]=1;
	       	res[3]=1;
	       	if(productRight(grid_i, grid_j, res[0]))
	       		if(res[0]>rightAns)
       				rightAns=res[0];
       		if(productUp(grid_i, grid_j, res[1]))
	       		if(res[1]>rightUp)
       				rightUp=res[1];
       		if(productUpperDiagonal(grid_i, grid_j, res[2]))
	       		if(res[2]>rightUppD)
       				rightUppD=res[2];
       		if(productLowerDiagonal(grid_i, grid_j, res[3]))
	       		if(res[3]>rightLowD)
       				rightLowD=res[3];
       		//cout<<res[0]<<" "<<res[1]<<" "<<res[2]<<" "<<res[3];
       		auto curAnswer=max({res[0],res[1],res[2],res[3]});
       		if(curAnswer>answer)
       			answer=curAnswer;
       }
   }

   cout<<answer;

    return 0;
}
