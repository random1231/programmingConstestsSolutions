#!/bin/python3

import sys


t = int(input().strip())
for a0 in range(t):
	n,k = input().strip().split(' ')
	n,k = [int(n),int(k)]
	num = input().strip()
	mx=0
	for i in range(n-k+1):
		res=1
		for j in range(i,i+k,1):
			res*=int(num[j])
		if res>mx:
			mx=res
	print(mx)