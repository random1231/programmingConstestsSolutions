#include <bits/stdc++.h>
using namespace std;

int main(){
    int n,m,x;
    cin>>n>>m>>x;
    vector<int> v;
    for(int i = 0; i < m; ++i){
        int tmp;
        cin>>tmp;
        v.push_back(tmp);
    }

    int ans1=0;
    for(auto val : v){
        if(val<x)
            ++ans1;
        else
            break;
    }
    int ans2=0;
    reverse(v.begin(), v.end());
    for(auto val : v) {
        if(val>x)
            ++ans2;
        else
            break;
    }

    cout<<min(ans1,ans2)<<endl;



    return 0;
}