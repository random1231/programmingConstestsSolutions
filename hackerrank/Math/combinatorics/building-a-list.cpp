#include <bits/stdc++.h>

using namespace std;

set<string> subsets(string &v){
    auto n=v.size();
    auto lim=(1<<n);
    set<string> ss;
    for(int i=1;i<lim;++i){
        string ans;
        for(int j=0;j<n;++j){
            //Test if jth bit is on
            if(i&(1<<j))
                ans+=v[j];
        }
        cerr<<"ans: "<<ans<<endl;
        ss.insert(ans);
    }
    return ss;
}

int main(int argc, char const *argv[])
{
    int t;cin>>t;while(t--){
        int n;cin>>n;
        string s;cin>>s;
        cerr<<"s: "<<s<<endl;
        for(auto x : subsets(s)){
            cout<<x<<endl;
        }
    }
    return 0;
}