#include <bits/stdc++.h>

using namespace std;
//Just set i=2 if you don't want to include 1 and n
vector<int> divisors(long long n) {
    long long lim = sqrt(n);
    vector<int> v;
    for(long long i = 2; i <= lim; ++i){
        if(n%i==0){
            v.push_back(i);
            if(i!=n/i) //To avoid adding a number twice in case that n is a perfect square
                v.push_back(n/i);
        }
    }
    return v;
}

bool isPS(int x){
    int ans=sqrt(x);
    return ans*ans==x;
}

int main(){
    int t;
    cin>> t;
    while(t--){
        int n;
        cin>>n;
        auto v=divisors(n);
        int sz=v.size()+1;
        int ans=0;
        for(auto x : v){
            if(x%2==0)
                if(isPS(x)) ++ans;
        }
        if(ans==0)
            cout<<"0";
        else
            cout<<ans/__gcd(ans,sz)<<"/"<<sz/__gcd(ans,sz);
        cout<<endl;

    }
    return 0;
}