#include <bits/stdc++.h>

using namespace std;

long long x, y, d;
//Extended euclid returns a triple that satisfies the equation
//ax+by=d where d is gcd(a, b)
void extendedEuclid(long long a, long long b){
    if(b == 0){
        x = 1;
        y = 0;
        d = a;
        return;
    }
    extendedEuclid(b, a%b);
    int x1 = y;
    int y1 = x-(a/b)*y;
    x = x1;
    y = y1;

}

//Sometimes modInv returns negative residues
void negToPos(long long &x, long long mod){
    while(x<0)
        x+=mod;
}

//Computes the modular multiplicative inverse of a modulo m
//ax=1(modm)
bool modInv(long long a, long long m, long long &ans){
    extendedEuclid(a, m);
    if(d != 1) 
        return false;
    ans = x;
    if(x < 0)
        negToPos(ans, m);
    return true;
}


long long multmod(long long a, long long b, long long c){
    long long x = 0,y=a%c;
    while(b > 0){
        if(b%2 == 1){
            x = (x+y)%c;
        }
        y = (y*2)%c;
        b /= 2;
    }
    return x%c;
}

long long expmod(long long a, long long b, long long c){
    long long res = 1;
    long long x = a%c;

    while(b > 0){
        if(b%2==1)
            res = multmod(res, x, c);
        x=multmod(x, x, c);
        b/=2;
    }
    return res;
}



int main(){
    int t;
    cin>>t;
    while(t--){
        long long a,b,x;
        cin>>a>>b>>x;
        if(b<0){
            long long ans;
            modInv(a,x,ans);
            b*=-1;
            cout<<expmod(ans%x,b,x);

        }else{
            cout<<expmod(a,b,x);

        }
        cout<<endl;
    }
    return 0;
}