#include <bits/stdc++.h>

using namespace std;


int main(){
    ios::sync_with_stdio(false);
    int t;
    cin>> t;
    map<pair<long long,long long>, long long> m;
    while(t--){
        long long n,k;
        cin>>n>>k;
        pair<long long,long long> p=make_pair(n,k);
        if(m.find(p)!=m.end()){
            cout<<m[p]<<endl;
            continue;
        }      

        if(k==1){
            cout<<(long long)(n*n+1)/2<<endl;
            return 0;
        }
        bool fl=false;
        if(k%2==0)
            fl=true;
        long long last=((long long)((2*n-1)/k))*k;
        long long ans=0;
        for(long long i = k; i <=last; i+=k){
            long long nm=(i-1);
            if(fl)
                --nm;
            nm/=2;
            if(i>n)
                nm-=(i-n-1);
            ans+=nm;
        }
        m[p]=ans;
        cout<<ans<<endl;

    }
    return 0;
}-