#include <bits/stdc++.h>

using namespace std;

long long sieve_size;
vector<int> primes;
bitset<1000000009> bs;

long long multmod(long long a, long long b, long long c){
    long long x = 0,y=a%c;
    while(b > 0){
        if(b%2 == 1){
            x = (x+y)%c;
        }
        y = (y*2)%c;
        b /= 2;
    }
    return x%c;
}

long long exp(long long a, long long b, long long c){
	long long res = 1;
	long long x = a%c;

	while(b > 0){
		if(b%2==1)
			res = multmod(res, x, c);
		x=multmod(x, x, c);
		b/=2;
	}
	return res;
}

void sieve(long long upperbound){
    sieve_size = upperbound+1;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]){
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    	primes.push_back((int)i);
	    }
}

vector<int> differentPrimeFact(long long n) {
	int lim = int(sqrt(n));

	sieve(lim+100); //For safety
	long long index = 0, primeF = primes[index], ans = 0;
	vector<int> factors;
	while(primeF <= lim){
		if(n%primeF == 0){
			while(n%primeF == 0){
				n /= primeF;
			}
			factors.push_back(primeF);
		}
		primeF = primes[++index];
	}

	if (n != 1)
		factors.push_back(n);
	return factors;
}

int eulerPhi(long long n){
	long long lim = sqrt(n);
	long long ans = n;
	int index = 0;
	int pr = primes[index];

    while(pr*pr <= n){
		if(n%pr==0)
			ans -= (ans/pr);
		while(n%pr == 0)
			n /= pr;
		pr = primes[++index];
	}
	if (n > 1)
		ans-=(ans/n);
	return ans;
}

void solve(long long p, int &minRoot, int &totalRoots){
	auto x = differentPrimeFact(p-1);
	int i = 2;
	bool found;
	while(1){
		for(auto n : x){
			found = true;
			long long num = (p-1)/n;
			if(exp(i, num, p) == 1){
				found = false;
				break;
			}
		}
		if(found){
			minRoot = i;
			break;
		}
		++i;
	}
	totalRoots = eulerPhi(p-1);
}

int main(){
	ios::sync_with_stdio(false);
	int p=260119679;
	//cin >> p;
	int ans1, ans2;
	solve(p, ans1, ans2);
	cout << ans1 << " " << ans2;
}
