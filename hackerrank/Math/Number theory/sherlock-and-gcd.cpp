#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[])
{
	int t;
	cin >> t;

	while(t--){
		set<int> s;
		int n;
		cin >> n;
		bool corner = false;
		for(int i = 0; i < n; ++i){
			int x;
			cin >> x;
			if(x == 1){
				corner = true;
				break;
			}
			s.insert(x);
		}
		if(corner){
			cout << "YES" <<endl;
			break;			
		}
		auto it = s.begin();
		bool found = false;
		auto ans = *it;
		++it;
		for(; it != s.end(); ++it){
			ans = __gcd(ans, *it);
			if(ans == 1){
				found = true;
				break;
			}
		}
		if(found)
			cout << "YES" <<endl;
		else
			cout << "NO" <<endl;
	}
	return 0;
}
