#include <bits/stdc++.h>

using namespace std;

long long lcm(std::vector<long long> v, int n){
	long long ans = v[0];
	for(int i = 1; i < n; ++i){
		ans = (ans*v[i])/__gcd(ans, v[i]);
	}
	return ans;
}

long long gcd(std::vector<long long> v, int n){
	long long ans = v[0];
	for(int i = 1; i < n; ++i){
		ans = __gcd(ans, v[i]);
	}
	return ans;
}
int main(int argc, char const *argv[])
{
	int n, m;
	cin >> n >> m;

	std::vector<long long> a, b;
	for(int i = 0; i < n; ++i){
		long long x;
		cin >> x;
		a.push_back(x);
	}
	for(int i = 0; i < m; ++i){
		long long x;
		cin >> x;
		b.push_back(x);
	}

	auto l = lcm(a, n);
	auto g = gcd(b, m);

	int ans = 0;
	auto tmp = l;
	while(l <= g/2){
		if(g%l == 0)
			ans++;
		l+=tmp;
	}

	if(g%tmp==0) ans++;
	cout << ans;

	return 0;
}