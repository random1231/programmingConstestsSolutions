include <bits/stdc++.h>

using namespace std;

int gcd(vector<int> v, int n){
	int ans = v[0];
	for(int i = 1; i < n; ++i){
		ans = __gcd(ans, v[i]);
	}
	return ans;
}

long long multmod(long long a, long long b, long long c){
    long long x = 0,y=a%c;
    while(b > 0){
        if(b%2 == 1){
            x = (x+y)%c;
        }
        y = (y*2)%c;
        b /= 2;
    }
    return x%c;
}

long long exp(long long a, long long b, long long c){
    long long res = 1;
    long long x = a%c;

    while(b > 0){
        if(b%2==1)
            res = multmod(res, x, c);
        x=multmod(x, x, c);
        b/=2;
    }
    return res;
}

bool Miller(long long p,int iteration){
    if(p<2){
        return false;
    }
    if(p!=2 && p%2==0){
        return false;
    }
    long long s=p-1;
    while(s%2==0){
        s/=2;
    }
    for(int i=0;i<iteration;i++){
        long long a=rand()%(p-1)+1,temp=s;
        long long mod=exp(a,temp,p);
        while(temp!=p-1 && mod!=1 && mod!=p-1){
            mod=multmod(mod,mod,p);
            temp *= 2;
        }
        if(mod!=p-1 && temp%2==0){
            return false;
        }
    }
    return true;
}


int main(int argc, char const *argv[])
{
	long long n, k;
	ofstream f("easy-gcd.out");
	ifstream inf("easy-gcd.in");
	inf >> n >> k;
    ios::sync_with_stdio(1);
	vector<int> v;
	for(int i = 0; i < n; ++i){
		int x;
		inf >> x;
		v.push_back(x);
	}

	long long g = gcd(v, n);
    f << g << endl;
    if(g > k){

    	if(Miller(g, 15)){
    		f << "0" << endl;
    		f << "Miller";
    		return 0;
    	}

        for(auto x = k; x>1; x--)
            if(__gcd(x, g)!=1){
            	f << "auto";
                f << x << endl;
                return 0;
            }
    }
	f << g*(k/g) << endl;
	f << "Normal";

	return 0;
}