#include <bits/stdc++.h>

using namespace std;

long long pre[100010];
long long suf[100010];
vector<long long> v;

int main(){
    int n;
    cin >> n;
    for(int i = 0; i < n; ++i){
        long long x;
        cin >> x;
        v.push_back(x);
    }
    if(n == 1)
        cout << v[0]+1 << endl;
    pre[0] = v[0];
    for(int i = 1; i <= n; ++i){
        pre[i] = __gcd(v[i-1], pre[i-1]);
    }

    suf[n] = v[n-1];

    for(int i = n-1; i >= 0; --i){
        suf[i] = __gcd(v[i], suf[i+1]);
    }
    suf[n] = 0;
    for(int i = 0; i < n; ++i){
        auto g = __gcd((i == 0 ? 0 : pre[i-1]), suf[i+1]);
        if(v[i]%g!=0){
            cout << g << endl;
            break;
        }
    }
    return 0;
}