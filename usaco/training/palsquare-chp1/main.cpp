/*
ID: ThrashansEXE
TASK: palsquare
LANG: C++
*/

#include <bits/stdc++.h>

using namespace std;

map<int, char> m;

void init(){
    m[10]='A';
    m[11]='B';
    m[12]='C';
    m[13]='D';
    m[14]='E';
    m[15]='F';
    m[16]='G';
    m[17]='H';
    m[18]='I';
    m[19]='J';
}

string convert(int n, int b){
    string res="";
    while(n>0){
        int tmp=n%b;
        char c;
        if(tmp<10)
            c='0'+tmp;
        else
            c=m[tmp];
        //cerr<<n<<" "<<c<<endl;
        res.insert(res.begin(),c);
        n/=b;
    }

    return res;
}

bool pal(string s){
    for(int i = 0; i < s.size()/2; ++i){
        if(s[i]!=s[s.size()-1-i])
            return false;
    }
    return true;
}

int main(){
    ofstream fout ("palsquare.out");
    ifstream fin ("palsquare.in");
    init();
    int n;
    fin>>n;
    for(int i = 1; i < 301; ++i){
        string s1=convert(i,n);
        string s2=convert(i*i,n);
        if(pal(s2))
            fout<<s1 << " "<<s2<<endl;
    }
    return 0;
}