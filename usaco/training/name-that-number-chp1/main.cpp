/*
ID: ThrashansEXE
TASK: namenum
LANG: C++
*/

#include <bits/stdc++.h> 

using namespace std;

ofstream fout ("namenum.out");
ifstream dict ("dict.txt");

string a[]={"","","ABC","DEF","GHI","JKL","MNO","PRS","TUV","WXY"};
int v[13];
int sz;
set<string> ss;
int ans=0;

void solve(int i,string res){
    if(i>=sz){
        if(ss.find(res)!=ss.end()){
            ans++;
            fout<<res<<endl;
        }
        return;
    }
    solve(i+1,res+a[v[i]][0]);
    solve(i+1,res+a[v[i]][1]);
    solve(i+1,res+a[v[i]][2]);
}

int main(){
    string tmp;
    while(dict>>tmp)
        ss.insert(tmp);
    ifstream fin ("namenum.in");
    string s;
    fin>>s;
    sz=s.size();
    for(int i = 0; i < sz; ++i){
        v[i]=s[i]-'0';
    }

    solve(0,"");
    if(ans==0)
        fout<<"NONE"<<endl;

    return 0;
}