/*
ID: ThrashansEXE
TASK: combo
LANG: C++
*/

#include <bits/stdc++.h> 

using namespace std;

bool comb[100][100][100];

int n;

int md(int x){
    int res=x%n;
    if(res<0)
        return n+res;
    return res;
}

int main(){
    ofstream fout ("combo.out");
    ifstream fin ("combo.in");
    int a[4];
    int b[4];
    fin>>n;
    fin>>a[0]>>a[1]>>a[2];
    fin>>b[0]>>b[1]>>b[2];

    int x=a[0];
    int y=a[1];
    int z=a[2];

    int ans=0;

    for(int i = -2; i < 3; ++i)
        for(int j = -2; j < 3; ++j)
            for(int k = -2; k < 3; ++k)
                if(!comb[md(x+i)][md(y+j)][md(z+k)]){
                    comb[md(x+i)][md(y+j)][md(z+k)]=true;
                    ++ans;
                }

    x=b[0];
    y=b[1];
    z=b[2];
    for(int i = -2; i < 3; ++i){
        for(int j = -2; j < 3; ++j){
            for(int k = -2; k < 3; ++k){
                if(!comb[md(x+i)][md(y+j)][md(z+k)]){
                    comb[md(x+i)][md(y+j)][md(z+k)]=true;
                    ++ans;
                }
            }
        }
    }

    fout<<ans<<endl;
    return 0;
}