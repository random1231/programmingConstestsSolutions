/*
ID: ThrashansEXE
TASK: crypt1
LANG: C++
*/

#include <bits/stdc++.h> 

using namespace std;

int v[10];

bool valid(int n,int cnt){
    while(n>0){
        if(!v[n%10])
            return false;
        --cnt;
        if(cnt<0)
            return false;
        n/=10;
    }

    return true;
}

bool check(int i, int j){
    if(!valid(i*j,4)||!valid(i,3)||!valid(j,2))
        return false;
    while(j>0){
        if(!valid(i*(j%10),3))
            return false;
        j/=10;
    }
    return true;
}


int main(){
    ofstream fout ("crypt1.out");
    ifstream fin ("crypt1.in");

    int n;
    fin>>n;
    while(n--){
        int a;
        fin>>a;
        v[a]=1;
    }
    int ans=0;
    for(int i = 100; i < 1000; ++i){
        for(int j = 10; j < 100; ++j){
            if(check(i,j)){
                ++ans;
            }
        }
    }

    fout<<ans;
    fout<<endl;

    return 0;
}