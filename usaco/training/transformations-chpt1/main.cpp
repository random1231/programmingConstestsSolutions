/*
ID: ThrashansEXE
TASK: transform
LANG: C++
*/

#include <bits/stdc++.h>

using namespace std;
char a[10][10];
char b[10][10];
char tmp[10][10];
int n;

bool eq(){
    for(int i = 0; i < n; ++i)
        for(int j = 0; j < n; ++j)
            if(tmp[i][j]!=b[i][j])
                return false;
    return true;
}

void res(){
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            tmp[i][j]=a[i][j];
        }
    }
}

void rot(){
    char aux[10][10];
    for(int i = 0; i < n; ++i)
        for(int j = 0; j < n; ++j)
            aux[i][j]=tmp[i][j];
    for(int i = 0; i < n; ++i)
        for(int j = 0,jj=n-1; j < n; ++j,--jj)
            tmp[i][j]=aux[jj][i];
}

void ref(){
    char aux[10][10];
    for(int i = 0; i < n; ++i)
        for(int j = 0; j < n; ++j)
            aux[i][j]=tmp[i][j];
    for(int i = 0; i < n; ++i)
        for(int j = 0; j < n; ++j)
            tmp[i][j]=aux[i][n-j-1];
}


int main(){
    ofstream fout ("transform.out");
    ifstream fin ("transform.in");

    fin>>n;
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            fin>>a[i][j];
        }
    }

    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            fin>>b[i][j];
        }
    }
    res();
    rot();
    if(eq()){
        fout<<"1"<<endl;
        return 0;
    }    
    rot();
    if(eq()){
        fout<<"2"<<endl;
        return 0;
    }
    rot();
    if(eq()){
        fout<<"3"<<endl;
        return 0;
    }

    res();
    ref();
    if(eq()){
        fout<<"4"<<endl;
        return 0;
    }
    
    ref();
    rot();
    ref();
    if(eq()){
        fout<<"5"<<endl;
        return 0;
    }    

    ref();
    rot();
    ref();
    if(eq()){
        fout<<"5"<<endl;
        return 0;
    }

    ref();
    rot();
    ref();
    if(eq()){
        fout<<"5"<<endl;
        return 0;
    }

    res();
    if(eq()){
        fout<<"6"<<endl;
        return 0;
    }


    fout<<"7"<<endl;
    return 0;
    
}