/*
ID: ThrashansEXE
TASK: milk
LANG: C++
*/

#include <bits/stdc++.h> 

using namespace std;



int main(){
    ofstream fout ("milk.out");
    ifstream fin ("milk.in");
    int n,f;
    fin>>n>>f;

    vector<pair<int,int> > v;
    for(int i = 0; i < f; ++i){
        int a,b;
        fin>>a>>b;
        v.push_back(make_pair(a,b));
    }

    sort(v.begin(), v.end());
    int ans=n;
    int tot=0;
    for(int i = 0; i < f; ++i){
        if(ans<=0) break;
        int a=v[i].first;
        int b=v[i].second;
        if(b<=ans){
            tot+=(a*b);
            ans-=b;
        }else{
            tot+=(ans*a);
            break;
        }
    }

    fout<<tot;
    fout<<endl;

    return 0;
}