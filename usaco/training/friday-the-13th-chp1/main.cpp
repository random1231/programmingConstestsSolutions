/*
ID: ThrashansEXE
TASK: friday
LANG: C++
*/

#include <bits/stdc++.h>

using namespace std;

int m[]={31,28,31,30,31,30,31,31,30,31,30,31};
int fr[7];

bool leap(int n){
    return ((n%100==0)?(n%400==0):(n%4==0));
}

int main(){
    ofstream fout ("friday.out");
    ifstream fin ("friday.in");
    int n;
    fin>>n;

    int cury=1900;
    int curm;
    int tmp=0;
    int i=0;
    while(cury<1900+n){
        curm=m[i];
        int res=(13+tmp)%7;
        if(res==0)
            res=7;
        //fout<< "mes: "<<i+1<< " 13:" << res<<endl;
        ++fr[--res];
        if(i==1&&leap(cury)){
            curm=29;
        }
        tmp=(curm+tmp)%7;
        //fout<<"tmp: "<<tmp<<endl;
        ++i;
        if(i==12){
            i=0;
            ++cury;
        }
    }


    fout<<fr[5]<< " "<< fr[6];
    for(int i = 0; i < 5; ++i){
        fout<<" "<<fr[i];
    }
    fout<<endl;

    return 0;
}