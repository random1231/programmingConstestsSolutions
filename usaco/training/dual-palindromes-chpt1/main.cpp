/*
ID: ThrashansEXE
TASK: dualpal
LANG: C++
*/

#include <bits/stdc++.h> 

using namespace std;

string convert(int n, int b){
    string res="";
    while(n>0){
        int tmp=n%b;
        char c;
        c='0'+tmp;
        //cerr<<n<<" "<<c<<endl;
        res.insert(res.begin(),c);
        n/=b;
    }

    return res;
}

bool pal(string s){
    for(int i = 0; i < s.size()/2; ++i){
        if(s[i]!=s[s.size()-1-i])
            return false;
    }
    return true;
}


int main(){
    ofstream fout ("dualpal.out");
    ifstream fin ("dualpal.in");
    int n,s;
    fin>>n>>s;
    int cn=0;
    int num=s+1;
    while(1){
        int x=0;
        for(int i = 2; i < 11; ++i){
            if(pal(convert(num,i))){
                x++;
            }
            if(x>=2){
                fout<<num<<endl;
                ++cn;
                if(cn>=n)
                    return 0;
                break;
            }
        }
        ++num;
    }

    return 0;
}
