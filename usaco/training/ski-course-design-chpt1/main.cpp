/*
ID: ThrashansEXE
TASK: skidesign
LANG: C++
*/

#include <bits/stdc++.h> 

using namespace std;

int main(){
    ofstream fout ("skidesign.out");
    ifstream fin ("skidesign.in");

    int n;
    fin>>n;
    vector<int> v;
    for(int i = 0; i < n; ++i){
        int x;
        fin>>x;
        v.push_back(x);
    }
    sort(v.begin(), v.end());
    int ans=1e9;
    int highest=v[v.size()-1];
    int lim=83;
    if(highest < lim)
        lim=highest;
    for(int i = 0; i <=lim; ++i){
        int res=0;
        for(int j = 0; j < n; ++j){
            int x=0;
            if(v[j]<i)
                x=i-v[j];
            else if(v[j]>i+17)
                x=v[j]-i-17;
            res+=x*x;
        }

        ans=min(ans,res);
    }

    fout<<ans;
    fout<<endl;
    return 0;
}
