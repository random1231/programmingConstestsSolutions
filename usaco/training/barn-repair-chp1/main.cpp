/*
ID: ThrashansEXE
TASK: barn1
LANG: C++
*/

#include <bits/stdc++.h> 

using namespace std;



int main(){
    ofstream fout ("barn1.out");
    ifstream fin ("barn1.in");
    int m,s,c;
    fin>>m>>s>>c;

    vector<int > v;
    for(int i = 0; i < c; ++i){
        int a;
        fin>>a;
        v.push_back(a);
    }
    sort(v.begin(), v.end());
    vector<pair<int,int> > dis;
    for(int i = 0; i < v.size()-1; ++i){
        int a=v[i];
        int b=v[i+1];
        if(b-a>1){
            dis.push_back(make_pair(b-a,i));
        }
    }

    sort(dis.rbegin(), dis.rend());
    int cn=0;
    vector<pair<int,int> > aux;
    for(int i = 0; i < dis.size(); ++i){
        if(cn>=m-1)
            break;
        aux.push_back(make_pair(dis[i].second,dis[i].first));
        ++cn;
    }
    aux.push_back(make_pair(v.size()-1,0));
    sort(aux.begin(), aux.end());

    int ans=0;

    int ls=v[0];
    for(int i = 0; i < aux.size(); ++i){
        int a=v[aux[i].first];
        int b=aux[i].second;
        ans+=(a-ls+1);
        ls=a+b;
    }



    fout<<ans<<endl;

    return 0;
}