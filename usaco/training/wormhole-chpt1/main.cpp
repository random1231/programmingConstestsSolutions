/*
ID: ThrashansEXE
TASK: wormhole
LANG: C++
*/

#include <bits/stdc++.h> 
#define MAX 12
using namespace std;

int x[MAX],y[MAX];
int paired[MAX];
int rightOf[MAX];
ofstream fout ("wormhole.out");
ifstream fin ("wormhole.in");

int n,ans;

void cycle(){
    for(int i = 0; i < n; ++i){
        int pos=i;
        for(int j = 0; j < n; ++j){
            pos=rightOf[paired[pos]];
            if(pos==-1)
                break;
        }
        if(pos!=-1){
            ++ans;
            return;
        }
    }
}

void solve(){
    int i;
    for(i = 0; i < n; ++i){
        if(paired[i]==-1) break;
    }

    if(i>=n){
        cycle();
        return;
    }
    for(int j = i+1; j < n; ++j){
        if(paired[j]==-1){
            paired[i]=j;
            paired[j]=i;
            solve();
            paired[i]=-1;
            paired[j]=-1;
        }
    }


}

int main(){
    fin>>n;

    for(int i = 0; i < n; ++i){
        fin>>x[i]>>y[i];
    }

    memset(paired,-1,sizeof paired);
    memset(rightOf,-1,sizeof rightOf);
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            if(y[i]==y[j]&&x[j]>x[i]){
                if(rightOf[i]==-1||x[j]<x[rightOf[i]]){
                    rightOf[i]=j;
                }
            }
        }
    }

    solve();
    fout<<ans<<endl;
    return 0;
}