/*
ID: ThrashansEXE
TASK: gift1
LANG: C++
*/

#include <bits/stdc++.h>

using namespace std;

int main(){

    ofstream fout ("gift1.out");
    ifstream fin ("gift1.in");

    int n;
    fin>>n;
    map<string, int> m;
    vector<string> v;
    for(int i = 0; i < n; ++i){
        string tm;
        fin>>tm;
        v.push_back(tm);
        m[tm]=0;
    }

        string tm;
    while(fin>>tm){
        int a, b;
        fin>>a>>b;
        if(a==0&&b==a)
            continue;
        m[tm]+= (a*-1)+(a%b);
        for(int i = 0; i < b; ++i){
            string s;
            fin>>s;
            m[s]+=(a/b);
        }
    }

    for(int i = 0; i < n; ++i){
        fout<<v[i]<<" "<<m[v[i]];
        fout<<endl;
    }

    return 0;
}