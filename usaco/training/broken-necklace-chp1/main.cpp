/*
ID: ThrashansEXE
TASK: beads
LANG: C++
*/

#include <bits/stdc++.h> 

using namespace std;
int main(){
    ofstream fout ("beads.out");
    ifstream fin ("beads.in");
    int n;
    fin>>n;

    string s;
    fin>>s;
    int mx=0;

    for(int i = 0; i < n; ++i){
        string tm=s;
        int j=i;
        char cur=s[j];
        int ans=0;
        //logs<<"i: "<<i<<endl;
        bool fl=false;
        while(!tm.empty()){
            if(j>=tm.size())
                fl=true;
            j=(j%(tm.size()));
            if(cur=='w'&&tm[j]!='w'){
                cur=tm[j];
            }
            else if(tm[j]!='w'&&tm[j]!=cur){
                break;
            }
            ++ans;
            tm.erase(tm.begin()+j);
            //logs<<tm<<endl;
            //j=(j+1)%n;
        }

        if(!fl)
            tm.erase(tm.begin()+j, tm.end());

        //logs<<ans<<endl;
        //logs<<tm<<endl;
        
        if(!tm.empty()){
            j=tm.size()-1;
            cur=tm[j];
            //logs<<"-cur: "<<cur<<endl;
            while(!tm.empty()){
                j=tm.size()-1;
                if(cur=='w'&&tm[j]!='w'){
                    cur=tm[j];
                    //logs<<"cur: "<<cur<<endl;
                }
                else if(tm[j]!='w'&&tm[j]!=cur){
                    break;
                }
                ++ans;
                tm.erase(tm.end()-1);
                //logs<<tm<<endl;
                //j=(j-1)%n;
            }
        }

        //logs<<"---"<<endl;
        //logs<<ans<<endl;
        //logs<<"---"<<endl;

        if(ans>mx)
            mx=ans;
    }

    fout<<mx<<endl;

    return 0;
}