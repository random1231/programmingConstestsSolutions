/*
ID: ThrashansEXE
TASK: milk2
LANG: C++
*/

#include <bits/stdc++.h> 
using namespace std;

short int n;
int mx,mn;


void solve(int i,int &cura, int &curb,vector<pair<int,int> > &v){
    if(i>=n)
        return;
    if(v[i].first>=cura&&v[i].first<=curb){
        curb=max(curb,v[i].second);
        if(curb-cura>mx)
            mx=curb-cura;
        solve(i+1,cura,curb,v);
    }
    else{
        if(v[i].first-curb>mn)
            mn=v[i].first-curb;
        cura=v[i].first;
        curb=v[i].second;
        if(curb-cura>mx)
            mx=curb-cura;
        solve(i+1,cura,curb,v);
    }
}


int main(){
    ofstream fout ("milk2.out");
    ifstream fin ("milk2.in");
    fin>>n;
    vector<pair<int,int> > v;
    for(int i = 0; i < n; ++i){
        int a,aa;
        fin>>a>>aa;
        v.push_back(make_pair(a,aa));
    }
    sort(v.begin(), v.end());

    mx=0;
    mn=0;
    int i=0;
    int cura=v[i].first;
    int curb=v[i].second;
    mx=curb-cura;
    solve(i+1,cura,curb,v);
    fout<<mx<<" "<<mn<<endl;

    return 0;

}