#include <bits/stdc++.h>

using namespace std;

int main(){
    ios::sync_with_stdio(false);
    int n;
    cin>>n;
    unordered_set<string> m;
    int ans=0;
    for(int i = 0; i < n; ++i){
        string s;
        unordered_set<char> tmp;
        cin>>s;
        string res="";
        for(auto x : s){
            if(tmp.find(x)==tmp.end()){
                tmp.insert(x);
                res+=x;
            }
        }

        sort(res.begin(), res.end());

        if(m.find(res)==m.end()){
            m.insert(res);
            ++ans;
        }
    }




    cout<<ans;
    cout<<endl;

    return 0;
}
