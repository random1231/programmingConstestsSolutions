#include <bits/stdc++.h>

using namespace std;

int main(){
    ios::sync_with_stdio(false);
    int b[15];
    for(int i = 0; i < 14; ++i){
        cin>>b[i];
    }
    long long ans=-1;
    for(int i = 0; i < 14; ++i){
        int a[15];
        if(b[i]==0)
            continue;
        for(int j = 0; j < 14; ++j){
                a[j]=b[j];
            }
        int cycle=(a[i])/14;
        int tmp=a[i];
        a[i]=0;
        if(cycle>0){
            for(int j = 0; j < 14; ++j){
                a[j]+=cycle;
                //cerr<<a[j]<< " ";
            }
            //cerr<<endl;
        }
        int rem=tmp%14;
        //cerr<<rem<<endl;
        int j = (i+1)%14;
        for(int cnt=1; cnt <= rem;++cnt){
            //cerr<<j<<" ";
            ++a[j];
             /*for(int j = 0; j < 14; ++j)
                cerr<<a[j]<< " ";
            cerr<<endl;*/
            j = (j+1)%14;
        }
        long long cur=0;
        for(int k = 0; k < 14; ++k){
            if(a[k]%2==0){
                cur+=a[k];
            }
        }
        ans=max(ans,cur);
    }

    cout<<ans;
    cout<<endl;
    
    return 0;
        
}