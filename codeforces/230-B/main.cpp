#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll sieve_size;
bitset<1000050> bs;

void sieve(ll upperbound){
    sieve_size = upperbound;
    bs.set();
    bs[0] = bs [1] = 0;
    for(long long i = 2; i <= sieve_size; i++)
    	if(bs[i]) {
	    	for (long long j = i*i; j <= sieve_size; j+=i)
	    		bs[j] = 0;
	    }
}
bool solve(ll n){
	ll res=round(sqrt(n));
	ll res1=res*res;
	ll res2=(res+1)*(res+1);
	if(res1==n){
		if(bs[res]) return true;
	}else if(res2==n){
		if(bs[res]) return true;
	}
	return false;
}



int main(){
	ios::sync_with_stdio(false);
	sieve(1000010);
	int n;
	cin>>n;
	ll x;
	for(int i=0; i<n;++i){
		cin>>x;
		if(solve(x)) cout<<"YES"; else cout<<"NO";
		cout<<endl;
	}
	return 0;
}
