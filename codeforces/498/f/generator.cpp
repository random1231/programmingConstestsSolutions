#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin
#define clear(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef pair<pair<int,int>,int> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;

int main(){
	
	int a[1010];
	//clear(a,1000000000);
	cout<<"1000"<<endl;
	int tam=1000;
	for(int i=1; i<=tam/2;++i){
		a[i]=i;
		a[tam-i+1]=i;
	}
	
	for(int i=1; i<=tam;++i){
		cout<<a[i]<< " ";
	}
	return 0;
}

