#include <bits/stdc++.h>
/*#include <ext/pb_ds/assoc_container.hpp>	// Common file
#include <ext/pb_ds/tree_policy.hpp> 		// Including */
using namespace std;
//using namespace __gnu_pbds;
/*
template<typename T>
using ordered_set = tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template<typename F, typename S>
using ordered_map = tree<F, S, less<F>, rb_tree_tag, tree_order_statistics_node_update>;
*/

typedef long long ll;
typedef vector <int> vi;
typedef vector <ll> vl;
typedef vector <vi> vvi;
typedef vector <vl> vvl;
typedef pair <int,int> pii;
typedef pair <double, double> pdd;
typedef pair <ll, ll> pll;
typedef vector <pii> vii;
typedef vector <pll> vll;
typedef vector<int>:: iterator vit;
typedef set<int>:: iterator sit;


const double PI  =      acos(-1.0);
const double eps =      1e-8;
const long inf   =      0x3f3f3f3f;
const ll infLL   =      (1ll << 62);
const double oo  =      1e15;


#define FOR(i,a,b)      for(i = a; i <= b; ++i)
#define REP(i,a,b)      for(i = a; i >= b; --i)
#define pb              push_back
#define eb              emplace_back
#define F               first
#define S               second
#define all(a)          a.begin(),a.end()
#define mp              make_pair
#define endl            '\n'
#define pf              printf
#define sf              scanf
//#define left            __left
//#define right           __right
//#define tree            __tree
#define inp(f)         freopen(f,"r",stdin)
#define out(f)         freopen (f,"w",stdout)
#define CHAR            getchar()
#define mxe(a,n)        (*max_element(a,a+n))
#define mne(a,n)        (*min_element(a,a+n))
#define SUM(a,n)        (accumulate(a,a+n,0)) // returns sum of all the values in the array //
#define countbit(x)     __builtin_popcountll(x) // counts number of 1's in binary representation //
#define trail_zero(x)   __builtin_ctz(x) 
//#define MOD             1000000007
#define RESET(a,b)      memset(a,b,sizeof(a))
#define CLR(a)          memset(a,0,sizeof(a))
#define SET(a)          memset(a,-1,sizeof(a))
#define gcd(a,b)        __gcd(a,b)
#define lcm(a,b)        (a * (b / gcd(a,b)))
#define sqr(a)          ((a) * (a))
#define max(a,b)        ((a) > (b) ? (a) : (b))
#define min(a,b)        ((a) < (b) ? (a) : (b))
#define abs(a)          ((a) < 0 ? (-(a)): (a))
//#define harmonic(n)     0.57721566490153286l+log(n)
#define optimize()      ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
#define fraction()      cout.unsetf(ios::floatfield); cout.precision(10); cout.setf(ios::fixed,ios::floatfield);
#define what_is(x)      cerr << #x << " is " << x << endl;

/*----------------------Graph Moves----------------*/
//const int fx[] = {+1, -1, +0, +0};
//const int fy[] = {+0, +0, +1, -1};
//const int fx[] = {+0, +0, +1, -1, -1, +1, -1, +1};   	// Kings Move
//const int fy[] = {-1, +1, +0, +0, +1, +1, -1, -1};  	// Kings Move
//const int fx[] = {-2, -2, -1, -1,  1,  1,  2,  2};	// Knights Move
//const int fy[] = {-1,  1, -2,  2, -2,  2, -1,  1};	// Knights Move
/*------------------------------------------------*/

inline bool     checkBit (ll n, int i) { return n&(1LL<<i); }
inline ll       setBit (ll n, int i) { return n|(1LL<<i);; }
inline ll       resetBit (ll n, int i) { return n&(~(1LL<<i)); }
inline bool     EQ (double a, double b) { return fabs(a-b) < 1e-9; }
inline bool     isLeapYear (ll year) { return (year%400==0) || (year%4==0 && year%100!=0); }
inline bool     isInside (pii p,ll n,ll m) { return (p.first>=0&&p.first<n&&p.second>=0&&p.second<m); }
inline bool     isInside (pii p,ll n) { return (p.first>=0&&p.first<n&&p.second>=0&&p.second<n); }
inline bool     isSquare (ll x) { ll s = sqrt(x);	return (s*s==x); }
inline bool     isFib (ll x) { return isSquare(5*x*x+4)|| isSquare(5*x*x-4); }
inline bool     isPowerOfTwo (ll x) { return (x && !(x & (x - 1))); }
inline void     normal (ll &a, ll MOD) { a %= MOD; (a < 0) && (a += MOD); }
inline ll       modMul (ll a, ll b, ll MOD) { a %= MOD, b %= MOD; normal(a, MOD), normal(b, MOD); return (a*b)%MOD; }
inline ll       modAdd (ll a, ll b, ll MOD) { a %= MOD, b %= MOD; normal(a, MOD), normal(b, MOD); return (a+b)%MOD; }
inline ll       modSub (ll a, ll b, ll MOD) { a %= MOD, b %= MOD; normal(a, MOD), normal(b, MOD); a -= b; normal(a, MOD); return a; }
inline ll       modPow (ll b, ll p, ll MOD) { ll r = 1; while(p) { if(p&1) r = modMul(r, b, MOD); b = modMul(b, b, MOD); p >>= 1; } return r; }
inline ll       modInverse (ll a, ll MOD)   { return modPow(a, MOD-2, MOD); }
inline ll       modDiv (ll a, ll b, ll MOD) { return modMul(a, modInverse(b, MOD), MOD); }


// sum..
int sum() { return 0; }
template<typename T, typename... Args>
T sum(T a, Args... args) { return a + sum(args...); }
// eg: cout << sum(2, 3, 5, 9) + sum(5.9); print : 24.9

struct func {
	//this is a sample overloading function for sorting stl
	bool operator()(pii const &a, pii const &b) {
		if(a.F==b.F)
			return (a.S<b.S);
		return (a.F<b.F);
	}
};
/*
// Find the largest power of 2 (most significant bit in binary form), which is less than or equal to the given number N.

ll  largestPower(ll n){
	for(int i = 0; i <= sizeof(n); ++i){
		n = n | (n >> (1 << i));
	}

	return (n + 1) >> 1;
}
*/
int a[11234],n;
int main(){
	//inp("input.txt");
	optimize();
	
	cin >> n;
	for(int i = 0; i < n; ++i){
		cin >> a[i];
	}
	for(int i = 0; i < n; ++i){

		if(a[i] % 2 == 0){
			a[i] = a[i] - 1;
		}
	}
	for(int i = 0; i < n; ++i){
		cout << a[i] << " ";
	}
	cout << '\n';
	return 0;
}
