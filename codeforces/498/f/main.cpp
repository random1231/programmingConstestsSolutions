#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin
#define clear(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef pair<pair<int,int>,int> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;

int main(){
	int n;cin>>n;
	int a[n+1];
	for(int i=0; i<n;++i){
		cin>>a[i];
	}
	
	
	for(int i=0; i<n;++i){
		if(a[i]%2==0) cout<<a[i]-1;
		else cout<<a[i];
		cout<<" ";
		
	}
	
	cout<<endl;
	
	
	
	return 0;
}

