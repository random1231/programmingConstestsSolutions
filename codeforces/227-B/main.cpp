#include <bits/stdc++.h>

using namespace std;

int main(){
    int n;
    cin>>n;
    map<int, int> m;    
    for(int i = 0; i < n; ++i){
        int x;
        cin>>x;
        m[x]=i;
    }

    long long a=0;
    long long b=0;

    int q;
    cin>>q;
    while(q--){
        int x;
        cin>>x;
        //cout<< m[x]<<endl;
        a+=m[x]+1;
        b+=(n-m[x]);
    }


    cout<< a<< " "<<b<<endl;


    return 0;
}