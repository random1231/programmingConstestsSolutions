#include <bits/stdc++.h>

using namespace std;

int main(){
    int x,y;
    cin>>x>>y;
    string s;
    cin>>s;

    while(y--){
        int i=0;
        while(i<x-1){
            if(s[i]=='B'&&s[i+1]=='G'){
                s[i]='G';
                s[i+1]='B';
                i+=2;
                continue;
            }
            ++i;
        }
    }

    cout<<s;
    cout<<endl;
    return 0;
}