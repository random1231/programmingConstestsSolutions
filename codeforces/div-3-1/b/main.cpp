#include <bits/stdc++.h>

using namespace std;

int main(){
    ios::sync_with_stdio(false);
    long long n, m, k;
    cin>>n>>m>>k;

    if(k<=n-1){
        cout<<k+1<<" "<<"1"<<endl;
        return 0;
    }

    if(k<m+n-1&&k>=n){
        cout<<n<<" "<<k-n+2<<endl;
        return 0;
    }

    k-=n+m-2;
    bool lef=true;
    long long x=n-1;
    long long y;
    while(1){
        if(m-1<k){
            k-=m-1;
            lef=!lef;
            --x;
        }
        else{
            if(lef)
                y=m-k+1;
            else
                y=k+1;
            break;
        }
    }
    
    cout<<x<<" "<<y;
    cout<<endl;

    return 0;
}