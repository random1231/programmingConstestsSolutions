#include <bits/stdc++.h>

using namespace std;

int n, a,b;
    vector<pair<int, int> > v;



int solve(int a, int b){
    if(a==0&&b==a){
        cout<<a<<" "<<b<<endl;
        long d=0;
        for(auto x : v){
            d+=x.second;
        }
        return d;
    }
    int ans=0;
    for(int i = 0; i < v.size(); ++i){
        int cur1=0;
        int cur2=0;
        if(a>0){
            v[i].first*=2;
            cur1=max(cur1,solve(a-1, b));
            v[i].first/=2;
        }
        else if(b>0){
            auto tmp=v[i].second;
            v[i].second=v[i].first;
            cur2=max(cur2,solve(a, b-1));
            v[i].second=tmp;
        }
        ans=max(cur1, cur2);
        cout<<ans;
        cout<<endl;
    }
    return ans;
}

int main(){
    ios::sync_with_stdio(false);
    int ans=0;
    cin>>n;
    for(int i = 0; i < n; ++i){
        int x, y;
        cin>>x>>y;
        v.push_back({x,y});
    }

    cout<<solve(a,b);
    cout<<endl;

    return 0;
}