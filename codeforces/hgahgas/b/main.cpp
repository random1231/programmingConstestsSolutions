#include<bits/stdc++.h>
using namespace std;

#define nl      '\n'
#define fi      first
#define se      second
#define mp      make_pair
#define pb      push_back
#define si(x)   scanf("%d", &x)
#define sl(x)   scanf("%lld", &x)

typedef long long ll;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef map<ll, ll> mll;
typedef pair<ll, ll> pll;
typedef map<int, int> mii;
typedef pair<int, int> pii;
typedef map<char, int> mci;
typedef map<char, char> mcc;
typedef pair<char, int> pci;
typedef unsigned long long ull;

const double EPS = 1e-11;
const ll MOD = 1000000007;
const double PI = 2 * acos(0.0);

int main() {
    int a, b, x;
    cin >> a >> b >> x;

    string s = "";
    bool flag = true;
    for (int i = 0; i < x; i++) {
        if (flag) {
            flag = false;
            s += '0';
            a--;
        } else {
            flag = true;
            s += '1';
            b--;
        }
    }

    if (s[0] != s[s.size() - 1]) {
        if (a == 0) {
            while (b > 0) {
                cout << '1';
                b--;
            }
            cout << s << nl;
        }
        else {
            cout << s;
            while (b > 0) {
                cout << '1';
                b--;
            }
            while (a > 0) {
                cout << '0';
                a--;
            }
            cout << nl;
        }
    } else {
        while (a > 0) {
            cout << '0';
            a--;
        }
        cout << s;
        while (b > 0) {
            cout << '1';
            b--;
        }
        cout << nl;
    }
}