#include <bits/stdc++.h>

using namespace std;

int n,k;
int prefix[5010];
int a[5010];

int main(){
    cin>>n>>k;
    for(int i = 0; i < n; ++i){
        cin>>a[i];
    }
    prefix[0]=0;
    for(int i = 1; i <= n; ++i){
        prefix[i]=prefix[i-1]+a[i-1];
        //cout<<prefix[i-1]<< " "<<a[i-1];
    }

    

    int mx=-1;
    set<double> s;
    for(int j = k; j <=n; ++j){
        for(int i = j; i <= n; ++i){
            int sm=prefix[i]-prefix[i-j];
            //cout<<sm<<endl;
            mx=max(mx,sm);
        }
        s.insert((mx*1.0)/(j*1.0));
    }

    cout<<fixed<<setprecision(6)<<*(--s.end())<<endl;;

    return 0;
}