#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n;
    cin>>n;
    vector <int> vec(n);

    for(int i=0;i<n;i++){
        int a;
        cin>>a;
        vec[a-1]++;
    }
    int pockets = 1;
    for(int i=0;i<n;i++){
        if (vec[i]>pockets)
            pockets=vec[i];
    }
    cout<<pockets<<endl;
    return 0;
}