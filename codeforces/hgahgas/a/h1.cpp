#include<bits/stdc++.h>
#define ll long long
#define int long long
#define ss string
#define pb push_back
#define rev(i,n) for(ll i=0;i<n;i++)
#define rev1(i,n) for(ll i=n-1;i>=0;i--)
#define sync ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0)
#define all(v) v.begin(),v.end()
#define S second
#define F first
#define tci(v,i) for(auto i=v.begin();i!=v.end();i++)
#define MOD 1000000007
#define lld long double
#define TIMESTAMP cerr<<(((double)clock())/CLOCKS_PER_SEC)
#define  rep(i,start,lim) for(ll (i)=(start);i<(lim);i++)
using namespace std;

main()
{
    int n;
    cin>>n;
    int arr[n];
    for(int i=0; i<n; i++) cin>>arr[i];
    sort(arr,arr+n);
    int ans=0,temp=0, finans=0;
    for(int i=0; i<n; i++)
    {
        if(temp == arr[i])
        {
            ans++;
        }
        else
        {
            if(ans>finans)
            finans=ans;
            ans=0;
            temp=arr[i];
        }
    }
    if(ans>finans)
        finans=ans;
    cout<<finans+1<<endl;
}