#include <bits/stdc++.h>

using namespace std;

int a[1000];
int n;
map<int, int> m;

int main(){
    cin>>n;
    for(int i = 0; i < n; ++i){
        cin>>a[i];
        if(m.find(a[i])!=m.end()){
            ++m[a[i]];
        }else{
            m[a[i]]=1;
        }
    }

    int ans=0;
    for(auto item : m){
        ans=max(ans,item.second);
    }

    cout<<ans<<endl;

    return 0;
}