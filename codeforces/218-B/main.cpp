#include <bits/stdc++.h>

using namespace std;

int main(){
    int n,m;
    cin>>n>>m;
    priority_queue<int> q;
    priority_queue<int, vector<int>,greater<int> > q2;

    for(int i = 0; i < m; ++i){
        int x;
        cin>>x;
        q.push(x);
        q2.push(x);
    }
    int s=n;
    int ans=0;
    while(s&&!q.empty()){
        auto x=q.top();
        ans+=x;
        q.pop();
        if(x>1)
            q.push(x-1);
        --s;
    }

    s=n;
    int ans2=0;
    while(s&&!q2.empty()){
        auto x=q2.top();
        ans2+=x;
        q2.pop();
        if(x>1)
            q2.push(x-1);
        --s;
    }
    
    cout<<ans<< " "<<ans2;
    cout<<endl;

    return 0;
}