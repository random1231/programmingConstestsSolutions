#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main(){
	ios::sync_with_stdio(false);
	int n;
	cin>>n;
	string s1,s2;cin>>s1>>s2;
	ll freq[10];
	//0->0,0  1-> 0,1 2->1,0 3->1,1
	mem(freq,0);
	assert(s1.sz==s2.sz);
	for(int i=0; i<(int)s1.sz;++i){
		if(s1[i]=='0'){
			if(s2[i]=='0'){
				++freq[0];
			}else if(s2[i]=='1'){
				++freq[1];
			}
		}else if(s1[i]=='1'){
			if(s2[i]=='1'){
				++freq[3];
			}else if(s2[i]=='0'){
				++freq[2];
			}
		}
	}
	
	ll res;
	res=freq[0]*freq[2]+freq[0]*freq[3]+freq[1]*freq[2];
	cout<<res;cout<<endl;
	
	
	return 0;
}
