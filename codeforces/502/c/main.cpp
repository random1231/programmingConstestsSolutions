#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main(){
	ios::sync_with_stdio(false);
	int n;
	cin>>n;
	int i=(n+1)/2+1;
	int lim=i;
	for(; i<=n;++i){
		cout<<i<<" ";
	}
	
	for(int i=1; i<lim;++i){
		cout<<i<< " ";
	}
	cout<<endl;
	//~ string per;
	//~ for(int i=1; i<=n;++i){
		//~ per+=to_string(i);
	//~ }
	
	//~ do
	//~ {
		//~ cout<<per;cout<<endl;
	//~ }while(next_permutation(per.beg, per.en));
	
	
	return 0;
}
