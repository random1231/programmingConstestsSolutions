#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main(){
	ios::sync_with_stdio(false);
	
	int n;cin>>n;
	int place=1;
	int a,b,c,d;
	cin>>a>>b>>c>>d;
	int total=a+b+c+d;
	for(int i=0; i<n-1;++i){
		int sm=0;
		for(int j=0; j<4;++j){
			int x;
			cin>>x;
			sm+=x;
		}
		if(sm>total){
			++place;
		}
	}
	
	cout<<place<<endl;
	
	return 0;
}
