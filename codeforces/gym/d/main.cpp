#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main(){
	ios::sync_with_stdio(false);
	int t;cin>>t;while(t--){
		int n;cin>>n;
		int ans=0;
		while(n--){
			int x;cin>>x;
			if(x>0) ++ans;
		}
		
		cout<<ans<<endl;
	}
	
	
	return 0;
}
