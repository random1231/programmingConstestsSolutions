#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

void negToPos(int &x, int mod){
	//~ while(x<0)
		//~ x+=mod;
		if(x>=0) return;
    x=(mod+(x%mod))%mod; //Not tested but it should work!
}

int main(){
	ios::sync_with_stdio(false);
	int t;cin>>t;while(t--){
		int n,m;cin>>n>>m;
		int totalh=0;
		int totalm=0;
		for(int i=0; i<n;++i){
			string s1,s2;
			cin>>s1>>s2;
			int h1=stoi(s1.substr(0,2));
			int h2=stoi(s2.substr(0,2));
			int m1=stoi(s1.substr(3,4));
			int m2=stoi(s2.substr(3,4));
			if(m2>m1){
				totalm+=((h2-h1)*60)+m2-m1;
				//~ cout<<totalm<<endl;
			}else{
				//~ cout<<totalm<<endl;
				totalm+=((h2-h1)*60)-(m1-m2);
			}
			//~ totalh+=(h2-h1);
			//~ int x=m2-m1;
			//~ negToPos(x,60);
			//~ cout<<"X "<<x<<endl;
			//~ totalm+=x;
			//~ cout<<h1<<":"<<m1<< " "<<h2<<":"<<m2;cout<<endl;
		}
		//~ cout<<totalh<< " "<<totalm;cout<<endl;
		totalh=((int)(totalm)/60);
		if(totalh>=m) cout<<"YES";else cout<<"NO";cout<<endl;
		
	}
	
	return 0;
}
