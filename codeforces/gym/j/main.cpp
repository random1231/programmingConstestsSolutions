#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define lob lower_bound
#define upb upper_bound
#define ff first
#define ss second
#define beg begin()
#define en end()
#define sz size()
#define mem(a,b) memset(a,b,sizeof a)

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int a[10010];
int n;

int main(){
	ios::sync_with_stdio(false);
	//~ unordered_set<int> m;
	int t;cin>>t;while(t--){
		cin>>n;
		for(int i=0; i<n;++i){
			cin>>a[i];
			//~ m.insert(a[i]);
		}
		
		sort(a,a+n);
		int ans=0;
		for(int i=0; i<n;++i){
			int pos=(upb(a,a+n,a[i]+1))-a-1;
			//~ cout<<i<< " pos: "<<pos;cout<<endl;
			ans=max(ans,pos-i+1);
		}
		cout<<ans;cout<<endl;
	}
	
	
	
	return 0;
}
