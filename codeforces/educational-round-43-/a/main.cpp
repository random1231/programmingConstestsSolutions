#include <bits/stdc++.h>

using namespace std;

int main(){
    ios::sync_with_stdio(false);
    int n;
    cin>>n;
    string s;
    cin>>s;
    if(n==1){
        cout<<s<<endl;
        return 0;
    }

    while(1){
        bool x=false;
       for(int i = 0; i < s.size()-1; ++i){
            if(s[i]=='1'&&s[i+1]=='1'){
                s.erase(s.begin()+i);
                x=true;
                break;
            }
            else if(s[i]=='0'&&s[i+1]=='1'){
                s[i]='1';
                s[i+1]='0';
                x=true;
            }
        }
        if(!x)
            break;
    }

    cout<<s;
    cout<<endl;

    return 0;
}